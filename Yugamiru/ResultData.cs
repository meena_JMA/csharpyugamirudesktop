﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    class ResultData
    {

        int YUGAMIRU_POINT_RANK_NONE = 0;
        int YUGAMIRU_POINT_RANK_A = 1;
        int YUGAMIRU_POINT_RANK_B = 2;
        int YUGAMIRU_POINT_RANK_C = 3;
        int YUGAMIRU_POINT_RANK_D = 4;

        public int  m_Score;

        public int CalcYugamiPoint(FrontBodyAngle m_FrontBodyAngleStanding,
            FrontBodyAngle m_FrontBodyAngleKneedown, SideBodyAngle m_SideBodyAngle)
{
    
    double iDistPoint =
          Math.Abs(m_FrontBodyAngleStanding.GetRightThigh())     // XO Right
        + Math.Abs(m_FrontBodyAngleStanding.GetLeftThigh())      // XO Left
        + Math.Abs(m_FrontBodyAngleKneedown.GetRightThigh()) * 3 // InOut Right
        + Math.Abs(m_FrontBodyAngleKneedown.GetLeftThigh()) * 3  // InOut Left
        + Math.Abs(m_FrontBodyAngleStanding.GetHip()) * 2           // œ”Õ‰ñù@—§ˆÊ
        + Math.Abs(m_FrontBodyAngleKneedown.GetHip()) * 2           // œ”Õ‰ñù@‹üL
        + Math.Abs(m_FrontBodyAngleStanding.GetCenter()) * 4 // œ”ÕƒVƒtƒg@—§ˆÊ
        + Math.Abs(m_FrontBodyAngleKneedown.GetCenter()) * 3 // œ”ÕƒVƒtƒg@‹üL
        + Math.Abs(m_FrontBodyAngleStanding.GetShoulder())   // Œ¨‹“ã@—§ˆÊ
        + Math.Abs(m_FrontBodyAngleKneedown.GetShoulder())   // Œ¨‹“ã@‹üL 
        + Math.Abs(m_FrontBodyAngleStanding.GetEar())        // “ª•”ŒX‚«@—§ˆÊ
        + Math.Abs(m_FrontBodyAngleKneedown.GetEar())        // “ª•”ŒX‚«@‹üL  
        + Math.Abs(m_FrontBodyAngleStanding.GetBody2Center())        // ‘ÌŠ²ƒVƒtƒg@—§ˆÊ
        + Math.Abs(m_FrontBodyAngleKneedown.GetBody2Center())        // ‘ÌŠ²ƒVƒtƒg@‹üL  
        + Math.Abs(m_FrontBodyAngleStanding.GetHead())        // ŽñƒVƒtƒg@—§ˆÊ
        + Math.Abs(m_FrontBodyAngleKneedown.GetHead())        // ŽñƒVƒtƒg@—§ˆÊ  
       + Math.Abs(m_SideBodyAngle.GetBodyBalance())    // ã‘Ì@—§ˆÊ  

    ;
    
        int iScore = 0;
	if( iDistPoint< 12 ) {
		iScore = 100 - 4 *(int) iDistPoint;
	} else if( iDistPoint< 37 ) {
		iScore = 52 - ((int)iDistPoint - 12) * 4 / 10;
	} else if (iDistPoint <= 43 ) {
		iScore = 42 - ((int)iDistPoint - 37) * 15 / 10;
	} else {
		iScore = 33;
	}
	if(iScore< 0){
		iScore = 0;
	}
	return iScore;
}

public int CalcYugamiPointRank(FrontBodyAngle m_FrontBodyAngleStanding, 
    FrontBodyAngle m_FrontBodyAngleKneedown, SideBodyAngle m_SideBodyAngle) 
{
	int iPoint = CalcYugamiPoint(m_FrontBodyAngleStanding, m_FrontBodyAngleKneedown, m_SideBodyAngle);
            SetScore(iPoint);
	if (( 91 <= iPoint ) && ( iPoint <= 100 )){
		return( YUGAMIRU_POINT_RANK_A );
	}
	if ( ( 61 <= iPoint ) && ( iPoint <= 90 )){
		return( YUGAMIRU_POINT_RANK_B );
	}
	if ( ( 41 <= iPoint ) && ( iPoint <= 60 )){
		return( YUGAMIRU_POINT_RANK_C );
	}
	if ( ( 0 <= iPoint ) && ( iPoint <= 40 )){
		return( YUGAMIRU_POINT_RANK_D );
	}
	return( YUGAMIRU_POINT_RANK_NONE );
}
        public void SetScore(int Score)
        {
            m_Score = Score;
        }
        public int GetScore()
        {
            return m_Score;
        }

    }
}
