﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Yugamiru
{
    public class FrontBodyLabelString
    {
        public string m_strGlabellaPosition;
        public string m_strEarCenterPosition;
        public string m_strChinPosition;
        public string m_strShoulderCenterPosition;
        public string m_strHipCenterPosition;
        public string m_strAnkleCenterPosition;
        public string m_strEarBalance;
        public string m_strShoulderBalance;
        public string m_strHipBalance;
        public string m_strRightKneeAngle;
        public string m_strLeftKneeAngle;

        public FrontBodyLabelString() 
        {
            m_strGlabellaPosition = "";

            m_strEarCenterPosition = "";

            m_strChinPosition = "";

            m_strShoulderCenterPosition = "";

            m_strHipCenterPosition = "";

            m_strAnkleCenterPosition = "";

            m_strEarBalance = "";

            m_strShoulderBalance = "";

            m_strHipBalance = "";

            m_strRightKneeAngle = "";

            m_strLeftKneeAngle = "";
        }

 public FrontBodyLabelString ( FrontBodyPosition FrontBodyPosition, FrontBodyAngle FrontBodyAngle, Point ptBenchmark1,Point ptBenchmark2,
		int iBenchmarkDistance )
        { 

            SetData(
                FrontBodyPosition,
                FrontBodyAngle,
                ptBenchmark1,
                ptBenchmark2,
                iBenchmarkDistance);
        }

        public FrontBodyLabelString(FrontBodyLabelString rSrc)
        {

            m_strGlabellaPosition = rSrc.m_strGlabellaPosition;

            m_strEarCenterPosition = rSrc.m_strEarCenterPosition;

            m_strChinPosition = rSrc.m_strChinPosition;

            m_strShoulderCenterPosition = rSrc.m_strShoulderCenterPosition;

            m_strHipCenterPosition = rSrc.m_strHipCenterPosition;

            m_strAnkleCenterPosition = rSrc.m_strAnkleCenterPosition;

            m_strEarBalance = rSrc.m_strEarBalance;

            m_strShoulderBalance = rSrc.m_strShoulderBalance;

            m_strHipBalance = rSrc.m_strHipBalance;

            m_strRightKneeAngle = rSrc.m_strRightKneeAngle;

            m_strLeftKneeAngle = rSrc.m_strLeftKneeAngle;
       }

   /*     CFrontBodyLabelString &CFrontBodyLabelString::operator=( const CFrontBodyLabelString &rSrc )
{
	m_strGlabellaPosition		= rSrc.m_strGlabellaPosition;
	m_strEarCenterPosition		= rSrc.m_strEarCenterPosition;
	m_strChinPosition			= rSrc.m_strChinPosition;
	m_strShoulderCenterPosition	= rSrc.m_strShoulderCenterPosition;
	m_strHipCenterPosition		= rSrc.m_strHipCenterPosition;
	m_strAnkleCenterPosition	= rSrc.m_strAnkleCenterPosition;
	m_strEarBalance				= rSrc.m_strEarBalance;
	m_strShoulderBalance		= rSrc.m_strShoulderBalance;
	m_strHipBalance				= rSrc.m_strHipBalance;
	m_strRightKneeAngle			= rSrc.m_strRightKneeAngle;
	m_strLeftKneeAngle			= rSrc.m_strLeftKneeAngle;
	return *this;
}*/

  public  void SetData( FrontBodyPosition FrontBodyPosition,
    		 FrontBodyAngle FrontBodyAngle,
    		 Point ptBenchmark1,
    		 Point ptBenchmark2,
            int iBenchmarkDistance)
    {
            // Point ptBenchmarkDistance = ptBenchmark2 - ptBenchmark1;
            var X = ptBenchmark2.X - ptBenchmark1.X;
            var Y = ptBenchmark2.Y - ptBenchmark1.Y;
            Point ptBenchmarkDistance = new Point(X, Y);

        int iPixelDistanceSquare = ptBenchmarkDistance.X * ptBenchmarkDistance.X + ptBenchmarkDistance.Y * ptBenchmarkDistance.Y;
        if (iPixelDistanceSquare <= 0)
        {
            iPixelDistanceSquare = 1;
        }
        double dPixelDistance = Math.Sqrt((double)iPixelDistanceSquare);

        double dDistancePerPixel = iBenchmarkDistance / dPixelDistance;

        double dAnkleCenterPixelPos = (FrontBodyPosition.GetLeftAnkleX() +
                                                 FrontBodyPosition.GetRightAnkleX()) / 2;
        double dGlabellaPixelOffset = FrontBodyPosition.GetGlabellaX() - dAnkleCenterPixelPos;
        double dEarCenterPixelOffset = (FrontBodyPosition.GetLeftEarX() +
                                                FrontBodyPosition.GetRightEarX()) / 2 - dAnkleCenterPixelPos;
        double dChinPixelOffset = FrontBodyPosition.GetChinX() - dAnkleCenterPixelPos;
        double dShoulderCenterPixelOffset = (FrontBodyPosition.GetLeftShoulderX() +
                                                FrontBodyPosition.GetRightShoulderX()) / 2 - dAnkleCenterPixelPos;
        double dHipCenterPixelOffset = (FrontBodyPosition.GetLeftHipX() +
                                                FrontBodyPosition.GetRightHipX()) / 2 - dAnkleCenterPixelPos;
        double dAnkleCenterPixelOffset = (FrontBodyPosition.GetLeftAnkleX() +
                                                 FrontBodyPosition.GetRightAnkleX()) / 2 - dAnkleCenterPixelPos;

        double dGlabellaCentimeterOffset = (dGlabellaPixelOffset * dDistancePerPixel);
        double dEarCenterCentimeterOffset = (dEarCenterPixelOffset * dDistancePerPixel);
        double dChinCentimeterOffset = (dChinPixelOffset * dDistancePerPixel);
        double dShoulderCenterCentimeterOffset = (dShoulderCenterPixelOffset * dDistancePerPixel);
        double dHipCenterCentimeterOffset = (dHipCenterPixelOffset * dDistancePerPixel);
        double dAnkleCenterCentimeterOffset = (dAnkleCenterPixelOffset * dDistancePerPixel);

        if (dGlabellaCentimeterOffset < 0)
        {
                m_strGlabellaPosition = (dGlabellaCentimeterOffset/1000).ToString() + " cm right";
            }
        else if (dGlabellaCentimeterOffset > 0)
        {
                m_strGlabellaPosition = (dGlabellaCentimeterOffset/1000).ToString() + " cm left";
            }
        else
        {
                m_strGlabellaPosition = "0cm";
            }

        if (dEarCenterCentimeterOffset < 0)
        {
            m_strEarCenterPosition = (dEarCenterCentimeterOffset/1000).ToString() +"cm right";
        }
        else if (dEarCenterCentimeterOffset > 0)
        {
            m_strEarCenterPosition = (dEarCenterCentimeterOffset/1000).ToString() + "cm left";
        }
        else
        {
            m_strEarCenterPosition = "0cm";
        }

        if (dChinCentimeterOffset < 0)
        {
            m_strChinPosition = (dChinCentimeterOffset/1000).ToString() + "cm right";
        }
        else if (dChinCentimeterOffset > 0)
        {
            m_strChinPosition = (dChinCentimeterOffset/1000).ToString() + "cm left";
        }
        else
        {
            m_strChinPosition = "0cm";
        }

        if (dShoulderCenterCentimeterOffset < 0)
        {
            m_strShoulderCenterPosition = (dShoulderCenterCentimeterOffset/1000) + " cm right";
        }
        else if (dShoulderCenterCentimeterOffset > 0)
        {
            m_strShoulderCenterPosition = (dShoulderCenterCentimeterOffset/1000) + " cm left";
        }
        else
        {
            m_strShoulderCenterPosition = "0cm";
        }

        if (dHipCenterCentimeterOffset < 0)
        {
            m_strHipCenterPosition = (dHipCenterCentimeterOffset/1000) +" cm right";
        }
        else if (dHipCenterCentimeterOffset > 0)
        {
                m_strHipCenterPosition = (dHipCenterCentimeterOffset/1000) + " cm left";
            }
        else
        {
            m_strHipCenterPosition = "0cm";
        }

        if (dHipCenterCentimeterOffset < 0)
        {
            m_strHipCenterPosition = (dHipCenterCentimeterOffset/1000) + " cm right";
        }
        else if (dHipCenterCentimeterOffset > 0)
        {
            m_strHipCenterPosition = (dHipCenterCentimeterOffset/1000) + " cm left";
        }
        else
        {
            m_strHipCenterPosition = "0cm";
        }

        if (dAnkleCenterCentimeterOffset < 0)
        {
            m_strAnkleCenterPosition = (dAnkleCenterCentimeterOffset/1000) + "cm right";
        }
        else if (dAnkleCenterCentimeterOffset > 0)
        {
                m_strAnkleCenterPosition = (dAnkleCenterCentimeterOffset/1000) + "cm left";
            }
        else
        {
            m_strAnkleCenterPosition ="0cm";
        }
        m_strAnkleCenterPosition = "";   
        m_strEarBalance = FrontBodyAngle.GetEar() + "deg";
        m_strShoulderBalance = FrontBodyAngle.GetShoulder() + "deg";
        m_strHipBalance = FrontBodyAngle.GetHip() + "deg";
        m_strRightKneeAngle = FrontBodyAngle.GetRightThigh() + "deg";
        m_strLeftKneeAngle = FrontBodyAngle.GetLeftThigh() + "deg";
    }

 public  string GetGlabellaPositionString( )
{
	return m_strGlabellaPosition;
}

public string GetEarCenterPositionString( ) 
{
	return m_strEarCenterPosition;
}

public string GetChinPositionString( ) 
{
	return m_strChinPosition;
}

public string  GetShoulderCenterPositionString( ) 
{
	return m_strShoulderCenterPosition;
}

public  string GetHipCenterPositionString( ) 
{
	return m_strHipCenterPosition;
}

public string GetAnkleCenterPositionString( ) 
{
	return m_strAnkleCenterPosition;
}

public string GetEarBalanceString( ) 
{
	return m_strEarBalance;
}

public string GetShoulderBalanceString( ) 
{
	return m_strShoulderBalance;
}

public string GetHipBalanceString( ) 
{
	return m_strHipBalance;
}

public string GetRightKneeAngleString( ) 
{
	return m_strRightKneeAngle;
}

public string GetLeftKneeAngleString( ) 
{
	return m_strLeftKneeAngle;
}

    }
}
