﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using MySql.Data.MySqlClient;
using QRCoder;

namespace Yugamiru
{
    public partial class Form1 : Forms.BaseForm
    {
        #region Global Variables
        FrontBodyPosition m_FrontBodyPositionStanding,m_FrontBodykneedownPosition;


        //Rectangle rect = new Rectangle(109, 0, 192, 241); - Meenakshi
        Rectangle rect = new Rectangle(89, 0, 192, 241);
        

        bool isMouseDown = false;
        bool isrectmoved = false;
        Image<Bgr, Byte> originalImage;
        Image<Bgr, Byte> currentImage;
        Image<Bgr, Byte> resizedImage;
        Image<Bgr, Byte> flipimage = null;
        Image<Bgr, Byte> sideimage_first;
        Image<Bgr, Byte> uprightimage_first;
        Image<Bgr, Byte> crouchedimage_first;
        Image<Bgr, Byte> resizeduprightimage_first;
        Image<Bgr, Byte> resizedcrouchedimage_first;
        Image<Bgr, Byte> resizedsideimage_first;

        

        int Marker_width = 10; // s= 10, M = 12, L = 14
         

        //circle and rectangle for belt and ankles for imagebox1
        Rectangle circle1 = new Rectangle(170, 350, 10, 10);
        Rectangle circle2 = new Rectangle(250, 350, 10, 10);
        Rectangle rect1 = new Rectangle(170, 250, 5, 5);
        Rectangle rect2 = new Rectangle(250, 250, 5, 5);

        //circle and rectangle for belt and ankles for imagebox2
        Rectangle circle3 = new Rectangle(150, 350, 10, 10);
        Rectangle circle4 = new Rectangle(270, 350, 10, 10);
        Rectangle rect3 = new Rectangle(170, 250, 5, 5);
        Rectangle rect4 = new Rectangle(250, 250, 5, 5);

        //circle for knees for imagebox1
        Rectangle circle5 = new Rectangle(196, 250, 10, 10); // for Leftknees
        Rectangle circle6 = new Rectangle(230, 250, 10, 10); // for Rightknees

        Rectangle circle7 = new Rectangle(189, 300, 10, 10);
        Rectangle circle8 = new Rectangle(239, 300, 10, 10);

        Rectangle circle9 = new Rectangle(189, 400, 10, 10);
        Rectangle circle10 = new Rectangle(239, 400, 10, 10);

        //circle for knees for imagebox2
        Rectangle circle11 = new Rectangle(196, 250, 10, 10); //for Leftknees
        Rectangle circle12 = new Rectangle(230, 250, 10, 10);//for Rightknees

        Rectangle circle13 = new Rectangle(189, 300, 10, 10);
        Rectangle circle14 = new Rectangle(239, 300, 10, 10);

        Rectangle circle15 = new Rectangle(189, 400, 10, 10);
        Rectangle circle16 = new Rectangle(239, 400, 10, 10);

        //circle for ear,shoulders for imagebox1
        Rectangle circle17 = new Rectangle(196, 250, 10, 10);
        Rectangle circle18 = new Rectangle(230, 250, 10, 10);

        Rectangle circle19 = new Rectangle(189, 300, 10, 10);
        Rectangle circle20 = new Rectangle(239, 300, 10, 10);

        Rectangle circle21 = new Rectangle(189, 400, 10, 10);
        Rectangle circle22 = new Rectangle(239, 400, 10, 10);

        Rectangle circle23 = new Rectangle(166, 120, 10, 10);
        Rectangle circle24 = new Rectangle(260, 120, 10, 10);

        Rectangle circle25 = new Rectangle(196, 250, 10, 10);
        Rectangle circle26 = new Rectangle(230, 250, 10, 10);

        Rectangle circle27 = new Rectangle(189, 300, 10, 10);
        Rectangle circle28 = new Rectangle(239, 300, 10, 10);

        Rectangle circle29 = new Rectangle(189, 400, 10, 10);
        Rectangle circle30 = new Rectangle(239, 400, 10, 10);

        Rectangle circle31 = new Rectangle(166, 120, 10, 10);
        Rectangle circle32 = new Rectangle(260, 120, 10, 10);

        //circle for face in imagebox1 paint
        Rectangle circle33 = new Rectangle(210, 20, 10, 10);
        Rectangle circle34 = new Rectangle(210, 100, 10, 10);

        //circle for face in imagebox1  paint
        Rectangle circle35 = new Rectangle(189, 50, 10, 10);
        Rectangle circle36 = new Rectangle(239, 50, 10, 10);

        //circle for face in imagebox2 paint
        Rectangle circle37 = new Rectangle(210, 20, 10, 10);
        Rectangle circle38 = new Rectangle(210, 100, 10, 10);

        //circle for face in imagebox2  paint
        Rectangle circle39 = new Rectangle(189, 50, 10, 10);
        Rectangle circle40 = new Rectangle(239, 50, 10, 10);


        //circle for imagebox3
        Rectangle circle41 = new Rectangle(230, 250, 10, 10);//hip
        Rectangle circle46 = new Rectangle(200, 250, 10, 10);// Left hip
        Rectangle circle47 = new Rectangle(260, 250, 10, 10);//Right hip
        Rectangle circle42 = new Rectangle(239, 300, 10, 10);
        Rectangle circle43 = new Rectangle(239, 400, 10, 10);
        Rectangle circle44 = new Rectangle(166, 120, 10, 10);
        Rectangle circle45 = new Rectangle(210, 20, 10, 10);


        bool firstCircle = false;
        bool secondCircle = false;
        bool firstrect = false;
        bool secondrect = false;
        bool mouseCliked = false;
        bool kneesCircle = false;
        bool kneesCircle1 = false;

        bool facecircle = false;
        bool facecircle1 = false;
        bool facecircle2 = false;
        bool facecircle3 = false;

        bool shoulder = false;
        bool shoulder1 = false;

        bool shoulder_side = false;
        bool ear_side = false;
        bool lefthip_side = false;
        bool righthip_side = false;
        bool patella_side = false;
        bool ankle_side = false;

       string URglabellapos;
        string URearangle;
        string URearpos;
        string URshoulderangle;
        string URchinpos;
        string URhipangle;
        string URshoulderpos;
        string URrightkneeangle;
        string URhippos;
        string URleftkneeangle;
        string Cglabellapos;
        string Cearangle;
        string Cearpos;
        string Cshoulderangle;
        string Cchinpos;
        string Chipangle;
        string Cshoulderpos;
        string Crightkneeangle;
        string Chippos;
        string Cleftkneeangle;

        Bitmap Global_sideImage;//global variable to store processed side image
        Bitmap Global_uprightImage;//global variable to store processed upright image
        Bitmap Global_crouchedImage;//global variable to store processed crouched image

        Image<Bgr, Byte>[] imagePyramid_upright = new Image<Bgr, byte>[6];
        Image<Bgr, Byte>[] imagepyramid_crouched = new Image<Bgr, byte>[6];
        Image<Bgr, Byte>[] imagepyramid_side = new Image<Bgr, byte>[6];
        double[] markerWidth = new double[] { 1.8, 1.6, 1.4, 1.2, 1, 0.8 };

        public string status;
        public string dragiconstatus;

        #endregion
        public Form1()
        {
            base.SetChildFormObject(this);
            try
            {
                m_FrontBodyPositionStanding = new FrontBodyPosition();
                m_FrontBodykneedownPosition = new FrontBodyPosition();

                //Initialize all the controls
                InitializeComponent();

                //Reset all the controls with default values
                SetControls();

                // uploadsideview.Image = Yugamiru.Properties.Resources.sokui;
                //Rectangle rect = new Rectangle((uploadsideview.Width / 2 - 192 / 2), 0, 192, 241); -- Meenakshi

                float f1 = uploadsideview.Width / 2;
                float f2 = f1 - (rect.Width / 2);

                rect.X = (int)f2;


                sideimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.sokui);
                pictureBox1.Image = sideimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();
                //originalImage = sideimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);
                sideimage_first = sideimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);
                uploadsideview.Image = sideimage_first;
                resizedsideimage_first = sideimage_first;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(startinitial.Image);

            startinitial.Image = Yugamiru.Properties.Resources.startgreen_down;
            //startbutton.Image = Yugamiru.Properties.Resources.startgreen_down;
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(startinitial.Image);
            startinitial.Image = Yugamiru.Properties.Resources.startgreen_up;

            // startbutton.Image = Yugamiru.Properties.Resources.startgreen_up;
        }



        private void quit_click(object sender, EventArgs e)
        {
            this.Close();

        }


      
        private void pictureBox4_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(quitbutton.Image);
            quitbutton.Image = Yugamiru.Properties.Resources.end_down;

        }

        private void returnbutton_Click(object sender, EventArgs e)
        {

            tabControl1.SelectedIndex = 0;

            // setting controls to null
            disposeMemory(startinitial.Image);
            disposeMemory(returnbutton.Image);
            disposeMemory(nextbutton.Image);

            mainpage.Image = Yugamiru.Properties.Resources.Mainpic;
            startinitial.Image = Yugamiru.Properties.Resources.startgreen_on;
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_up;
            quitapp.Image = Yugamiru.Properties.Resources.end_up;



        }

        private void nextbutton_Click(object sender, EventArgs e)
        {
            String gendervariable, yearvariable, monthvariable, dayvariable;
            tabControl1.SelectedIndex = 3;
            useridlabel.Text = user_ID.Text;
            usernamelabel.Text = user_Name.Text;

            gendervariable = Gender.GetItemText(Gender.SelectedItem);
            if (gendervariable == "MALE")
                genderlabel.Text = "M";
            else if (gendervariable == "FEMALE")
                genderlabel.Text = "F";
            else
                genderlabel.Text = "-";

            yearvariable = Year.Value.ToString();
            monthvariable = Month.GetItemText(Month.SelectedItem);
            dayvariable = Day.GetItemText(Day.SelectedItem);
            dateofbirthlabel.Text = dayvariable + "." + monthvariable + "." + yearvariable;

            heightlabel.Text = numericUpDown1.Value.ToString();


            // setting resources to picturebox
            Loadimage.Image = Yugamiru.Properties.Resources.Mainpic3;
            returnimage.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            openimage.Image = Yugamiru.Properties.Resources.imageload_down;
            rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
            nextimage.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
            Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechSideStanding;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }



        private void button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;

        }

        private void returnfromsideimage_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }

        private void nextfromside_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 4;

        }

        private void enterfromupright_click(object sender, EventArgs e)
        {
            nextimage.Visible = true;
            //rect.x = 88 because X value changes when sokui.bmp image loaded in default  
            /*      if (resizedImage == null && isrectmoved == true)// checking whether we are using default image
                  {

                      pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                      sideimage_first = (Image<Bgr, Byte>)uploadsideview.Image;
                      sideimage_first.ROI = new Rectangle(rect.X - uploadsideview.Image.Size.Width / 2, rect.Y, uploadsideview.Image.Size.Width, rect.Height);
                      //pictureBox1.Image = sideimage_first.ToBitmap();
                      //sideimage_first.ToBitmap().SetResolution(currentImage.ToBitmap().HorizontalResolution, currentImage.ToBitmap().VerticalResolution);
                      // pictureBox1.Image = sideimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();
                      pictureBox1.Image = originalImage.ToBitmap();
                  }
                  else if (resizedImage != null)
                  {
                      pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                      if (isrectmoved == false)
                          pictureBox1.Image = currentImage.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR, false).ToBitmap();
                      else
                      {
                          resizedImage.ROI = new Rectangle(rect.X - uploadsideview.Image.Size.Width / 2, rect.Y, uploadsideview.Image.Size.Width, rect.Height);
                          resizedImage.ToBitmap().SetResolution(currentImage.ToBitmap().HorizontalResolution, currentImage.ToBitmap().VerticalResolution);
                          pictureBox1.Image = resizedImage.ToBitmap();

                          //pictureBox1.Image = resizedImage.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();
                      }
                  }*/

            if (resizedImage != null) // start line - Meenakshi
            {
                var image = new Bitmap(uploadsideview.Image.Bitmap);

                var imageStartingPoint = (uploadsideview.Width / 2) - (uploadsideview.Image.Size.Width / 2); //getting image starting point
                var imageendingpoint = imageStartingPoint + uploadsideview.Image.Size.Width;
                var selectionRectangleStartingPoint = rect.X; //getting selection rectangle starting point
                var selectionRectangleEndingPoint = selectionRectangleStartingPoint + rect.Width; //getting selection rectangle ending point



                //Condition 1 towards left
                if (selectionRectangleStartingPoint < imageStartingPoint)
                {
                    var t = imageStartingPoint - selectionRectangleStartingPoint; //leftoutbundportion
                                                                                   //var rightoutboundportion = selectionReactangleEndingPoint - imageendingpoint;

                    /*     pictureBox2.Image = image.Clone(new Rectangle(0, 0, selectionReactangle.Width - t, 161),
                    System.Drawing.Imaging.PixelFormat.Undefined);*/
                 /*   if (selectionRectangleEndingPoint > imageendingpoint)
                        pictureBox1.Image = image.Clone(new Rectangle(0, 0, uploadsideview.Image.Size.Width, 161),
                            System.Drawing.Imaging.PixelFormat.Undefined);
                    else
                        pictureBox1.Image = image.Clone(new Rectangle(0, 0, rect.Width - t, 161),
                   System.Drawing.Imaging.PixelFormat.Undefined);*/



                    decimal imageWidthRatio = (decimal)originalImage.Width / (decimal)uploadsideview.Image.Size.Width;
                    try
                    {
                        if (selectionRectangleEndingPoint > imageendingpoint)
                        {
                            var newImage = originalImage.Copy(new Rectangle(0, 0, (int)((pictureBox1.Image.Width) * imageWidthRatio),
                              originalImage.Height));

                            // var newImage = originalImage.Clone(new Rectangle(0,0,pictureBox1.Image.Width,161), System.Drawing.Imaging.PixelFormat.Undefined);
                            
                            pictureBox1.Image = newImage
                                .Resize(ImageSizeWithAspectRatio(newImage.Size.Width, 
                                newImage.Size.Height, 450), 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).ToBitmap();
                                
                        }
                        else
                        {
                            var newImage = originalImage.Copy(new Rectangle(0, 0, (int)((rect.Width - t) * imageWidthRatio), originalImage.Height));

                            pictureBox1.Image = newImage
                                .Resize(ImageSizeWithAspectRatio(newImage.Width, newImage.Height, 450), 450,
                            Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).ToBitmap();

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
                //Condition 2
                else if (imageStartingPoint == selectionRectangleStartingPoint)
                {
                   /* pictureBox1.Image = image
                    .Clone(new Rectangle(0, 0, selectionReactangle.Width, 161),
                    System.Drawing.Imaging.PixelFormat.Undefined);*/

                    decimal imageWidthRatio = (decimal)originalImage.Width / (decimal)uploadsideview.Image.Size.Width;

                    var newImage = originalImage
                        .Copy(new Rectangle(0, 0, (int)(rect.Width * imageWidthRatio),originalImage.Height));

                    //pictureBox2.Image = newImage.GetThumbnailImage(400, 470, null, IntPtr.Zero);

                    /* pictureBox2.Image = newImage
                         .GetThumbnailImage(400, ImageSizeWithAspectRatio(newImage.Width, newImage.Height, 400),
                     null, IntPtr.Zero);--Meenakshi*/
                    pictureBox1.Image = newImage
                                .Resize(ImageSizeWithAspectRatio(newImage.Width, newImage.Height, 450), 450,
                            Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).ToBitmap();

                }
                //Condition 3
                else if (selectionRectangleStartingPoint > imageStartingPoint)
                {
                    var t = selectionRectangleStartingPoint - imageStartingPoint;
                    var t1 = t + 200 - 10;
                    /*   if (selectionReactangleEndingPoint > imageendingpoint)
                        {

                            pictureBox2.Image = image
                   .Clone(new Rectangle(t, 0, (pictureBox1.Image.Width - t), 161),
                   System.Drawing.Imaging.PixelFormat.Undefined);


                        }
                        else
                        {
                            pictureBox2.Image = image
                        .Clone(new Rectangle(t, 0, selectionReactangle.Width, 161),
                        System.Drawing.Imaging.PixelFormat.Undefined);
                        }
                        */

                    decimal imageWidthRatio = (decimal)originalImage.Width / (decimal)uploadsideview.Image.Size.Width;
                    if (selectionRectangleEndingPoint > imageendingpoint)
                    {
                        var newImage = originalImage
                   .Copy(new Rectangle((int)(t * imageWidthRatio), 0, (int)((uploadsideview.Image.Size.Width - t) * imageWidthRatio),
                   originalImage.Height));

                        pictureBox1.Image = newImage
                                 .Resize(ImageSizeWithAspectRatio(newImage.Width, newImage.Height, 450), 450,
                             Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).ToBitmap();
                    }
                    else
                    {
                        var newImage = originalImage
                    .Copy(new Rectangle((int)(t * imageWidthRatio), 0, (int)((rect.Width - t) * imageWidthRatio),
                    originalImage.Height));

                        pictureBox1.Image = newImage
                                .Resize(ImageSizeWithAspectRatio(newImage.Width, newImage.Height, 450), 450,
                            Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).ToBitmap();
                    }


                }

                /*
              var imageSelectedArea = selectionReactangle.Width -
                  (selectionReactangleStratingPoint - imageStartingPoint);

              pictureBox2.Image = image
                  .Clone(new Rectangle(imageSelectedArea, 0, selectionReactangle.Width, 161), 
                  System.Drawing.Imaging.PixelFormat.Undefined);*/
            } // end line - Meenakshi


        }
        public int ImageSizeWithAspectRatio(int originalWidth, int originalHeight, int newHeight)
        {
            decimal aspectRatio = (decimal)originalWidth / (decimal)originalHeight;
            var newWidth = aspectRatio * newHeight;
            return (int)newWidth;
        }

        private void nextimage_click(object sender, EventArgs e)
        {
            switch (status)
            {
                case "side":
                    Speechimagetext.Visible = true;
                    disposeMemory(Speechimagetext.Image);
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
                    nextimage.Visible = false;
                    Global_sideImage = new Bitmap(pictureBox1.Image); // setting global side image to access in markers pages
                    resizedsideimage_first = (Image<Bgr, Byte>)uploadsideview.Image;

                    //Setting default image
                    uprightimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui);
                    pictureBox1.Image = uprightimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();

                    resizeduprightimage_first = uprightimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);
                    //resizedImage = sideimage_first;
                    uploadsideview.Image = resizeduprightimage_first;
                    sideimage_first = resizeduprightimage_first;
                    //rect.X = 88;//  to centre the rectangle while moving next page 

                    status = "upright"; //updating status for next page
                    break;

                case "upright":
                    disposeMemory(Speechimagetext.Image);
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
                    nextimage.Visible = false;
                    Global_uprightImage = new Bitmap(pictureBox1.Image);// setting global upright image to access in markers pages
                    resizeduprightimage_first = (Image<Bgr, Byte>)uploadsideview.Image;

                    //Setting default image
                    crouchedimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui);
                    pictureBox1.Image = crouchedimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();
                    originalImage = crouchedimage_first.Clone();
                    resizedcrouchedimage_first = crouchedimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);
                    //resizedImage = sideimage_first;
                    uploadsideview.Image = resizedcrouchedimage_first;
                    sideimage_first = resizedcrouchedimage_first;

                    // rect.X = 88;//  to centre the rectangle while moving next page 


                    status = "crouched";  //updating status for next page
                    break;

                case "crouched":
                    Global_crouchedImage = new Bitmap(pictureBox1.Image);
                    resizedcrouchedimage_first = (Image<Bgr, Byte>)uploadsideview.Image;

                   imageBox1.Image = new Image<Bgr, Byte>(Global_uprightImage).Resize(430, 472, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                   // imageBox1.Image = new Image<Bgr, byte>(Global_uprightImage).Resize(5, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);

                    /* -- Meenakshi

                                        //
                                        Image<Bgr, byte> test = new Image<Bgr, Byte>(Global_uprightImage);//.Resize(ImageSizeWithAspectRatio(Global_uprightImage.Width, Global_uprightImage.Height, 430), 472,Emgu.CV.CvEnum.INTER.CV_INTER_AREA);

                                          //Setting image pyramids
                                          imagePyramid_upright[5] = test;
                                          imagePyramid_upright[4] = test
                                              //.Resize(1.25, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                              .Resize(1.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                        imagePyramid_upright[3] = test
                                              //.Resize(1.50, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                              .Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                        imagePyramid_upright[2] = test
                                              //.Resize(1.75, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                              .Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                        imagePyramid_upright[1] = test
                                              //.Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                              .Resize(3, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                        imagePyramid_upright[0] = test
                                              //.Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                                              .Resize(3.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                                        imageBox1.Image = imagePyramid_upright[0];

                    */


                    imageBox2.Image = new Image<Bgr, Byte>(Global_crouchedImage).Resize(430, 472, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                    //imageBox2.Image = new Image<Bgr, byte>(Global_crouchedImage).Resize(5, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                    /*   Image<Bgr, byte> test1 = new Image<Bgr, Byte>(Global_crouchedImage);

                       //setting image pyramids
                       imagepyramid_crouched[5] = test1;
                       imagepyramid_crouched[4] = test1
                           .Resize(1.25, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                       imagepyramid_crouched[3] = test1
                           .Resize(1.50, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                       imagepyramid_crouched[2] = test1
                           .Resize(1.75, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                       imagepyramid_crouched[1] = test1
                           .Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                       imagepyramid_crouched[0] = test1
                           .Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                       imageBox2.Image = imagepyramid_crouched[0];*/

                    //To zoom circle and rectangle For Imagebox1 ------

                    
              /*      circle1 = new Rectangle(150, 300, 10 ,10 );

                           rect1 = new Rectangle(100, 150, (int)(5 * markerWidth[0]),
                               (int)(5 * markerWidth[0]));

                           circle2 = new Rectangle(250, 300, (int)(10 * markerWidth[0]),
                               (int)(10 * markerWidth[0]));

                           rect2 = new Rectangle(300, 150, (int)(5 * markerWidth[0]),
                               (int)(5 * markerWidth[0]));*/
                    //------------------------------------------

                    //To Zoom circle and rectangle For Imagebox2
                    /*  circle3 = new Rectangle(150, 300, (int)(10 * markerWidth[0]),
                          (int)(10 * markerWidth[0]));

                      rect3 = new Rectangle(100, 150, (int)(5 * markerWidth[0]),
                          (int)(5 * markerWidth[0]));

                      circle4 = new Rectangle(250, 300, (int)(10 * markerWidth[0]),
                          (int)(10 * markerWidth[0]));

                      rect4 = new Rectangle(300, 150, (int)(5 * markerWidth[0]),
                          (int)(5 * markerWidth[0]));*/
                    //---------------------------------------------------


                    // setting global crouched image to accesss in markers pages
                    tabControl1.SelectedIndex = 4;

                    // Setting picturebox to images

                    disposeMemory(beltanklescreen.Image);
                    disposeMemory(returndragiconscreen.Image);
                    disposeMemory(nextdragiconscreen.Image);
                    beltanklescreen.Image = Yugamiru.Properties.Resources.Mainpic4;

                    returndragiconscreen.Image = Yugamiru.Properties.Resources.gobackgreen_down;

                    nextdragiconscreen.Image = Yugamiru.Properties.Resources.gonextgreen_on;
                    break;
                default:
                    break;

            }
        }

        private void return_click(object sender, EventArgs e)
        {


            switch (status)
            {
                case "side":
                    tabControl1.SelectedIndex = 1;

                    // setting picturebox to null values
                    disposeMemory(Loadimage.Image);
                    disposeMemory(returnimage.Image);
                    disposeMemory(openimage.Image);
                    disposeMemory(rotateimage.Image);
                    disposeMemory(nextimage.Image);
                    disposeMemory(enteruprightimage.Image);
                    disposeMemory(Speechimagetext.Image);


                    // setting controls to initial page

                    startinitialscreen.Image = Yugamiru.Properties.Resources.Mainpic2;
                    returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                    nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;


                    break;
                case "upright":
                    Speechimagetext.Visible = false;
                    status = "side";
                    pictureBox1.Image = Global_sideImage;

                    /* sideimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui);
                     sideimage_first = sideimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);*/
                    uploadsideview.Image = resizedsideimage_first;
                    break;
                case "crouched":
                    status = "upright";
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
                    pictureBox1.Image = Global_uprightImage; //setting upright image
                    uploadsideview.Image = resizeduprightimage_first;

                    /*sideimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui);
                    sideimage_first = sideimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);*/
                    // uploadsideview.Image = sideimage_first;
                    break;
                default:
                    break;
            }
        }

        private void returndragicon_click(object sender, EventArgs e)
        {
            switch (dragiconstatus)
            {
                case "belt":
                    status = "crouched";
                    tabControl1.SelectedIndex = 3;

                    disposeMemory(Loadimage.Image);
                    disposeMemory(returnimage.Image);
                    disposeMemory(openimage.Image);
                    disposeMemory(rotateimage.Image);
                    disposeMemory(nextimage.Image);
                    disposeMemory(enteruprightimage.Image);
                    disposeMemory(Speechimagetext.Image);

                    // setting resources to picturebox
                    Loadimage.Image = Yugamiru.Properties.Resources.Mainpic3;
                    returnimage.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                    openimage.Image = Yugamiru.Properties.Resources.imageload_down;
                    rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
                    nextimage.Image = Yugamiru.Properties.Resources.gonextgreen_down;
                    enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;

                    // Setting picturebox to images

                    beltanklescreen.Image = null;
                    returndragiconscreen.Image = null;
                    nextdragiconscreen.Image = null;
                    break;
                case "knees":
                    dragiconstatus = "belt";
                    //labelforiconsscreen.Text = "Drag icons to belt, ankles.";
                    labelforiconsscreen.Text = Yugamiru.Properties.Resources.BELT_ANKLE_MODE;
                    break;
                case "shoulder":
                    dragiconstatus = "knees";
                    //labelforiconsscreen.Text = "Drag icons to knees.";
                    labelforiconsscreen.Text = Yugamiru.Properties.Resources.KNEES_MODE;
                    break;
                default:
                    break;
            }

        }

        private void nextdragicon_Click(object sender, EventArgs e)
        {

            switch (dragiconstatus)
            {
                case "belt":
                    dragiconstatus = "knees";
                   /* LeftAnkleX = circle1.X;
                    LeftAnkleY = circle1.Y;
                    RightAnkleX = circle2.X;
                    RightAnkleY = circle2.Y;*/

                    /* m_FrontBodyPositionStanding.m_ptLeftAnkle.X = circle1.X;
                     m_FrontBodyPositionStanding.m_ptLeftAnkle.Y = circle1.Y; 
                     m_FrontBodyPositionStanding.m_ptRightAnkle.X = circle2.X;
                     m_FrontBodyPositionStanding.m_ptRightAnkle.Y = circle2.Y;*/

                    m_FrontBodyPositionStanding.m_ptLeftAnkle = new Point(circle1.X, circle1.Y);
                    m_FrontBodyPositionStanding.m_ptRightAnkle = new Point(circle2.X, circle2.Y);  
                    
                    m_FrontBodyPositionStanding.m_ptLeftBelt = new Point(rect1.X, rect1.Y);
                    m_FrontBodyPositionStanding.m_ptRightBelt = new Point(rect2.X, rect2.Y);

                    m_FrontBodykneedownPosition.m_ptLeftAnkle = new Point(circle3.X, circle3.Y);
                    m_FrontBodykneedownPosition.m_ptRightAnkle = new Point(circle4.X, circle4.Y);

                    m_FrontBodykneedownPosition.m_ptLeftBelt = new Point(rect3.X, rect3.Y);
                    m_FrontBodykneedownPosition.m_ptRightBelt = new Point(rect4.X, rect4.Y);


                    imageBox1.Invalidate();
                    imageBox1.Update();
                    imageBox2.Invalidate();
                    imageBox2.Update();
                    //labelforiconsscreen.Text = "Drag icons to knees.";
                    labelforiconsscreen.Text = Yugamiru.Properties.Resources.KNEES_MODE;
                    break;
                case "knees":
                    dragiconstatus = "shoulder";

                    m_FrontBodyPositionStanding.m_ptLeftKnee = new Point(circle7.X, circle7.Y);
                    m_FrontBodyPositionStanding.m_ptRightKnee = new Point(circle8.X, circle8.Y);

                    m_FrontBodyPositionStanding.m_ptLeftHip = new Point(circle5.X, circle5.Y);
                    m_FrontBodyPositionStanding.m_ptRightHip = new Point(circle6.X, circle6.Y);

                    m_FrontBodykneedownPosition.m_ptLeftKnee = new Point(circle13.X, circle13.Y);
                    m_FrontBodykneedownPosition.m_ptRightKnee = new Point(circle14.X, circle14.Y);

                    m_FrontBodykneedownPosition.m_ptLeftHip = new Point(circle11.X, circle11.Y);
                    m_FrontBodykneedownPosition.m_ptRightHip = new Point(circle12.X, circle12.Y);

                    imageBox1.Invalidate();
                    imageBox1.Update();
                    imageBox2.Invalidate();
                    imageBox2.Update();
                    //labelforiconsscreen.Text = "Drag icons to shoulders, ears, etc.";
                    labelforiconsscreen.Text = Yugamiru.Properties.Resources.SHOULDER_EARS_MODE;
                    break;
                case "shoulder":
                    tabControl1.SelectedIndex = 5;
                    //MessageBox.Show((circle37.X).ToString());

                   /* GlabellaX = circle37.X;
                    GlabellaY = circle37.Y;*/
                    /* m_FrontBodyPositionStanding.m_ptGlabella.X =circle37.X;
                     m_FrontBodyPositionStanding.m_ptGlabella.Y = circle37.Y;*/
                    m_FrontBodyPositionStanding.m_ptGlabella = new Point(circle33.X, circle33.Y);
                    m_FrontBodyPositionStanding.m_ptChin = new Point(circle34.X, circle34.Y);
                    m_FrontBodyPositionStanding.m_ptLeftEar = new Point(circle35.X, circle35.Y);
                    m_FrontBodyPositionStanding.m_ptRightEar = new Point(circle36.X, circle36.Y);
                    m_FrontBodyPositionStanding.m_ptLeftShoulder = new Point(circle23.X, circle23.Y);
                    m_FrontBodyPositionStanding.m_ptRightShoulder = new Point(circle24.X, circle24.Y);

                    m_FrontBodykneedownPosition.m_ptGlabella = new Point(circle37.X, circle37.Y);
                    m_FrontBodykneedownPosition.m_ptChin = new Point(circle38.X, circle38.Y);
                    m_FrontBodykneedownPosition.m_ptLeftEar = new Point(circle39.X, circle39.Y);
                    m_FrontBodykneedownPosition.m_ptRightEar = new Point(circle40.X, circle40.Y);
                    m_FrontBodykneedownPosition.m_ptLeftShoulder = new Point(circle31.X, circle31.Y);
                    m_FrontBodykneedownPosition.m_ptRightShoulder = new Point(circle32.X, circle32.Y);





                    imageBox3.Image = new Image<Bgr, Byte>(Global_sideImage).Resize(419, 455, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                    Image<Bgr, byte> test3 = new Image<Bgr, Byte>(Global_sideImage);

                    //Setting image pyramids
                    imagepyramid_side[5] = test3;
                    imagepyramid_side[4] = test3
                        .Resize(1.25, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_side[3] = test3
                        .Resize(1.50, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_side[2] = test3
                        .Resize(1.75, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_side[1] = test3
                        .Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_side[0] = test3
                        .Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                   // imageBox3.Image = imagepyramid_side[0]; -Meenakshi

                    disposeMemory(sidescreen.Image);
                    disposeMemory(returnsideicon.Image);
                    disposeMemory(Finish.Image);


                    // setting picturebox with resources
                    sidescreen.Image = Yugamiru.Properties.Resources.Mainpic5;
                    returnsideicon.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                    Finish.Image = Yugamiru.Properties.Resources.completered_down;
                    labelforsideicon.Text = Yugamiru.Properties.Resources.SIDE_VIEW_MODE;

                    break;
                default:
                    break;
            }

        }



        private void returnsideicon_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 4;

            // setting picturebox with null values
            disposeMemory(sidescreen.Image);
            disposeMemory(returnsideicon.Image);
            disposeMemory(Finish.Image);

            disposeMemory(beltanklescreen.Image);
            disposeMemory(returndragiconscreen.Image);
            disposeMemory(nextdragiconscreen.Image);


            beltanklescreen.Image = Yugamiru.Properties.Resources.Mainpic4;
            returndragiconscreen.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextdragiconscreen.Image = Yugamiru.Properties.Resources.gonextgreen_on;

        }

        private void Finish_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 6;

            disposeMemory(finalscreen.Image);
            disposeMemory(editid.Image);
            disposeMemory(changebutton.Image);
            disposeMemory(checkpositionbutton.Image);
            disposeMemory(showreportbutton.Image);
            disposeMemory(printreportbutton.Image);
            disposeMemory(saveresultbutton.Image);
            disposeMemory(restartbutton.Image);
            disposeMemory(initialscreenbutton.Image);

            finalscreen.Image = Yugamiru.Properties.Resources.Mainpic6;
            editid.Image = Yugamiru.Properties.Resources.namechange_on;
            changebutton.Image = Yugamiru.Properties.Resources.imagechange_up;
            checkpositionbutton.Image = Yugamiru.Properties.Resources.jointedit_up;
            showreportbutton.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            printreportbutton.Image = Yugamiru.Properties.Resources.reportprint_up;
            saveresultbutton.Image = Yugamiru.Properties.Resources.datasave_up;
            restartbutton.Image = Yugamiru.Properties.Resources.startred_up;
            initialscreenbutton.Image = Yugamiru.Properties.Resources.returnstart_up;

            imageBox4.Image = new Image<Bgr, Byte>(Global_uprightImage).Resize(305, 380, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
            imageBox5.Image = new Image<Bgr, byte>(Global_crouchedImage).Resize(305, 380, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);

            Image<Bgr, byte> test = new Image<Bgr, Byte>(Global_uprightImage).Resize(305,380,Emgu.CV.CvEnum.INTER.CV_INTER_AREA);

            //Setting image pyramids
            imagePyramid_upright[5] = test;
            imagePyramid_upright[4] = test
                .Resize(1.25, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            imagePyramid_upright[3] = test
                .Resize(1.50, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            imagePyramid_upright[2] = test
                .Resize(1.75, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            imagePyramid_upright[1] = test
                .Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            imagePyramid_upright[0] = test
                .Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

           // imageBox4.Image = imagePyramid_upright[0]; -- Meenakshi


            //imageBox2.Image = new Image<Bgr, Byte>(Global_crouchedImage).Resize(430, 472, Emgu.CV.CvEnum.INTER.CV_INTER_AREA); 
            Image<Bgr, byte> test1 = new Image<Bgr, Byte>(Global_crouchedImage).Resize(305,380, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);

            //setting image pyramids
            imagepyramid_crouched[5] = test1;
            imagepyramid_crouched[4] = test1
                .Resize(1.25, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            imagepyramid_crouched[3] = test1
                .Resize(1.50, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            imagepyramid_crouched[2] = test1
                .Resize(1.75, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            imagepyramid_crouched[1] = test1
                .Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            imagepyramid_crouched[0] = test1
                .Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

            //imageBox5.Image = imagepyramid_crouched[0]; --- Meenakshi

            MyData MydataObj = new MyData();
            CCalibInf m_CalibInf = new CCalibInf();
            CCalcPostureProp m_CalcPostureProp = new CCalcPostureProp();

            MydataObj.InitBodyBalance(1024, 1280, m_CalibInf, m_CalcPostureProp);

        }

        private void comboBox2_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var selectedLanguage = comboBox2.SelectedItem.ToString();

            switch (selectedLanguage)
            {
                case "Japanese":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ja-JP");
                    break;
                case "Thai":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("th-TH");
                    break;
                default:
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en");
                    break;
            }

            //Reload all the controls
            SetControls();
            //Finally refresh the parent form
            Refresh();
        }


        private void startinitial_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;

            disposeMemory(startinitialscreen.Image);
            disposeMemory(returnbutton.Image);
            disposeMemory(nextbutton.Image);


            // setting controls
            startinitialscreen.Image = Yugamiru.Properties.Resources.Mainpic2;
            returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;


        }

        private void quitapp_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void settingsbutton_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
            setting_screen.Image = Yugamiru.Properties.Resources.Mainpic7;

        }

        private void initialscreenbutton_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void openimage_Click(object sender, EventArgs e)
        {
            // open file dialog 
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "Desktop";
            open.FilterIndex = 2;
            open.RestoreDirectory = true;
            open.Title = "Open Image";

            // image filters
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    //uploadimage = new Bitmap(open.FileName);
                    currentImage = new Image<Bgr, Byte>(open.FileName);
                     originalImage = currentImage.Clone();

                    // to Resize image
                    // ImageResize(currentImage);

                    int oriWidth = currentImage.Size.Width;
                    int oriHeight = currentImage.Size.Height;

                    // To preserve the aspect ratio
                    float ratioX = (float)192 / (float)oriWidth;
                    float ratioY = (float)241 / (float)oriHeight;
                    float ratio = Math.Min(ratioX, ratioY);
                    

                    // New width and height based on aspect ratio
                    int newWidth = (int)(oriWidth * ratio);
                    int newHeight = (int)(oriHeight * ratio);

                    // Convert other formats (including CMYK) to RGB.
                    Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);
                    // Draws the image in the specified size with quality mode set to HighQuality
                    newImage.SetResolution(currentImage.ToBitmap().HorizontalResolution, currentImage.ToBitmap().VerticalResolution);


                    using (Graphics graphics = Graphics.FromImage(newImage))
                    {
                        graphics.CompositingQuality = CompositingQuality.HighQuality;
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphics.SmoothingMode = SmoothingMode.HighQuality;
                        graphics.DrawImage(currentImage.ToBitmap(), 0, 0, newWidth, newHeight);
                        graphics.Dispose();
                    }
                    //newImage.Dispose();



                    //uploadimage = resizeimage(uploadimage);  

                    //currentImage = currentImage.Resize(newWidth, newHeight, Emgu.CV.CvEnum.INTER.CV_INTER_AREA,false);
                    //resizedImage = currentImage.Clone();

                    uploadsideview.Image = new Image<Bgr, Byte>(newImage);
                    resizedImage = new Image<Bgr, Byte>(newImage).Clone();
                    // uploadsideview.Image = currentImage;

                    var originalHeight = 450 / 2;
                    var newHeightcopy = originalHeight - (uploadsideview.Image.Size.Height / 2);
                    uploadsideview.Location = new Point(2, newHeightcopy);


                    uploadsideview.Height = uploadsideview.Image.Size.Height + 1;
                    rect.Height = uploadsideview.Image.Size.Height;
                    rect.Width = uploadsideview.Image.Size.Width;
                    if (rect.Height < rect.Width)
                    {

                        // rect.Width = rect.Height / 2;
                        int aspect = rect.Width / rect.Height;
                        rect.Width = rect.Height / aspect;

                    }

                    float f1 = uploadsideview.Width / 2;
                    float f2 = f1 - (rect.Width / 2);

                    float f3 = uploadsideview.Height / 2;
                    float f4 = f3 - (rect.Height / 2);

                    rect.X = (int)f2;
                    rect.Y = (int)f4 - 1;
                    

                    isrectmoved = false;

                }
                catch (Exception ex)
                {

                }

                open.Dispose();
            }
        }

        

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            // Create pen.
            Pen redPen = new Pen(Color.Red, 1);

            e.Graphics.DrawRectangle(redPen, rect);
            redPen.Dispose();

        }

        private void uploadsideview_mousemove(object sender, MouseEventArgs e)
        {
            // MessageBox.Show("hi");
            if (isMouseDown == true)
            {
                isrectmoved = true;
                rect.Location = e.Location;
                // rect.Height = uploadsideview.Image == null ? 240 : uploadsideview.Height;

                // rect.Height = 242;
                rect.Height = uploadsideview.Height - 1;
                if (rect.Right > uploadsideview.Width)
                {
                    rect.X = (uploadsideview.Width - rect.Width) - 1;

                }
                if (rect.Top < 0)
                {
                    rect.Y = 0;
                }
                if (rect.Left < 0)
                {
                    rect.X = 0;

                }
                if (rect.Bottom > uploadsideview.Height)
                {
                    rect.Y = 0;
                }
                Refresh();
            }

         

        }

        private void uploadsideview_mouseup(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        private void uploadsideview_mousedown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;

        }

        private void rotateimage_Click(object sender, EventArgs e)
        {
            //uploadsideview.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            Image<Bgr, Byte> flipimage = (Image<Bgr, Byte>)uploadsideview.Image;
            flipimage = flipimage.Rotate(90, new Bgr(255, 255, 255), false);

            /*  if (flipimage == null)
                  flipimage = resizedImage.Rotate(90, new Bgr(255, 255, 255), false);
              else
                  flipimage = flipimage.Rotate(90, new Bgr(255, 255, 255), false);*/

            uploadsideview.Image = flipimage;
        }

        private void openresultbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(openresultbutton.Image);
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_down;
        }

        private void openresultbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(openresultbutton.Image);
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_on;

        }



        /// <summary>
        /// To reset all the controls with the default values
        /// </summary>
        /// <remarks>
        /// While updating the language culture with a selected language, 
        /// this method needs to be called to reset all the control images, so the images will be updated 
        /// with the selected language images
        /// </remarks>
        private void SetControls()
        {
            //Default Labels
            status = "side";
            dragiconstatus = "belt";
            //labelforiconsscreen.Text = "Drag icons to belt, ankles.";
            labelforiconsscreen.Text = Yugamiru.Properties.Resources.BELT_ANKLE_MODE;
            //labelforsideicon.Text = "Drag each icons in side view.";
            labelforiconsscreen.Text = Yugamiru.Properties.Resources.SIDE_VIEW_MODE;

            //Reset dropdown selected values
            Gender.SelectedIndex = 0;
            Month.SelectedIndex = 0;
            Day.SelectedIndex = 0;
            

            disposeMemory(mainpage.Image);
            disposeMemory(startinitial.Image);
            disposeMemory(openresultbutton.Image);
            disposeMemory(quitapp.Image);

            disposeMemory(minimumbeltankles.Image);
            disposeMemory(maximumbeltankles.Image);
            disposeMemory(originalsizebeltankles.Image);

            disposeMemory(minimizesideview.Image);
            disposeMemory(maximizesideview.Image);
            disposeMemory(originalsizesideview.Image);

            disposeMemory(minimizefinalscreen.Image);
            disposeMemory(maximizefinalscreen.Image);
            disposeMemory(originalsizefinalscreen.Image);


            //Default control images
            mainpage.Image = Yugamiru.Properties.Resources.Mainpic;
            startinitial.Image = Yugamiru.Properties.Resources.startgreen_on;
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_up;
            quitapp.Image = Yugamiru.Properties.Resources.end_up;
            settingsbox.Image = Yugamiru.Properties.Resources.setting_on;


            minimumbeltankles.Image = Yugamiru.Properties.Resources.mag1_down;
            maximumbeltankles.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizebeltankles.Image = Yugamiru.Properties.Resources.mag3_down;

            minimizesideview.Image = Yugamiru.Properties.Resources.mag1_down;
            maximizesideview.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizesideview.Image = Yugamiru.Properties.Resources.mag3_down;
            
            minimizefinalscreen.Image = Yugamiru.Properties.Resources.mag1_down;
            maximizefinalscreen.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizefinalscreen.Image = Yugamiru.Properties.Resources.mag3_down;

            labelforiconsscreen.Text = Properties.Resources.BELT_ANKLE_MODE;

        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            //var selectedValue = e.NewValue / 2;
            double selectedValue = 0.0;
            if (e.NewValue == 1)
                selectedValue = 5;
            else if (e.NewValue == 2)
                selectedValue = 4;
            else if (e.NewValue == 3)
                selectedValue = 3;
            else if (e.NewValue == 4)
                selectedValue = 2;
            else if (e.NewValue == 5)
                selectedValue = 1;
             double oldwidth = imageBox1.Image.Size.Width;
             double oldheight = imageBox1.Image.Size.Height;

            
            imageBox1.SetZoomScale(selectedValue, new Point(220, 220));
            imageBox2.SetZoomScale(selectedValue, new Point(220, 220)); 
            

            /*  Rectangle oldcircle1 = circle1;
             Rectangle oldcircle2 = circle2;

             Rectangle oldrect1 = rect1;
             Rectangle oldrect2 = rect2;

             Rectangle oldcircle3 = circle3;
             Rectangle oldcircle4 = circle4;

             Rectangle oldrect3 = rect3;
             Rectangle oldrect4 = rect4;

             Rectangle oldcircle5 = circle5;
             Rectangle oldcircle6 = circle6;
             Rectangle oldcircle7 = circle7;
             Rectangle oldcircle8 = circle8;
             Rectangle oldcircle9 = circle9;
             Rectangle oldcircle10 = circle10;

             imageBox1.Image = new Image<Bgr,Byte>(Global_uprightImage)
                 .Resize(selectedValue, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
              imageBox2.Image = new Image<Bgr, Byte>(Global_crouchedImage)
                 .Resize(selectedValue, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            // imageBox1.SetZoomScale(selectedValue, new Point(0, 0));
            // imageBox2.SetZoomScale(selectedValue, new Point(0, 0)); 

            double newwidth = imageBox1.Image.Size.Width;
             double newheight = imageBox1.Image.Size.Height;

             double ratiox;
             double ratioy;

             if (newwidth > oldwidth)
             {
                 ratiox = newwidth / oldwidth;
                 ratioy = newheight / oldheight;

             }
             else
             {
                 ratiox = oldwidth / newwidth;
                 ratioy = oldheight / newheight;

             }

                 //imageBox1.Image = imagePyramid_upright[selectedValue];-- Meenakshi
                 //imageBox2.Image = imagepyramid_crouched[selectedValue];
                 switch (dragiconstatus)
             {
                 case "belt":
                     //For Imagebox1
                     if (newwidth > oldwidth)
                     {                        

                         circle1 = Zoomcircle(oldcircle1,selectedValue,ratiox,ratioy,true);
                         circle2 = Zoomcircle(oldcircle2, selectedValue, ratiox, ratioy, true);
                         circle3 = Zoomcircle(oldcircle3, selectedValue, ratiox, ratioy, true);
                         circle4 = Zoomcircle(oldcircle4, selectedValue, ratiox, ratioy, true);

                         rect1 = ZoomRectangle(oldrect1, selectedValue, ratiox, ratioy, true);
                         rect2 = ZoomRectangle(oldrect2, selectedValue, ratiox, ratioy, true);
                         rect3 = ZoomRectangle(oldrect3, selectedValue, ratiox, ratioy, true);
                         rect4 = ZoomRectangle(oldrect4, selectedValue, ratiox, ratioy, true);

                     }
                     else
                     {

                         circle1 = Zoomcircle(circle1, selectedValue, ratiox, ratioy, false);
                         circle2 = Zoomcircle(circle2, selectedValue, ratiox, ratioy, false);
                         circle3 = Zoomcircle(oldcircle3, selectedValue, ratiox, ratioy, false);
                         circle4 = Zoomcircle(oldcircle4, selectedValue, ratiox, ratioy, false);

                         rect1 = ZoomRectangle(rect1, selectedValue, ratiox, ratioy, false);
                         rect2 = ZoomRectangle(rect2, selectedValue, ratiox, ratioy, false);
                         rect3 = ZoomRectangle(oldrect3, selectedValue, ratiox, ratioy, false);
                         rect4 = ZoomRectangle(oldrect4, selectedValue, ratiox, ratioy, false);

                     }*/

            /* circle1 = new Rectangle((int)(circle1.X * ratiox),(int)(circle1.Y * ratioy), 10 * selectedValue,
         10 * selectedValue);*/

            /* rect1 = new Rectangle((int)(100 * ratiox),(int)(150 * ratioy), 5 * selectedValue,
                 5 * selectedValue);

             circle2 = new Rectangle((int)(250 * ratiox),(int)(300 * ratioy), 10 * selectedValue,
                 10 * selectedValue);

             rect2 = new Rectangle((int)(300 * ratiox), (int)(150 * ratioy), 5 * selectedValue,
                 5 * selectedValue);

             //For Imagebox2
             circle3 = new Rectangle(150, 300, (int)(10 * markerWidth[selectedValue]),
                 (int)(10 * markerWidth[selectedValue]));

             rect3 = new Rectangle(100, 150, (int)(5 * markerWidth[selectedValue]),
                 (int)(5 * markerWidth[selectedValue]));

             circle4 = new Rectangle(250, 300, (int)(10 * markerWidth[selectedValue]),
                 (int)(10 * markerWidth[selectedValue]));

             rect4 = new Rectangle(300, 150, (int)(5 * markerWidth[selectedValue]),
                 (int)(5 * markerWidth[selectedValue]));*/
            /*    break;
            case "knees":
                Pen yellowpen = new Pen(Brushes.Yellow, 6);
                Pen Redpen = new Pen(Brushes.Red, 6);
                if (newwidth > oldwidth)
                {

                    circle5 = Zoomcircle(oldcircle5, selectedValue, ratiox, ratioy, true);
                    circle6 = Zoomcircle(oldcircle6, selectedValue, ratiox, ratioy, true);
                    circle7 = Zoomcircle(oldcircle7, selectedValue, ratiox, ratioy, true);
                    circle8 = Zoomcircle(oldcircle8, selectedValue, ratiox, ratioy, true);
                    circle9 = Zoomcircle(oldcircle9, selectedValue, ratiox, ratioy, true);
                    circle10 = Zoomcircle(oldcircle10, selectedValue, ratiox, ratioy, true);
                }
                else
                {
                    circle5 = Zoomcircle(oldcircle5, selectedValue, ratiox, ratioy, false);
                    circle6 = Zoomcircle(oldcircle6, selectedValue, ratiox, ratioy, false);
                    circle7 = Zoomcircle(oldcircle7, selectedValue, ratiox, ratioy, false);
                    circle8 = Zoomcircle(oldcircle8, selectedValue, ratiox, ratioy, false);
                    circle9 = Zoomcircle(oldcircle9, selectedValue, ratiox, ratioy, false);
                    circle10 = Zoomcircle(oldcircle10, selectedValue, ratiox, ratioy, false);

                }*/
            /*circle5 = new Rectangle(196, 250, (int)(10 * markerWidth[selectedValue]),
                (int)(10 * markerWidth[selectedValue]));
            circle6 = new Rectangle(230, 250, (int)(10 * markerWidth[selectedValue]),
                (int)(10 * markerWidth[selectedValue]));*/

            //Graphics g;

            //g = this.CreateGraphics();

            //g.DrawLine(yellowpen, circle5.X + 4, circle5.Y + 4, circle6.X + 2, circle6.Y + 4);


            /* circle7 = new Rectangle(189, 300, (int)(10 * markerWidth[selectedValue]),
                 (int)(10 * markerWidth[selectedValue]));
             circle8 = new Rectangle(239, 300, (int)(10 * markerWidth[selectedValue]),
                 (int)(10 * markerWidth[selectedValue]));

             circle9 = new Rectangle(189, 400, (int)(10 * markerWidth[selectedValue]),
                 (int)(10 * markerWidth[selectedValue]));
             circle10 = new Rectangle(239, 400, (int)(10 * markerWidth[selectedValue]),
                 (int)(10 * markerWidth[selectedValue]));*/

            /*       yellowpen.Dispose();
               break;
           default:
               break;
       }*/

        }

        public Rectangle Zoomcircle(Rectangle circle, int selectedValue, double ratiox, double ratioy, bool zoomout)
        {
            Rectangle zoomrect;
            if (zoomout)
            {
                zoomrect = new Rectangle((int)(circle.X * ratiox), (int)(circle.Y * ratioy), 10 * selectedValue,
                            10 * selectedValue);
            }
            else
            {
                zoomrect = new Rectangle((int)(circle.X / ratiox), (int)(circle.Y / ratioy), 10 * selectedValue,
                            10 * selectedValue);

            }
            return zoomrect;
        }
        public Rectangle ZoomRectangle(Rectangle recttozoom, int selectedValue, double ratiox, double ratioy, bool zoomout)
        {
            Rectangle zoomrect;
            if (zoomout)
            {
                zoomrect = new Rectangle((int)(recttozoom.X * ratiox), (int)(recttozoom.Y * ratioy), 5 * selectedValue,
                            5 * selectedValue);
            }
            else
            {
                zoomrect = new Rectangle((int)(recttozoom.X / ratiox), (int)(recttozoom.Y / ratioy), 5 * selectedValue,
                            5 * selectedValue);

            }
            return zoomrect;
        }

        private void quitapp_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(quitapp.Image);
            quitapp.Image = Yugamiru.Properties.Resources.end_down;
        }

        private void quitapp_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(quitapp.Image);
            quitapp.Image = Yugamiru.Properties.Resources.end_on;
        }

        private void returnbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(returnbutton.Image);
            returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_down;
        }

        private void returnbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(returnbutton.Image);
            returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_up;
        }

        private void nextbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(nextbutton.Image);
            nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;

        }

        private void nextbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(nextbutton.Image);
            nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;
        }

        private void enteruprightimage_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(enteruprightimage.Image);
            enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
        }

        private void enteruprightimage_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(enteruprightimage.Image);
            enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_up;
        }

        private void openimage_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(openimage.Image);
            openimage.Image = Yugamiru.Properties.Resources.imageload_down;
        }

        private void openimage_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(openimage.Image);
            openimage.Image = Yugamiru.Properties.Resources.imageload_up;
        }

        private void rotateimage_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(rotateimage.Image);
            rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
        }

        private void rotateimage_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(rotateimage.Image);
            rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_up;
        }

        private void editid_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(editid.Image);
            editid.Image = Yugamiru.Properties.Resources.namechange_down;
        }

        private void editid_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(editid.Image);
            editid.Image = Yugamiru.Properties.Resources.namechange_up;
        }

        private void Finish_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(Finish.Image);
            Finish.Image = Yugamiru.Properties.Resources.completegreen_down;
        }

        private void Finish_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(Finish.Image);
            Finish.Image = Yugamiru.Properties.Resources.completegreen_up;
        }

        private void changebutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(changebutton.Image);
            changebutton.Image = Yugamiru.Properties.Resources.imagechange_down;
        }

        private void changebutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(changebutton.Image);
            changebutton.Image = Yugamiru.Properties.Resources.imagechange_on;
        }

        private void checkpositionbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(checkpositionbutton.Image);
            checkpositionbutton.Image = Yugamiru.Properties.Resources.jointedit_down;
        }

        private void checkpositionbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(checkpositionbutton.Image);
            checkpositionbutton.Image = Yugamiru.Properties.Resources.jointedit_on;
        }

        private void showreportbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(showreportbutton.Image);
            showreportbutton.Image = Yugamiru.Properties.Resources.reportdisplay_down;
        }

        private void showreportbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(showreportbutton.Image);
            showreportbutton.Image = Yugamiru.Properties.Resources.reportdisplay_on;
        }

        private void printreportbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(printreportbutton.Image);
            printreportbutton.Image = Yugamiru.Properties.Resources.reportprint_down;
        }

        private void printreportbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(printreportbutton.Image);
            printreportbutton.Image = Yugamiru.Properties.Resources.reportprint_on;
        }

        private void saveresultbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(saveresultbutton.Image);
            saveresultbutton.Image = Yugamiru.Properties.Resources.datasave_down;
        }

        private void saveresultbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(saveresultbutton.Image);
            saveresultbutton.Image = Yugamiru.Properties.Resources.datasave_on;
        }

        private void restartbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(restartbutton.Image);
            restartbutton.Image = Yugamiru.Properties.Resources.startred_down;
        }

        private void restartbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(restartbutton.Image);
            restartbutton.Image = Yugamiru.Properties.Resources.startred_on;
        }

        private void initialscreenbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(initialscreenbutton.Image);
            initialscreenbutton.Image = Yugamiru.Properties.Resources.returnstart_down;
        }

        private void initialscreenbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(initialscreenbutton.Image);
            initialscreenbutton.Image = Yugamiru.Properties.Resources.returnstart_on;
        }
        private void imageBox1_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.TranslateTransform(0.0F, 0.0F);
            Pen yellowpen = new Pen(Brushes.Yellow, 3);
            Pen Redpen = new Pen(Brushes.Red, 3);
            switch (dragiconstatus)
            {
                case "belt":
                    e.Graphics.FillEllipse(Brushes.Red, circle1); //left ankle
                    e.Graphics.FillEllipse(Brushes.Red, circle2); // right ankle
                    e.Graphics.FillRectangle(Brushes.Red, rect1);//left belt
                    e.Graphics.FillRectangle(Brushes.Red, rect2);//right belt
                    break;
                case "knees":
                    circle5.X = rect1.X + 10; //left hip
                    circle5.Y = rect1.Y;
                    circle6.X = rect2.X - 10; // right hip
                    circle6.Y = rect2.Y;

                    circle9 = circle1; // left ankle
                    circle10 = circle2; // right ankle

                   

                    e.Graphics.DrawLine(yellowpen, circle5.X + 4, circle5.Y + 4, circle6.X + 2, circle6.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle5.X + 4, circle5.Y + 4, circle7.X + 2, circle7.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle6.X + 4, circle6.Y + 4, circle8.X + 2, circle8.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle7.X + 4, circle7.Y + 4, circle9.X + 2, circle9.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle8.X + 4, circle8.Y + 4, circle10.X + 2, circle10.Y + 4);

                    e.Graphics.FillEllipse(Brushes.Green, circle5); //left belt
                    e.Graphics.FillEllipse(Brushes.Green, circle6);// right belt
                    e.Graphics.FillEllipse(Brushes.Red, circle7); //left knees
                    e.Graphics.DrawEllipse(new Pen(Color.Red, 1), new RectangleF(circle7.X - 4, circle7.Y - 4, circle7.Width + 6, circle7.Height + 6));
                    e.Graphics.FillEllipse(Brushes.Red, circle8); //right knees
                    e.Graphics.DrawEllipse(new Pen(Color.Red, 1), new RectangleF(circle8.X - 4, circle8.Y - 4, circle8.Width + 6, circle8.Height + 6));
                    
                    e.Graphics.FillEllipse(Brushes.Green, circle9); //left ankle
                    e.Graphics.FillEllipse(Brushes.Green, circle10);// right ankle
                    
                    yellowpen.Dispose();
                    break;
                case "shoulder":
                    circle17 = circle5;//left hip
                    circle18 = circle6;//right hip

                    circle19 = circle7;//left knees
                    circle20 = circle8;//right knees

                    circle21 = circle1;//left ankle
                    circle22 = circle2;//right ankle

                    e.Graphics.DrawLine(yellowpen, circle17.X + 4, circle17.Y + 4, circle18.X + 2, circle18.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle17.X + 4, circle17.Y + 4, circle19.X + 2, circle19.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle18.X + 4, circle18.Y + 4, circle20.X + 2, circle20.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle19.X + 4, circle19.Y + 4, circle21.X + 2, circle21.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle20.X + 4, circle20.Y + 4, circle22.X + 2, circle22.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle23.X +4, circle23.Y +4, circle17.X +2, circle17.Y+4);
                    e.Graphics.DrawLine(yellowpen, circle24.X+4, circle24.Y+4, circle18.X+2, circle18.Y+4);
                    e.Graphics.DrawLine(yellowpen, circle23.X+4, circle23.Y+4, circle24.X+2, circle24.Y+4);
                    e.Graphics.DrawLine(yellowpen, circle33.X+4, circle33.Y+4, circle34.X+2, circle34.Y+4);
                    e.Graphics.DrawLine(yellowpen, circle35.X+4, circle35.Y+4, circle36.X+2, circle36.Y+4);

                    e.Graphics.FillEllipse(Brushes.Green, circle17); //left hip
                    e.Graphics.FillEllipse(Brushes.Green, circle18); // right hip

                    e.Graphics.FillEllipse(Brushes.Green, circle19); //left knees
                    e.Graphics.FillEllipse(Brushes.Green, circle20);//right knees
                    
                    e.Graphics.FillEllipse(Brushes.Green, circle21);//left ankle
                    e.Graphics.FillEllipse(Brushes.Green, circle22);//right ankle
                    
                    e.Graphics.FillEllipse(Brushes.Red, circle23);//left shoulder
                    e.Graphics.FillEllipse(Brushes.Red, circle24);//right shoulder

                    e.Graphics.DrawArc(Redpen, circle23.X - 10, circle23.Y - 10, 20, 40, -90, -90);
                    e.Graphics.DrawArc(Redpen, circle24.X, circle24.Y - 10, 20, 40, 0, -90);

                    e.Graphics.FillEllipse(Brushes.Red, circle33); //Glabella
                    e.Graphics.FillEllipse(Brushes.Red, circle34);//chin
                    e.Graphics.FillEllipse(Brushes.Red, circle35);//left ear
                    e.Graphics.FillEllipse(Brushes.Red, circle36);//right ear

                    Redpen.Dispose();
                    yellowpen.Dispose();

                    break;
                default:
                    break;

            }

        }
        private void imageBox1_MouseDown(object sender, MouseEventArgs e)
        {
           /* MessageBox.Show("e.X is "+(e.X).ToString());
            MessageBox.Show("e.Y is " + (e.Y).ToString());
            MessageBox.Show("circle1.X is "+(circle1.X).ToString());
            MessageBox.Show("circle1.Y is " + (circle1.Y).ToString());
            MessageBox.Show("circle1.width is " + (circle1.Width).ToString());
            MessageBox.Show("circle1.height is " + (circle1.Height).ToString());*/

            int offsetX = (int)(e.X / imageBox1.ZoomScale);
            int offsetY = (int)(e.Y / imageBox1.ZoomScale);
            int horizontalScrollBarValue = imageBox1.HorizontalScrollBar.Visible ? (int)imageBox1.HorizontalScrollBar.Value : 0;
            int verticalScrollBarValue = imageBox1.VerticalScrollBar.Visible ? (int)imageBox1.VerticalScrollBar.Value : 0;

            /*if ((e.X >= circle1.X) && (e.Y >= circle1.Y))
            {
                firstCircle = true;
            }*/
            var horizantalPosition = e.X + 220;
            var verticalPosition = e.Y + 220;


            //if ((horizantalPosition >= circle1.X) && (horizantalPosition <= (circle1.X + circle1.Width)) && (verticalPosition >= circle1.Y) && (verticalPosition <= (circle1.Y + circle1.Height)))
            //{
            //firstCircle = true;
            //}
            /*if ((e.X >= circle1.X) && (e.X <= ((circle1.X + horizontalScrollBarValue) + circle1.Width)) && (e.Y >= circle1.Y) && (e.Y <= ((circle1.Y + verticalScrollBarValue) + circle1.Height)))
            {
                firstCircle = true;
            }*/
            if ((e.X >= circle1.X) && (e.X <= (circle1.X + circle1.Width)) && (e.Y >= circle1.Y) && (e.Y <= (circle1.Y + circle1.Height)))
            {
                firstCircle = true;
            }
            else if ((e.X >= circle2.X) && (e.X <= (circle2.X + circle2.Width)) && (e.Y >= circle2.Y) && (e.Y <= (circle2.Y + circle2.Height)))
            {
                secondCircle = true;
            }
            else if ((e.X >= rect1.X) && (e.X <= (rect1.X + rect1.Width)) && (e.Y >= rect1.Y) && (e.Y <= (rect1.Y + rect1.Height)))
            {
                firstrect = true;
            }
            else if ((e.X >= rect2.X) && (e.X <= (rect2.X + rect2.Width)) && (e.Y >= rect2.Y) && (e.Y <= (rect2.Y + rect2.Height)))
            {
                secondrect = true;
            }
            else if ((e.X >= circle7.X) && (e.X <= (circle7.X + circle7.Width)) && (e.Y >= circle7.Y) && (e.Y <= (circle7.Y + circle7.Height)))
            {
                kneesCircle = true;
            }
            else if ((e.X >= circle8.X) && (e.X <= (circle8.X + circle8.Width)) && (e.Y >= circle8.Y) && (e.Y <= (circle8.Y + circle8.Height)))
            {
                kneesCircle1 = true;
            }
            else if ((e.X >= circle23.X) && (e.X <= (circle23.X + circle23.Width)) && (e.Y >= circle23.Y) && (e.Y <= (circle23.Y + circle23.Height)))
            {
                shoulder = true;
            }
            else if ((e.X >= circle24.X) && (e.X <= (circle24.X + circle24.Width)) && (e.Y >= circle24.Y) && (e.Y <= (circle24.Y + circle24.Height)))
            {
                shoulder1 = true;
            }
            else if ((e.X >= circle33.X) && (e.X <= (circle33.X + circle33.Width)) && (e.Y >= circle33.Y) && (e.Y <= (circle33.Y + circle33.Height)))
            {
                facecircle = true;
            }
            else if ((e.X >= circle34.X) && (e.X <= (circle34.X + circle34.Width)) && (e.Y >= circle34.Y) && (e.Y <= (circle34.Y + circle34.Height)))
            {
                facecircle1 = true;
            }
            else if ((e.X >= circle35.X) && (e.X <= (circle35.X + circle35.Width)) && (e.Y >= circle35.Y) && (e.Y <= (circle35.Y + circle35.Height)))
            {
                facecircle2 = true;
            }
            else if ((e.X >= circle36.X) && (e.X <= (circle36.X + circle36.Width)) && (e.Y >= circle36.Y) && (e.Y <= (circle36.Y + circle36.Height)))
            {
                facecircle3 = true;
            }

            mouseCliked = true;

        }

        private void imageBox1_MouseMove(object sender, MouseEventArgs e)
        {
            //MessageBox.Show("Hi");
            if (mouseCliked)
            {
                label_for_position.Visible = true;
                //MessageBox.Show("Hi inside mouseclicked");
                if (firstCircle)
                {
                    //MessageBox.Show("Hi inside mouseclicked");
                    circle1.X = e.X;
                    circle1.Y = e.Y;
                    
                    label_for_position.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;
                }
                else if (secondCircle)
                {
                    circle2.X = e.X;
                    circle2.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;
                }
                else if (firstrect)
                {
                    rect1.X = e.X;
                    rect1.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.BELT_POSITION;
                }
                else if (secondrect)
                {
                    rect2.X = e.X;
                    rect2.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.BELT_POSITION;
                }
                else if (kneesCircle)
                {
                    circle7.X = e.X;
                    circle7.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.KNEE_POSITION;
                }
                else if (kneesCircle1)
                {
                    circle8.X = e.X;
                    circle8.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.KNEE_POSITION;
                }
                else if(shoulder)
                {
                    circle23.X = e.X;
                    circle23.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.SHOULDER_POSITION;

                    
                }
                else if(shoulder1)
                {
                    circle24.X = e.X;
                    circle24.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.SHOULDER_POSITION;
                }
                else if(facecircle)
                {
                    circle33.X = e.X;
                    circle33.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.GLABELLA_POSITION;
                }
                else if (facecircle1)
                {
                    circle34.X = e.X;
                    circle34.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.CHIN_POSITION;
                }
                else if (facecircle2)
                {
                    circle35.X = e.X;
                    circle35.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.EAR_POSITION;
                }
                else if (facecircle3)
                {
                    circle36.X = e.X;
                    circle36.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.EAR_POSITION;
                }
                Refresh();
            }

        }


        private void imageBox1_MouseUp(object sender, MouseEventArgs e)
        {
            label_for_position.Visible = false;
            firstCircle = false;
            secondCircle = false;
            firstrect = false;
            secondrect = false;
            mouseCliked = false;
            kneesCircle = false;
            kneesCircle1 = false;

            facecircle = false;
            facecircle1 = false;
            facecircle2 = false;
            facecircle3 = false;

            shoulder = false;
            shoulder1 = false;
        }

        private void imageBox1_MouseLeave(object sender, EventArgs e)
        {

            label_for_position.Visible = false;
            firstCircle = false;
            secondCircle = false;
            firstrect = false;
            secondrect = false;
            mouseCliked = false;
            kneesCircle = false;
            kneesCircle1 = false;

            facecircle = false;
            facecircle1 = false;
            facecircle2 = false;
            facecircle3 = false;

            shoulder = false;
            shoulder1 = false;

        }

        private void vScrollBarsideview_Scroll(object sender, ScrollEventArgs e)
        {
            /* var selectedValue = e.NewValue / 2;        
             imageBox3.Image = imagepyramid_side[selectedValue];*/
            //imageBox3.SetZoomScale(selectedValue, new Point(200, 200));
            double selectedValue = 0.0;
            if (e.NewValue == 1)
                selectedValue = 5;
            else if (e.NewValue == 2)
                selectedValue = 4;
            else if (e.NewValue == 3)
                selectedValue = 3;
            else if (e.NewValue == 4)
                selectedValue = 2;
            else if (e.NewValue == 5)
                selectedValue = 1;
            
            imageBox3.SetZoomScale(selectedValue, new Point(220, 220));
        }
        private void disposeMemory(Image objImage)
        {
            if (objImage != null)
            {
                objImage.Dispose();
                objImage = null;
            }

        }

        private void imageBox2_MouseDown(object sender, MouseEventArgs e)
        {
            //MessageBox.Show((e.X).ToString());
            if ((e.X >= circle3.X) && (e.X <= (circle3.X + circle3.Width)) && (e.Y >= circle3.Y) && (e.Y <= (circle3.Y + circle3.Height)))
            {
                firstCircle = true;
            }
            else if ((e.X >= circle4.X) && (e.X <= (circle4.X + circle4.Width)) && (e.Y >= circle4.Y) && (e.Y <= (circle4.Y + circle4.Height)))
            {
                secondCircle = true;
            }
            else if ((e.X >= rect3.X) && (e.X <= (rect3.X + rect3.Width)) && (e.Y >= rect3.Y) && (e.Y <= (rect3.Y + rect3.Height)))
            {
                firstrect = true;
            }
            else if ((e.X >= rect4.X) && (e.X <= (rect4.X + rect4.Width)) && (e.Y >= rect4.Y) && (e.Y <= (rect4.Y + rect4.Height)))
            {
                secondrect = true;
            }
            else if ((e.X >= circle13.X) && (e.X <= (circle13.X + circle13.Width)) && (e.Y >= circle13.Y) && (e.Y <= (circle13.Y + circle13.Height)))
            {
                kneesCircle = true;
            }
            else if ((e.X >= circle14.X) && (e.X <= (circle14.X + circle14.Width)) && (e.Y >= circle14.Y) && (e.Y <= (circle14.Y + circle14.Height)))
            {
                kneesCircle1 = true;
            }
            else if ((e.X >= circle31.X) && (e.X <= (circle31.X + circle31.Width)) && (e.Y >= circle31.Y) && (e.Y <= (circle31.Y + circle31.Height)))
            {
                shoulder = true;
            }
            else if ((e.X >= circle32.X) && (e.X <= (circle32.X + circle32.Width)) && (e.Y >= circle32.Y) && (e.Y <= (circle32.Y + circle32.Height)))
            {
                shoulder1 = true;
            }
            else if ((e.X >= circle37.X) && (e.X <= (circle37.X + circle37.Width)) && (e.Y >= circle37.Y) && (e.Y <= (circle37.Y + circle37.Height)))
            {
                facecircle = true;
            }
            else if ((e.X >= circle38.X) && (e.X <= (circle38.X + circle38.Width)) && (e.Y >= circle38.Y) && (e.Y <= (circle38.Y + circle38.Height)))
            {
                facecircle1 = true;
            }
            else if ((e.X >= circle39.X) && (e.X <= (circle39.X + circle39.Width)) && (e.Y >= circle39.Y) && (e.Y <= (circle39.Y + circle39.Height)))
            {
                facecircle2 = true;
            }
            else if ((e.X >= circle40.X) && (e.X <= (circle40.X + circle40.Width)) && (e.Y >= circle40.Y) && (e.Y <= (circle40.Y + circle40.Height)))
            {
                facecircle3 = true;
            }

            mouseCliked = true;

        }

        private void imageBox2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseCliked)
            {
                label_for_position.Visible = true;
                if (firstCircle)
                {
                    circle3.X = e.X;
                    circle3.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;
                }
                else if (secondCircle)
                {
                    circle4.X = e.X;
                    circle4.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;
                }
                else if (firstrect)
                {
                    rect3.X = e.X;
                    rect3.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.BELT_POSITION;
                }
                else if (secondrect)
                {
                    rect4.X = e.X;
                    rect4.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.BELT_POSITION;
                }
                else if (kneesCircle)
                {
                    circle13.X = e.X;
                    circle13.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.KNEE_POSITION;


                }
                else if (kneesCircle1)
                {
                    circle14.X = e.X;
                    circle14.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.KNEE_POSITION;
                }
                else if (shoulder)
                {
                    circle31.X = e.X;
                    circle31.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.SHOULDER_POSITION;
                }
                else if (shoulder1)
                {
                    circle32.X = e.X;
                    circle32.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.SHOULDER_POSITION;
                }
                else if (facecircle)
                {
                    circle37.X = e.X;
                    circle37.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.GLABELLA_POSITION;


                }
                else if (facecircle1)
                {
                    circle38.X = e.X;
                    circle38.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.CHIN_POSITION;
                }
                else if (facecircle2)
                {
                    circle39.X = e.X;
                    circle39.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.EAR_POSITION;
                }
                else if (facecircle3)
                {
                    circle40.X = e.X;
                    circle40.Y = e.Y;

                    label_for_position.Text = Yugamiru.Properties.Resources.EAR_POSITION;
                }
                Refresh();
            }

        }

        private void imageBox2_MouseUp(object sender, MouseEventArgs e)
        {
            label_for_position.Visible = false;
            firstCircle = false;
            secondCircle = false;
            firstrect = false;
            secondrect = false;
            mouseCliked = false;
            kneesCircle = false;
            kneesCircle1 = false;

            facecircle = false;
            facecircle1 = false;
            facecircle2 = false;
            facecircle3 = false;

            shoulder = false;
            shoulder1 = false;

        }

        private void imageBox2_MouseLeave(object sender, EventArgs e)
        {
            label_for_position.Visible = false;
            firstCircle = false;
            secondCircle = false;
            firstrect = false;
            secondrect = false;
            mouseCliked = false;
            kneesCircle = false;
            kneesCircle1 = false;

            facecircle = false;
            facecircle1 = false;
            facecircle2 = false;
            facecircle3 = false;

            shoulder = false;
            shoulder1 = false;

        }

        private void imageBox2_Paint(object sender, PaintEventArgs e)
        {
            Pen yellowpen = new Pen(Brushes.Yellow, 3);
            Pen Redpen = new Pen(Brushes.Red, 3);
            switch (dragiconstatus)
            {
                case "belt":
                    e.Graphics.FillEllipse(Brushes.Red, circle3);//Left ankle
                    e.Graphics.FillEllipse(Brushes.Red, circle4);//Right ankle
                    e.Graphics.FillRectangle(Brushes.Red, rect3);//Left Belt
                    e.Graphics.FillRectangle(Brushes.Red, rect4);//Right belt
                    break;

                case "knees":

                    circle11.X = rect3.X + 10; //left hip
                    circle11.Y = rect3.Y;
                    circle12.X = rect4.X - 10; // right hip
                    circle12.Y = rect4.Y;

                    circle15 = circle3; // left ankle
                    circle16 = circle4; // right ankle

                    e.Graphics.DrawLine(yellowpen, circle11.X + 4, circle11.Y + 4, circle12.X + 2, circle12.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle11.X + 4, circle11.Y + 4, circle13.X + 2, circle13.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle12.X + 4, circle12.Y + 4, circle14.X + 2, circle14.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle13.X + 4, circle13.Y + 4, circle15.X + 2, circle15.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle14.X + 4, circle14.Y + 4, circle16.X + 2, circle16.Y + 4);

                    e.Graphics.FillEllipse(Brushes.Green, circle11);//left hip
                    e.Graphics.FillEllipse(Brushes.Green, circle12);//right hip
                    e.Graphics.FillEllipse(Brushes.Red, circle13);//left knees
                    e.Graphics.DrawEllipse(new Pen(Color.Red, 1), new RectangleF(circle13.X - 4, circle13.Y - 4, circle13.Width + 6, circle13.Height + 6));
                    e.Graphics.FillEllipse(Brushes.Red, circle14);//right knees
                    e.Graphics.DrawEllipse(new Pen(Color.Red, 1), new RectangleF(circle14.X - 4, circle14.Y - 4, circle14.Width + 6, circle14.Height + 6));
                    e.Graphics.FillEllipse(Brushes.Green, circle15);
                    e.Graphics.FillEllipse(Brushes.Green, circle16);
                    
                    yellowpen.Dispose();
                    break;

                case "shoulder":

                    circle25 = circle11;//left hip
                    circle26 = circle12;//right hip

                    circle27 = circle13;//left knees
                    circle28 = circle14;//right knees

                    circle29 = circle15;//left ankle
                    circle30 = circle16;//right ankle

                    

                    //e.Graphics.DrawLine(yellowpen, 202, 254, 232, 254);
                    e.Graphics.DrawLine(yellowpen, circle25.X + 4, circle25.Y + 4, circle26.X + 2, circle26.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle25.X + 4, circle25.Y + 4, circle27.X + 2, circle27.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle26.X + 4, circle26.Y + 4, circle28.X + 2, circle28.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle27.X + 4, circle27.Y + 4, circle29.X + 2, circle29.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle28.X + 4, circle28.Y + 4, circle30.X + 2, circle30.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle31.X + 4, circle31.Y + 4, circle25.X + 2, circle25.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle32.X + 4, circle32.Y + 4, circle26.X + 2, circle26.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle31.X + 4, circle31.Y + 4, circle32.X + 2, circle32.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle37.X + 4, circle37.Y + 4, circle38.X + 2, circle38.Y + 4);
                    e.Graphics.DrawLine(yellowpen, circle39.X + 4, circle39.Y + 4, circle40.X + 2, circle40.Y + 4);



                    e.Graphics.FillEllipse(Brushes.Green, circle25);//left hip
                    e.Graphics.FillEllipse(Brushes.Green, circle26);//right hip
                    e.Graphics.FillEllipse(Brushes.Green, circle27);//left knees
                    e.Graphics.FillEllipse(Brushes.Green, circle28);//right knees
                    e.Graphics.FillEllipse(Brushes.Green, circle29);//left ankle
                    e.Graphics.FillEllipse(Brushes.Green, circle30);//right ankle
                    e.Graphics.FillEllipse(Brushes.Red, circle31); // Left Shoulder
                    e.Graphics.FillEllipse(Brushes.Red, circle32);// Right Shoulder

       
                    e.Graphics.DrawArc(Redpen, circle31.X - 10, circle31.Y - 10, 20, 40, -90, -90);

                    e.Graphics.DrawArc(Redpen, circle32.X , circle32.Y - 10, 20, 40, 0, -90);

                    e.Graphics.FillEllipse(Brushes.Red, circle37);//glabella
                    e.Graphics.FillEllipse(Brushes.Red, circle38);//chin
                    e.Graphics.FillEllipse(Brushes.Red, circle39);//Left ear
                    e.Graphics.FillEllipse(Brushes.Red, circle40);//Right ear

                    


                    Redpen.Dispose();
                    yellowpen.Dispose();

                    break;
                    

                default:
                    break;
            }

        }

        private void imageBox3_Paint(object sender, PaintEventArgs e)
        {
            Pen yellowpen = new Pen(Brushes.Yellow, 3);
            Pen Redpen = new Pen(Brushes.Red, 3);

            e.Graphics.DrawLine(yellowpen, circle41.X, circle41.Y, circle42.X, circle42.Y);
            e.Graphics.DrawLine(yellowpen, circle42.X, circle42.Y, circle43.X, circle43.Y);
            e.Graphics.DrawLine(yellowpen, circle44.X, circle44.Y, circle41.X, circle41.Y);


            e.Graphics.FillEllipse(Brushes.Red, circle41);//hip
            e.Graphics.FillEllipse(Brushes.Red, circle46);//left_belt
            e.Graphics.FillEllipse(Brushes.Red,circle47);//right_belt
            e.Graphics.FillEllipse(Brushes.Red, circle42);  //side knees               
            e.Graphics.FillEllipse(Brushes.Red, circle43); // side ankle
            e.Graphics.FillEllipse(Brushes.Red, circle44); //shoulder
            
            e.Graphics.DrawArc(Redpen, circle44.X - 10, circle44.Y - 10, 20, 40, -90, -90);

            e.Graphics.FillEllipse(Brushes.Red, circle45); //ear

            Redpen.Dispose();
            yellowpen.Dispose();

        }

        private void imageBox3_MouseDown(object sender, MouseEventArgs e)
        {
            
            if ((e.X >= circle44.X) && (e.X <= (circle44.X + circle44.Width)) && (e.Y >= circle44.Y) && (e.Y <= (circle44.Y + circle44.Height)))
            {
                shoulder_side = true;
            }
            else if ((e.X >= circle45.X) && (e.X <= (circle45.X + circle45.Width)) && (e.Y >= circle45.Y) && (e.Y <= (circle45.Y + circle45.Height)))
            {
                ear_side = true;
            }
            else if ((e.X >= circle46.X) && (e.X <= (circle46.X + circle46.Width)) && (e.Y >= circle46.Y) && (e.Y <= (circle46.Y + circle46.Height)))
            {
                lefthip_side = true;
            }
            else if ((e.X >= circle47.X) && (e.X <= (circle47.X + circle47.Width)) && (e.Y >= circle47.Y) && (e.Y <= (circle47.Y + circle47.Height)))
            {
                righthip_side = true;
            }
            else if ((e.X >= circle42.X) && (e.X <= (circle42.X + circle42.Width)) && (e.Y >= circle42.Y) && (e.Y <= (circle42.Y + circle42.Height)))
            {
                patella_side = true;
            }
            else if ((e.X >= circle43.X) && (e.X <= (circle43.X + circle43.Width)) && (e.Y >= circle43.Y) && (e.Y <= (circle43.Y + circle43.Height)))
            {
                ankle_side = true;
            }

            mouseCliked = true;

        }

        private void imageBox3_MouseLeave(object sender, EventArgs e)
        {
            label_for_Side_Position.Visible = false;

            shoulder_side = false;
            ear_side = false;
                mouseCliked = false;
            lefthip_side = false;
            righthip_side = false;
            patella_side = false;
            ankle_side = false;
        }

        private void imageBox3_MouseMove(object sender, MouseEventArgs e)
        {
            
            if (mouseCliked)
            {
                label_for_Side_Position.Visible = true;
                if (shoulder_side)
                {
                    circle44.X = e.X;
                    circle44.Y = e.Y;
                    label_for_Side_Position.Text = Yugamiru.Properties.Resources.SHOULDER_POSITION;
                }
                else if(ear_side)
                {
                    circle45.X = e.X;
                    circle45.Y = e.Y;
                    label_for_Side_Position.Text = Yugamiru.Properties.Resources.EAR_POSITION;
                }
                else if (lefthip_side)
                {
                    circle46.X = e.X;
                    circle46.Y = e.Y;
                    circle41.X = circle46.X + 30;
                    circle41.Y = circle46.Y;

                    label_for_Side_Position.Text = Yugamiru.Properties.Resources.BELT_POSITION;
                }
                else if (righthip_side)
                {
                    circle47.X = e.X;
                    circle47.Y = e.Y;
                    circle41.X = circle47.X - 30;
                    circle41.Y = circle47.Y;

                    label_for_Side_Position.Text = Yugamiru.Properties.Resources.BELT_POSITION;
                }
                else if (patella_side)
                {
                    circle42.X = e.X;
                    circle42.Y = e.Y;

                    label_for_Side_Position.Text = Yugamiru.Properties.Resources.KNEE_POSITION;
                }
                else if (ankle_side)
                {
                    circle43.X = e.X;
                    circle43.Y = e.Y;
                    label_for_Side_Position.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;

                }
                Refresh();
            }

        }

        private void imageBox3_MouseUp(object sender, MouseEventArgs e)
        {
            label_for_Side_Position.Visible = false;

            shoulder_side = false;
            ear_side = false;
            mouseCliked = false;
            lefthip_side = false;
            righthip_side = false;
            patella_side = false;
            ankle_side = false;

        }

        private void vScrollBarfinalscreen_Scroll(object sender, ScrollEventArgs e)
        {
            /* var selectedValue = e.NewValue / 2;
             imageBox4.Image = imagePyramid_upright[selectedValue];
             imageBox5.Image = imagepyramid_crouched[selectedValue];   */
            double selectedValue = 0.0;
            if (e.NewValue == 1)
                selectedValue = 5;
            else if (e.NewValue == 2)
                selectedValue = 4;
            else if (e.NewValue == 3)
                selectedValue = 3;
            else if (e.NewValue == 4)
                selectedValue = 2;
            else if (e.NewValue == 5)
                selectedValue = 1;

            imageBox4.SetZoomScale(selectedValue, new Point(220, 220));
            imageBox5.SetZoomScale(selectedValue, new Point(220, 220));
            
        }
        void SampleDrawOutlineText(Graphics g, String text, Font font, Point p)
        {
            // set atialiasing
            g.SmoothingMode = SmoothingMode.HighQuality;
            // make thick pen for outlining
            Pen pen = new Pen(Color.White, 3);
            // round line joins of the pen
            pen.LineJoin = LineJoin.Round;
            // create graphics path
            GraphicsPath textPath = new GraphicsPath();
            // convert string to path
            textPath.AddString(text, font.FontFamily, (int)font.Style, font.Size, p, StringFormat.GenericTypographic);
            // clone path to make outlining path
            GraphicsPath outlinePath = (GraphicsPath)textPath.Clone();
            // outline the path
            outlinePath.Widen(pen);
            // fill outline path with some color
            g.FillPath(Brushes.White, outlinePath);
            // fill original text path with some color
            g.FillPath(Brushes.Black, textPath);
        }

        private void imageBox4_Paint(object sender, PaintEventArgs e)
        {
            int iBenchmarkDistance = 70;
            Point ptBenchmark1 = new Point(0, 0);
            Point ptBenchmark2 = new Point(0, 0);
            FrontBodyAngle m_FrontBodyAngle = new FrontBodyAngle();

            MyData mydataObj = new MyData();
            mydataObj.CalcBodyAngleStanding(m_FrontBodyAngle, 0);
            //m_FrontBodyPositionStanding.Init(1024, 1280, m_CalibInf, m_CalcPostureProp);
            FrontBodyLabelString m_FrontBodyLabelString = new FrontBodyLabelString(m_FrontBodyPositionStanding, m_FrontBodyAngle, ptBenchmark1, ptBenchmark2, iBenchmarkDistance);

            double ratioX = 305.00 / 430.00;
            double ratioY = 380.00 / 472.00;
            
            Pen pinkpen = new Pen(Brushes.Pink, 1);
            e.Graphics.DrawLine(pinkpen, 152, 0, 152, 380);

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptGlabella.X * ratioX ) +2, 
                (float)(m_FrontBodyPositionStanding.m_ptGlabella.Y  * ratioY), 
                (float)(m_FrontBodyPositionStanding.m_ptChin.X * ratioX)+2, 
                (float)(m_FrontBodyPositionStanding.m_ptChin.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptLeftEar.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptLeftEar.Y * ratioY),
                (float)(m_FrontBodyPositionStanding.m_ptRightEar.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptRightEar.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptLeftShoulder.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptLeftShoulder.Y * ratioY),
                (float)(m_FrontBodyPositionStanding.m_ptRightShoulder.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptRightShoulder.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptLeftHip.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptLeftHip.Y * ratioY),
                (float)(m_FrontBodyPositionStanding.m_ptRightHip.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptRightHip.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptLeftShoulder.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptLeftShoulder.Y * ratioY),
                (float)(m_FrontBodyPositionStanding.m_ptLeftHip.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptLeftHip.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptRightShoulder.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptRightShoulder.Y * ratioY),
                (float)(m_FrontBodyPositionStanding.m_ptRightHip.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptRightHip.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptLeftHip.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptLeftHip.Y * ratioY),
                (float)(m_FrontBodyPositionStanding.m_ptLeftKnee.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptLeftKnee.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptRightHip.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptRightHip.Y * ratioY),
                (float)(m_FrontBodyPositionStanding.m_ptRightKnee.X * ratioX) + 2,
                (float)(m_FrontBodyPositionStanding.m_ptRightKnee.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptLeftKnee.X * ratioX) + 2,
          (float)(m_FrontBodyPositionStanding.m_ptLeftKnee.Y * ratioY),
          (float)(m_FrontBodyPositionStanding.m_ptLeftAnkle.X * ratioX) + 2,
          (float)(m_FrontBodyPositionStanding.m_ptLeftAnkle.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodyPositionStanding.m_ptRightKnee.X * ratioX) + 2,
          (float)(m_FrontBodyPositionStanding.m_ptRightKnee.Y * ratioY),
          (float)(m_FrontBodyPositionStanding.m_ptRightAnkle.X * ratioX) + 2,
          (float)(m_FrontBodyPositionStanding.m_ptRightAnkle.Y * ratioY));

            e.Graphics.FillEllipse(Brushes.DarkOliveGreen, (float)(m_FrontBodyPositionStanding.m_ptGlabella.X * ratioX),(float)(m_FrontBodyPositionStanding.m_ptGlabella.Y * ratioY),6,6);//glabella
            e.Graphics.FillEllipse(Brushes.DarkOliveGreen, (float)(m_FrontBodyPositionStanding.m_ptChin.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptChin.Y * ratioY) ,6,6);//Chin
            e.Graphics.FillEllipse(Brushes.Purple, (float)(m_FrontBodyPositionStanding.m_ptLeftEar.X * ratioX),(float) (m_FrontBodyPositionStanding.m_ptLeftEar.Y * ratioY),6,6);//left ear
            e.Graphics.FillEllipse(Brushes.Purple, (float)(m_FrontBodyPositionStanding.m_ptRightEar.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightEar.Y * ratioY),6,6);//right ear
            e.Graphics.FillEllipse(Brushes.Gold, (float)(m_FrontBodyPositionStanding.m_ptLeftShoulder.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftShoulder.Y * ratioY), 6, 6);//left shoulder
            e.Graphics.FillEllipse(Brushes.Gold, (float)(m_FrontBodyPositionStanding.m_ptRightShoulder.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightShoulder.Y * ratioY), 6, 6);//right shoulder

            e.Graphics.FillEllipse(Brushes.Red, (float)(m_FrontBodyPositionStanding.m_ptLeftHip.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftHip.Y * ratioY), 6, 6);//left hip
            e.Graphics.FillEllipse(Brushes.Red, (float)(m_FrontBodyPositionStanding.m_ptRightHip.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightHip.Y * ratioY), 6, 6);//right hip

            e.Graphics.FillEllipse(Brushes.Green, (float)(m_FrontBodyPositionStanding.m_ptLeftKnee.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftKnee.Y * ratioY), 6, 6);//left knees
            e.Graphics.FillEllipse(Brushes.Green, (float)(m_FrontBodyPositionStanding.m_ptRightKnee.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightKnee.Y * ratioY), 6, 6);//right knees

            e.Graphics.FillEllipse(Brushes.Blue, (float)(m_FrontBodyPositionStanding.m_ptLeftAnkle.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftAnkle.Y * ratioY), 6, 6);//left ankle
            e.Graphics.FillEllipse(Brushes.Blue, (float)(m_FrontBodyPositionStanding.m_ptRightAnkle.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightAnkle.Y * ratioY), 6, 6);//right ankle

            GlyphOverlayerToFrontImage ArrowObj = new GlyphOverlayerToFrontImage();
            ArrowObj.SetArrowInvisible(1);
            // ArrowObj.DrawHorizontalArrow(e.Graphics,100,150,2);
            ArrowObj.Draw(e.Graphics, m_FrontBodyPositionStanding, m_FrontBodyAngle,ratioX, ratioY);

            LabelandFont(e.Graphics, m_FrontBodyLabelString, true);
        }


        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void saveresultbutton_Click(object sender, EventArgs e)
        {
            FrontBodyAngle m_FrontBodyAngleStanding = new FrontBodyAngle();
            FrontBodyAngle m_FrontBodyAngleKneedown = new FrontBodyAngle();
            SideBodyAngle m_SideBodyAngle = new SideBodyAngle();

            MyData mydataObj = new MyData();
            mydataObj.CalcBodyAngleStanding(m_FrontBodyAngleStanding, 0);
            mydataObj.CalcBodyAngleKneedown(m_FrontBodyAngleKneedown, 0);
            mydataObj.CalcBodyAngleSide(m_SideBodyAngle, 0);
            

            ResultData m_ResultObj = new ResultData();
            int Rank = m_ResultObj.CalcYugamiPointRank(m_FrontBodyAngleStanding, m_FrontBodyAngleKneedown, m_SideBodyAngle);
            int Score = m_ResultObj.GetScore();

            string connetionString = null;
            MySqlConnection cnn;
            connetionString = "server=localhost;database=world;uid=root;pwd=1982;";
            cnn = new MySqlConnection(connetionString);
            try
            {
                cnn.Open();
                MessageBox.Show("Connection Open ! ");
                string cmdText = string.Empty;

                var computerName = ComputerInfo.GetComputerName();
                var computerId = ComputerInfo.GetComputerId();

                cmdText = "INSERT INTO yugamiru.tblmaster_yugamiru(Sideimage,Uprightimage,";
                cmdText = cmdText + "Crouchedimage,URglabellapos,URearangle,URearpos,";
                cmdText = cmdText + "URshoulderangle,URchinpos,URhipangle,";
                cmdText = cmdText + "URshoulderpos,URrightkneeangle,";
                cmdText = cmdText + "URhippos,URleftkneeangle,Cglabellapos,Cearangle,";
                cmdText = cmdText + "Cearpos,Cshoulderangle,Cchinpos,Chipangle,Cshoulderpos,";
                cmdText = cmdText + "Crightkneeangle,Chippos,Cleftkneeangle,";
                cmdText = cmdText + "patientID,Patientname,Gender,PatientDOB,Height,Computerid,Computername,Score,Rank) ";
                cmdText = cmdText + "values(@Image,@Image1,@Image2,'";
                cmdText = cmdText + URglabellapos + "','";
                cmdText = cmdText + URearangle + "','";
                cmdText = cmdText + URearpos + "','";
                cmdText = cmdText + URshoulderangle + "','";
                cmdText = cmdText + URchinpos + "','";
                cmdText = cmdText + URhipangle + "','";
                cmdText = cmdText + URshoulderpos + "','";
                cmdText = cmdText + URrightkneeangle + "','";
                cmdText = cmdText + URhippos + "','";
                cmdText = cmdText + URleftkneeangle + "','";
                cmdText = cmdText + Cglabellapos + "','";
                cmdText = cmdText + Cearangle + "','";
                cmdText = cmdText + Cearpos + "','";
                cmdText = cmdText + Cshoulderangle + "','";
                cmdText = cmdText + Cchinpos + "','";
                cmdText = cmdText + Chipangle + "','";
                cmdText = cmdText + Cshoulderpos + "','";
                cmdText = cmdText + Crightkneeangle + "','";
                cmdText = cmdText + Chippos + "','";
                cmdText = cmdText + Cleftkneeangle + "','";
                cmdText = cmdText + user_ID.Text + "','";
                cmdText = cmdText + user_Name.Text + "','";
                cmdText = cmdText + genderlabel.Text + "','";
                cmdText = cmdText + dateofbirthlabel.Text + "','";
                cmdText = cmdText + heightlabel.Text + "','";
                cmdText = cmdText + computerId + "','";
                cmdText = cmdText + computerName + "','";
                cmdText = cmdText + Score + "','";
                cmdText = cmdText + Rank + "')";


                MySqlCommand cmd = new MySqlCommand(cmdText, cnn);
                cmd.Parameters.Add("@Image",MySqlDbType.LongBlob);
                cmd.Parameters["@Image"].Value = imageToByteArray(Global_sideImage);
                cmd.Parameters.Add("@Image1", MySqlDbType.LongBlob);
                cmd.Parameters["@Image1"].Value = imageToByteArray(Global_uprightImage);
                cmd.Parameters.Add("@Image2", MySqlDbType.LongBlob);
                cmd.Parameters["@Image2"].Value = imageToByteArray(Global_crouchedImage);

                cmd.ExecuteNonQuery();
                MessageBox.Show("inserted into tbl_master");

                cmdText = string.Empty;
                cmdText = "insert into yugamiru.kneedownimagevalues (PatientID,KneedownRightHipX,";
                cmdText = cmdText + "KneedownRightHipY,KneedownLeftHipX,KneedownLeftHipY,";
                cmdText = cmdText + "KneedownRightKneeX,KneedownRightKneeY,";
                cmdText = cmdText + "KneedownLeftKneeX,KneedownLeftKneeY,";
                cmdText = cmdText + "KneedownRightAnkleX,KneedownRightAnkleY,";
                cmdText = cmdText + "KneedownLeftAnkleX,KneedownLeftAnkleY,";
                cmdText = cmdText + "KneedownRightBeltX,KneedownRightBeltY,";
                cmdText = cmdText + "KneedownLeftBeltX, KneedownLeftBeltY,";
                cmdText = cmdText + "KneedownRightShoulderX,KneedownRightShoulderY,";
                cmdText = cmdText + "KneedownLeftShoulderX,KneedownLeftShoulderY,";
                cmdText = cmdText + "KneedownRightEarX,KneedownRightEarY,";
                cmdText = cmdText + "KneedownLeftEarX,KneedownLeftEarY,";
                cmdText = cmdText + "KneedownChinX,KneedownChinY,";
                cmdText = cmdText + "KneedownGlabellaX,KneedownGlabellaY,";
                cmdText = cmdText + "KneedownUnderBodyPositionDetected,KneedownKneePositionDetected,";
                cmdText = cmdText + "KneedownUpperBodyPositionDetected) values(";
                cmdText = cmdText + "'"+user_ID.Text+"','";
                cmdText = cmdText + circle26.X + "','"+circle26.Y+"','";
                cmdText = cmdText + circle25.X + "','"+circle25.Y+"','";
                cmdText = cmdText + circle28.X + "','" + circle28.Y + "','";
                cmdText = cmdText + circle27.X + "','" + circle27.Y + "','";
                cmdText = cmdText + circle30.X + "','" + circle30.Y + "','";
                cmdText = cmdText + circle29.X + "','" + circle29.Y + "','";
                cmdText = cmdText + rect2.X + "','" + rect2.Y + "','";
                cmdText = cmdText + rect1.X + "','" + rect1.Y + "','";
                cmdText = cmdText + circle32.X + "','" + circle32.Y + "','";
                cmdText = cmdText + circle31.X + "','" + circle31.Y + "','";
                cmdText = cmdText + circle40.X + "','" + circle40.Y + "','";
                cmdText = cmdText + circle39.X + "','" + circle39.Y + "','";
                cmdText = cmdText + circle38.X + "','" + circle38.Y + "','";
                cmdText = cmdText + circle37.X + "','" + circle37.Y + "','0','0','0')";

                cmd = new MySqlCommand(cmdText, cnn);
                cmd.ExecuteNonQuery();

                MessageBox.Show("inserted into kneedownimagevalues ");
                cmdText = string.Empty;
                cmdText = "insert into yugamiru.standingimagevalues (PatientID,StandingRightHipX,";
                cmdText = cmdText + "StandingRightHipY,StandingLeftHipX,StandingLeftHipY,";
                cmdText = cmdText + "StandingRightKneeX,StandingRightKneeY,";
                cmdText = cmdText + "StandingLeftKneeX,StandingLeftKneeY,";
                cmdText = cmdText + "StandingRightAnkleX,StandingRightAnkleY,";
                cmdText = cmdText + "StandingLeftAnkleX,StandingLeftAnkleY,";
                cmdText = cmdText + "StandingRightBeltX,StandingRightBeltY,";
                cmdText = cmdText + "StandingLeftBeltX, StandingLeftBeltY,";
                cmdText = cmdText + "StandingRightShoulderX,StandingRightShoulderY,";
                cmdText = cmdText + "StandingLeftShoulderX,StandingLeftShoulderY,";
                cmdText = cmdText + "StandingRightEarX,StandingRightEarY,";
                cmdText = cmdText + "StandingLeftEarX,StandingLeftEarY,";
                cmdText = cmdText + "StandingChinX,StandingChinY,";
                cmdText = cmdText + "StandingGlabellaX,StandingGlabellaY,";
                cmdText = cmdText + "StandingUnderBodyPositionDetected,StandingKneePositionDetected,";
                cmdText = cmdText + "StandingUpperBodyPositionDetected) values(";
                cmdText = cmdText + "'" + user_ID.Text + "','";
                cmdText = cmdText + circle18.X + "','" + circle18.Y + "','";
                cmdText = cmdText + circle17.X + "','" + circle17.Y + "','";
                cmdText = cmdText + circle20.X + "','" + circle20.Y + "','";
                cmdText = cmdText + circle19.X + "','" + circle19.Y + "','";
                cmdText = cmdText + circle22.X + "','" + circle22.Y + "','";
                cmdText = cmdText + circle21.X + "','" + circle21.Y + "','";
                cmdText = cmdText + rect4.X + "','" + rect4.Y + "','";
                cmdText = cmdText + rect3.X + "','" + rect3.Y + "','";
                cmdText = cmdText + circle24.X + "','" + circle24.Y + "','";
                cmdText = cmdText + circle23.X + "','" + circle23.Y + "','";
                cmdText = cmdText + circle36.X + "','" + circle36.Y + "','";
                cmdText = cmdText + circle35.X + "','" + circle35.Y + "','";
                cmdText = cmdText + circle34.X + "','" + circle34.Y + "','";
                cmdText = cmdText + circle33.X + "','" + circle33.Y + "','0','0','0')";

                cmd = new MySqlCommand(cmdText, cnn);
                cmd.ExecuteNonQuery();

                MessageBox.Show("inserted into standingimagevalues");
                cmdText = string.Empty;
                cmdText = "insert into yugamiru.sideimagevalues (PatientID,";
                cmdText = cmdText + "SideHipX,SideHipY,";
                cmdText = cmdText + "SideKneeX,SideKneeY,";
                cmdText = cmdText + "SideAnkleX,SideAnkleY,";
                cmdText = cmdText + "SideRightBelt,SideRightBeltY,";
                cmdText = cmdText + "SideLeftBeltX, SideLeftBeltY,";
                cmdText = cmdText + "SideShoulderX,SideShoulderY,";
                cmdText = cmdText + "SideEarX,SideEarY) values(";
                //cmdText = cmdText + "SideChinX,SideChinY";
                //cmdText = cmdText + "SideGlabellaX,SideGlabellaY";
                //cmdText = cmdText + "SideBenchmark1X,SideBenchmark1Y";
                //cmdText = cmdText + "SideBenchmark2X,SideBenchmark2Y) values(";
                cmdText = cmdText + "'" + user_ID.Text + "','";
                cmdText = cmdText + circle41.X + "','" + circle41.Y + "','";
                cmdText = cmdText + circle42.X + "','" + circle42.Y + "','";
                cmdText = cmdText + circle43.X + "','" + circle43.Y + "','";
                cmdText = cmdText + circle47.X + "','" + circle47.Y + "','";
                cmdText = cmdText + circle46.X + "','" + circle46.Y + "','";
                cmdText = cmdText + circle44.X + "','" + circle44.Y + "','";
                cmdText = cmdText + circle45.X + "','" + circle45.Y + "')";
                

                cmd = new MySqlCommand(cmdText, cnn);
                cmd.ExecuteNonQuery();
                MessageBox.Show("inserted into side table");

                MessageBox.Show("image saved ! ");
                cnn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public byte[] imageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        private void glabella_crouched_label_Click(object sender, EventArgs e)
        {

        }

        private void finalscreen_Click(object sender, EventArgs e)
        {

        }

        private void imageBox5_Click(object sender, EventArgs e)
        {

        }

        private void glabella_label_Click(object sender, EventArgs e)
        {

        }

        private void imageBox5_Paint(object sender, PaintEventArgs e)
        {
            int iBenchmarkDistance = 70;
            Point ptBenchmark1 = new Point(0, 0);
            Point ptBenchmark2 = new Point(0, 0);
            FrontBodyAngle m_FrontBodyAngle = new FrontBodyAngle();

            MyData mydataObj = new MyData();
            mydataObj.CalcBodyAngleKneedown(m_FrontBodyAngle, 0);

            FrontBodyLabelString m_FrontBodyLabelString = new FrontBodyLabelString(m_FrontBodyPositionStanding, m_FrontBodyAngle, ptBenchmark1, ptBenchmark2, iBenchmarkDistance);
            Pen pinkpen = new Pen(Brushes.Pink, 1);
            e.Graphics.DrawLine(pinkpen, 152, 0, 152, 380);


            double ratioX = 305.00 / 430.00;
            double ratioY = 380.00 / 472.00;

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptGlabella.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptGlabella.Y * ratioY),
                (float)(m_FrontBodykneedownPosition.m_ptChin.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptChin.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptLeftEar.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptLeftEar.Y * ratioY),
                (float)(m_FrontBodykneedownPosition.m_ptRightEar.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptRightEar.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptLeftShoulder.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptLeftShoulder.Y * ratioY),
                (float)(m_FrontBodykneedownPosition.m_ptRightShoulder.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptRightShoulder.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptLeftHip.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptLeftHip.Y * ratioY),
                (float)(m_FrontBodykneedownPosition.m_ptRightHip.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptRightHip.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptLeftShoulder.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptLeftShoulder.Y * ratioY),
                (float)(m_FrontBodykneedownPosition.m_ptLeftHip.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptLeftHip.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptRightShoulder.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptRightShoulder.Y * ratioY),
                (float)(m_FrontBodykneedownPosition.m_ptRightHip.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptRightHip.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptLeftHip.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptLeftHip.Y * ratioY),
                (float)(m_FrontBodykneedownPosition.m_ptLeftKnee.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptLeftKnee.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptRightHip.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptRightHip.Y * ratioY),
                (float)(m_FrontBodykneedownPosition.m_ptRightKnee.X * ratioX) + 2,
                (float)(m_FrontBodykneedownPosition.m_ptRightKnee.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptLeftKnee.X * ratioX) + 2,
          (float)(m_FrontBodykneedownPosition.m_ptLeftKnee.Y * ratioY),
          (float)(m_FrontBodykneedownPosition.m_ptLeftAnkle.X * ratioX) + 2,
          (float)(m_FrontBodykneedownPosition.m_ptLeftAnkle.Y * ratioY));

            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), (float)(m_FrontBodykneedownPosition.m_ptRightKnee.X * ratioX) + 2,
          (float)(m_FrontBodykneedownPosition.m_ptRightKnee.Y * ratioY),
          (float)(m_FrontBodykneedownPosition.m_ptRightAnkle.X * ratioX) + 2,
          (float)(m_FrontBodykneedownPosition.m_ptRightAnkle.Y * ratioY));

            e.Graphics.FillEllipse(Brushes.DarkOliveGreen, (float)(m_FrontBodykneedownPosition.m_ptGlabella.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptGlabella.Y * ratioY), 6, 6);//glabella
            e.Graphics.FillEllipse(Brushes.DarkOliveGreen, (float)(m_FrontBodykneedownPosition.m_ptChin.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptChin.Y * ratioY), 6, 6);//Chin
            e.Graphics.FillEllipse(Brushes.Purple, (float)(m_FrontBodykneedownPosition.m_ptLeftEar.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftEar.Y * ratioY), 6, 6);//left ear
            e.Graphics.FillEllipse(Brushes.Purple, (float)(m_FrontBodykneedownPosition.m_ptRightEar.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightEar.Y * ratioY), 6, 6);//right ear
            e.Graphics.FillEllipse(Brushes.Gold, (float)(m_FrontBodykneedownPosition.m_ptLeftShoulder.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftShoulder.Y * ratioY), 6, 6);//left shoulder
            e.Graphics.FillEllipse(Brushes.Gold, (float)(m_FrontBodykneedownPosition.m_ptRightShoulder.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightShoulder.Y * ratioY), 6, 6);//right shoulder

            e.Graphics.FillEllipse(Brushes.Red, (float)(m_FrontBodykneedownPosition.m_ptLeftHip.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftHip.Y * ratioY), 6, 6);//left hip
            e.Graphics.FillEllipse(Brushes.Red, (float)(m_FrontBodykneedownPosition.m_ptRightHip.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightHip.Y * ratioY), 6, 6);//right hip

            e.Graphics.FillEllipse(Brushes.Green, (float)(m_FrontBodykneedownPosition.m_ptLeftKnee.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftKnee.Y * ratioY), 6, 6);//left knees
            e.Graphics.FillEllipse(Brushes.Green, (float)(m_FrontBodykneedownPosition.m_ptRightKnee.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightKnee.Y * ratioY), 6, 6);//right knees

            e.Graphics.FillEllipse(Brushes.Blue, (float)(m_FrontBodykneedownPosition.m_ptLeftAnkle.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptLeftAnkle.Y * ratioY), 6, 6);//left ankle
            e.Graphics.FillEllipse(Brushes.Blue, (float)(m_FrontBodykneedownPosition.m_ptRightAnkle.X * ratioX), (float)(m_FrontBodyPositionStanding.m_ptRightAnkle.Y * ratioY), 6, 6);//right ankle


            LabelandFont(e.Graphics, m_FrontBodyLabelString,false);

            GlyphOverlayerToFrontImage ArrowObj = new GlyphOverlayerToFrontImage();
            ArrowObj.SetArrowInvisible(1);
            // ArrowObj.DrawHorizontalArrow(e.Graphics,100,150,2);
            ArrowObj.Draw(e.Graphics, m_FrontBodyPositionStanding, m_FrontBodyAngle, ratioX, ratioY);

        }

        private void imageBox1_DragDrop(object sender, DragEventArgs e)
        {
            MessageBox.Show("hi");
        }

        private void imageBox2_DragDrop(object sender, DragEventArgs e)
        {
            MessageBox.Show("from imagebox2");
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void settingsbox_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
            disposeMemory(setting_screen.Image);
            disposeMemory(CancelSettings.Image);
            disposeMemory(OkSettings.Image);
            disposeMemory(ArrowDisplayON.Image);
            disposeMemory(MedianLineON.Image);
            disposeMemory(PositionAngleON.Image);
            disposeMemory(ScoreDisplayON.Image);
            disposeMemory(MuscleReportON.Image);
            disposeMemory(CentreGravityON.Image);
            disposeMemory(ArrowDisplayOFF.Image);
            disposeMemory(MedianLineOFF.Image);
            disposeMemory(PositionAngleOFF.Image);
            disposeMemory(ScoreDisplayOFF.Image);
            disposeMemory(MuscleReportOFF.Image);
            disposeMemory(CentreGravityOFF.Image);


            setting_screen.Image = Yugamiru.Properties.Resources.Mainpic7;

            //setting combobox initial values
            comboBox2.SelectedIndex = 0;
            MarkerSize.SelectedIndex = 0;

            CancelSettings.Image = Yugamiru.Properties.Resources.cancel_on;
            OkSettings.Image = Yugamiru.Properties.Resources.setting_on;

            ArrowDisplayON.Image = Yugamiru.Properties.Resources.radio_on;
            ArrowDisplayOFF.Image = Yugamiru.Properties.Resources.radio_off;

            MedianLineON.Image = Yugamiru.Properties.Resources.radio_on;
            MedianLineOFF.Image = Yugamiru.Properties.Resources.radio_off;

            PositionAngleON.Image = Yugamiru.Properties.Resources.radio_on;
            PositionAngleOFF.Image = Yugamiru.Properties.Resources.radio_off;

            ScoreDisplayON.Image = Yugamiru.Properties.Resources.radio_on;
            ScoreDisplayOFF.Image = Yugamiru.Properties.Resources.radio_off;

            MuscleReportON.Image = Yugamiru.Properties.Resources.radio_on;
            MuscleReportOFF.Image = Yugamiru.Properties.Resources.radio_off;

            CentreGravityON.Image = Yugamiru.Properties.Resources.radio_on;
            CentreGravityOFF.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void MarkerSize_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ArrowDisplayON_Click(object sender, EventArgs e)
        {
            ArrowDisplayON.Image = Yugamiru.Properties.Resources.radio_on;
            ArrowDisplayOFF.Image = Yugamiru.Properties.Resources.radio_off;
        }

        private void ArrowDisplayOFF_Click(object sender, EventArgs e)
        {
            ArrowDisplayON.Image = Yugamiru.Properties.Resources.radio_off;
            ArrowDisplayOFF.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void MedianLineON_Click(object sender, EventArgs e)
        {
            MedianLineON.Image = Yugamiru.Properties.Resources.radio_on;
            MedianLineOFF.Image = Yugamiru.Properties.Resources.radio_off;
        }

        private void MedianLineOFF_Click(object sender, EventArgs e)
        {
            MedianLineON.Image = Yugamiru.Properties.Resources.radio_off;
            MedianLineOFF.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void PositionAngleON_Click(object sender, EventArgs e)
        {
            PositionAngleON.Image = Yugamiru.Properties.Resources.radio_on;
            PositionAngleOFF.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void PositionAngleOFF_Click(object sender, EventArgs e)
        {
            PositionAngleON.Image = Yugamiru.Properties.Resources.radio_off;
            PositionAngleOFF.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void ScoreDisplayON_Click(object sender, EventArgs e)
        {
            ScoreDisplayON.Image = Yugamiru.Properties.Resources.radio_on;
            ScoreDisplayOFF.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void ScoreDisplayOFF_Click(object sender, EventArgs e)
        {
            ScoreDisplayON.Image = Yugamiru.Properties.Resources.radio_off;
            ScoreDisplayOFF.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void MuscleReportON_Click(object sender, EventArgs e)
        {
            MuscleReportON.Image = Yugamiru.Properties.Resources.radio_on;
            MuscleReportOFF.Image = Yugamiru.Properties.Resources.radio_off;
        }

        private void MuscleReportOFF_Click(object sender, EventArgs e)
        {
            MuscleReportON.Image = Yugamiru.Properties.Resources.radio_off;
            MuscleReportOFF.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void CentreGravityON_Click(object sender, EventArgs e)
        {
            CentreGravityON.Image = Yugamiru.Properties.Resources.radio_on;
            CentreGravityOFF.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void CentreGravityOFF_Click(object sender, EventArgs e)
        {
            CentreGravityON.Image = Yugamiru.Properties.Resources.radio_off;
            CentreGravityOFF.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void OkSettings_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;

        }

        private void CancelSettings_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void mainpage_Click(object sender, EventArgs e)
        {

        }

        public void LabelandFont(Graphics e, FrontBodyLabelString m_FrontBodyLabelString,  bool uprightflag)
        {
            Font f = new Font("Ariel", 12, FontStyle.Regular, GraphicsUnit.Pixel);

            SampleDrawOutlineText(e, "glabella pos", f, new Point(20, 10));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strGlabellaPosition, f, new Point(20, 30));

            SampleDrawOutlineText(e, "ear pos", f, new Point(20, 70));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strEarCenterPosition, f, new Point(20, 90));
            SampleDrawOutlineText(e, "chin pos", f, new Point(20, 150));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strChinPosition, f, new Point(20, 170));
            SampleDrawOutlineText(e, "shoulder pos", f, new Point(20, 230));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strShoulderCenterPosition, f, new Point(20, 250));
            SampleDrawOutlineText(e, "hip pos", f, new Point(20, 310));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strHipCenterPosition, f, new Point(20, 330));

            SampleDrawOutlineText(e, "ear angle", f, new Point(220, 10));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strEarBalance, f, new Point(220, 30));
            SampleDrawOutlineText(e, "shoulder angle", f, new Point(220, 70));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strShoulderBalance, f, new Point(220, 90));
            SampleDrawOutlineText(e, "hip angle", f, new Point(220, 150));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strHipBalance, f, new Point(220, 170));
            SampleDrawOutlineText(e, "right knee angle", f, new Point(220, 230));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strRightKneeAngle, f, new Point(220, 250));
            SampleDrawOutlineText(e, "left knee angle", f, new Point(220, 310));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strLeftKneeAngle, f, new Point(220, 330));

            if(uprightflag)
            {
                
                URglabellapos = m_FrontBodyLabelString.GetGlabellaPositionString();//m_FrontBodyLabelString.m_strGlabellaPosition;
                URearpos = m_FrontBodyLabelString.GetEarCenterPositionString();//.m_strEarCenterPosition;
                URchinpos = m_FrontBodyLabelString.GetChinPositionString();//m_strChinPosition;
                URshoulderpos = m_FrontBodyLabelString.GetShoulderCenterPositionString();//m_strShoulderCenterPosition;
                URhippos = m_FrontBodyLabelString.GetHipCenterPositionString();//m_strHipCenterPosition;


                URearangle = m_FrontBodyLabelString.GetEarBalanceString();//m_strEarBalance;
                URshoulderangle = m_FrontBodyLabelString.GetShoulderBalanceString();//m_strShoulderBalance;
                URhipangle = m_FrontBodyLabelString.GetHipBalanceString();// m_strHipBalance;
                URleftkneeangle = m_FrontBodyLabelString.GetLeftKneeAngleString();// m_strLeftKneeAngle;
                URrightkneeangle = m_FrontBodyLabelString.GetRightKneeAngleString();// m_strRightKneeAngle;

            }
            else
            {

                Cglabellapos = m_FrontBodyLabelString.GetGlabellaPositionString();//m_strGlabellaPosition;
                Cearpos = m_FrontBodyLabelString.GetEarCenterPositionString();//m_strEarCenterPosition;
                Cchinpos = m_FrontBodyLabelString.GetChinPositionString();//m_strChinPosition;
                Cshoulderpos = m_FrontBodyLabelString.GetShoulderCenterPositionString();//m_strShoulderCenterPosition;
                Chippos = m_FrontBodyLabelString.GetHipCenterPositionString();//m_strHipCenterPosition;


                Cearangle = m_FrontBodyLabelString.GetEarBalanceString();//m_strEarBalance;
                Cshoulderangle = m_FrontBodyLabelString.GetShoulderBalanceString();//m_strShoulderBalance;
                URhipangle = m_FrontBodyLabelString.GetHipBalanceString();//m_strHipBalance;
                URleftkneeangle = m_FrontBodyLabelString.GetLeftKneeAngleString();//m_strLeftKneeAngle;
                URrightkneeangle = m_FrontBodyLabelString.GetRightKneeAngleString();//m_strRightKneeAngle;

            }

        }
    }
}
