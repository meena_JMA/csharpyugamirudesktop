﻿namespace Yugamiru.Forms
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hELPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qrCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pORTSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionInfoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hELPToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hELPToolStripMenuItem
            // 
            this.hELPToolStripMenuItem.BackColor = System.Drawing.SystemColors.Window;
            this.hELPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.qrCodeToolStripMenuItem,
            this.pORTSettingsToolStripMenuItem});
            this.hELPToolStripMenuItem.Name = "hELPToolStripMenuItem";
            this.hELPToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.hELPToolStripMenuItem.Text = "SETTINGS";
            this.hELPToolStripMenuItem.Click += new System.EventHandler(this.hELPToolStripMenuItem_Click);
            // 
            // qrCodeToolStripMenuItem
            // 
            this.qrCodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Window;
            this.qrCodeToolStripMenuItem.CheckOnClick = true;
            this.qrCodeToolStripMenuItem.Name = "qrCodeToolStripMenuItem";
            this.qrCodeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.qrCodeToolStripMenuItem.Text = "QR Code";
            this.qrCodeToolStripMenuItem.Click += new System.EventHandler(this.qrCodeToolStripMenuItem_Click);
            // 
            // pORTSettingsToolStripMenuItem
            // 
            this.pORTSettingsToolStripMenuItem.Name = "pORTSettingsToolStripMenuItem";
            this.pORTSettingsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pORTSettingsToolStripMenuItem.Text = "PORT Settings";
            this.pORTSettingsToolStripMenuItem.Click += new System.EventHandler(this.pORTSettingsToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionInfoToolStripMenuItem1});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.settingsToolStripMenuItem.Text = "HELP";
            // 
            // versionInfoToolStripMenuItem1
            // 
            this.versionInfoToolStripMenuItem1.Name = "versionInfoToolStripMenuItem1";
            this.versionInfoToolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.versionInfoToolStripMenuItem1.Text = "VersionInfo";
            this.versionInfoToolStripMenuItem1.Click += new System.EventHandler(this.versionInfoToolStripMenuItem1_Click);
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BaseForm";
            this.Text = "Yugamiru";
            this.Load += new System.EventHandler(this.BaseForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hELPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qrCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionInfoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pORTSettingsToolStripMenuItem;
    }
}