﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QRCoder;
using System.IO;
using System.Net;
using MySql.Data.MySqlClient;


namespace Yugamiru.Forms
{
    public partial class BaseForm : Form
    {
        Form1 childForm = null;
        public BaseForm()
        {
            InitializeComponent();
        }
        public void SetChildFormObject(Form1 f)
        {
            childForm = f;
        }
        private void BaseForm_Load(object sender, EventArgs e)
        {

        }

        private void hELPToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void qrCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (qrCodeToolStripMenuItem.Checked)
            {
                //to get network number
                string sHostName = Dns.GetHostName();
                IPHostEntry ipE = Dns.GetHostByName(sHostName);
                IPAddress[] IpA = ipE.AddressList;
                /* for (int i = 0; i < IpA.Length; i++)
                 {
                     Console.WriteLine("IP Address {0}: {1} ", i, IpA[i].ToString());
                 }*/

                var port = GetPort();

                childForm.mainpage.Image.Dispose();
                //string code = txtCode.Text;
                QRCodeGenerator qrGenerator = new QRCodeGenerator();

                QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(IpA.ToString() + port, QRCodeGenerator.ECCLevel.Q);


                // System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                Image imgBarCode;

                //imgBarCode.Height = 150;
                //imgBarCode.Width = 150;
                Bitmap bitMap;
                using (bitMap = qrCode.GetGraphic(20))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] byteImage = ms.ToArray();
                        //imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);

                        imgBarCode = (Bitmap)((new ImageConverter()).ConvertFrom(byteImage));



                    }
                    //plBarCode.Controls.Add(imgBarCode);

                }
                childForm.mainpage.Image = imgBarCode;
            }
            else
            {
                childForm.mainpage.Image.Dispose();
                childForm.mainpage.Image = Yugamiru.Properties.Resources.Mainpic;

            }
        }
        string GetPort()
        {
            
            MySQLDBConnection mdbObj = new MySQLDBConnection();
            mdbObj.OpenConnection();        

                string query = "select port from port_settings Where id = (select max(id) from port_settings)";              
                string port = mdbObj.RetrieveData(query);

            mdbObj.closeconnction();
            MessageBox.Show(port);
            return port;

        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void versionInfoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("VersionInfo");
        }

        private void pORTSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PORT portui = new PORT();
            portui.Show();

        }

    }
}
