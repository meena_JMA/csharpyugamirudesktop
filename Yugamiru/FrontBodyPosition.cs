﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Yugamiru
{
    public class FrontBodyPosition
    {

       public Point m_ptRightHip;
        public Point m_ptLeftHip;
        public Point m_ptRightKnee;
        public Point m_ptLeftKnee;
        public Point m_ptRightAnkle;
        public Point m_ptLeftAnkle;
        public Point m_ptRightBelt;
        public Point m_ptLeftBelt;
        public Point m_ptRightShoulder;
        public Point m_ptLeftShoulder;
        public Point m_ptRightEar;
        public Point m_ptLeftEar;
        public Point m_ptChin;
        public Point m_ptGlabella;

        bool m_bUnderBodyPositionDetected;
        bool m_bKneePositionDetected;
        bool m_bUpperBodyPositionDetected;
        

     public FrontBodyPosition()
        { }

~FrontBodyPosition()
{
}

/* operator=(   rSrc )
{
	m_ptRightHip		= rSrc.m_ptRightHip;
	m_ptLeftHip			= rSrc.m_ptLeftHip;
	m_ptRightKnee		= rSrc.m_ptRightKnee;
	m_ptLeftKnee		= rSrc.m_ptLeftKnee;
	m_ptRightAnkle		= rSrc.m_ptRightAnkle;
	m_ptLeftAnkle		= rSrc.m_ptLeftAnkle;
	m_ptRightBelt		= rSrc.m_ptRightBelt;
	m_ptLeftBelt		= rSrc.m_ptLeftBelt;
	m_ptRightShoulder	= rSrc.m_ptRightShoulder;
	m_ptLeftShoulder	= rSrc.m_ptLeftShoulder;
	m_ptRightEar		= rSrc.m_ptRightEar;
	m_ptLeftEar			= rSrc.m_ptLeftEar;
	m_ptChin			= rSrc.m_ptChin;
	m_ptGlabella		= rSrc.m_ptGlabella;
	m_bUnderBodYPositionDetected	= rSrc.m_bUnderBodYPositionDetected;
	m_bKneePositionDetected			= rSrc.m_bKneePositionDetected;
	m_bUpperBodYPositionDetected	= rSrc.m_bUpperBodYPositionDetected;

	return *this;
}*/

 public void InitInvalid()
    {
        m_ptRightHip = new Point(0, 0);
        m_ptLeftHip = new Point(0,0);
        m_ptRightKnee = new Point(0, 0);
        m_ptLeftKnee = new Point(0, 0);
        m_ptRightAnkle = new Point(0, 0);
            m_ptLeftAnkle = new Point(0, 0);
            m_ptRightBelt = new Point(0, 0);
            m_ptLeftBelt = new Point(0, 0);
            m_ptRightShoulder = new Point(0, 0);
            m_ptLeftShoulder = new Point(0, 0);
            m_ptRightEar = new Point(0, 0);
            m_ptLeftEar = new Point(0, 0);
            m_ptChin = new Point(0, 0);
            m_ptGlabella = new Point(0, 0);
            m_bUnderBodyPositionDetected = false;
            m_bKneePositionDetected = false;
            m_bUpperBodyPositionDetected = false;
    }

    public void Init(int width, long height, CCalibInf CalibInf, CCalcPostureProp CalcPostureProp )
    {
        int middleX = (int)CalibInf.GetFPLcenterX();
         double DIST_FACTOR = 0.07;

        int ankleXoffset = (int)(width * DIST_FACTOR);
        int hipXoffset = (int)(width * (DIST_FACTOR - 0.005));
        int kneeXoffset = (int)(width * DIST_FACTOR);

        m_ptRightAnkle.X = middleX - ankleXoffset;
        m_ptLeftAnkle.X = middleX + ankleXoffset;
        m_ptRightHip.X = middleX - hipXoffset;
        m_ptLeftHip.X = middleX + hipXoffset;
        m_ptRightKnee.X = middleX - kneeXoffset;
        m_ptLeftKnee.X = middleX + kneeXoffset;
        int hipLength = Math.Abs(m_ptRightHip.X - m_ptLeftHip.X);
        int beltXoffset = (int)(hipLength * 0.125);
        m_ptRightBelt.X = m_ptRightHip.X - beltXoffset;
        m_ptLeftBelt.X = m_ptLeftHip.X + beltXoffset;

        m_ptRightAnkle.Y = (int)(height - CalibInf.GetRightAnkleposY());
        m_ptLeftAnkle.Y = (int)(height - CalibInf.GetLeftAnkleposY());
        m_ptRightHip.Y = m_ptRightAnkle.Y - m_ptRightAnkle.Y * 38 / 100;
        m_ptLeftHip.Y = m_ptRightHip.Y;
        m_ptRightKnee.Y = m_ptRightAnkle.Y - (m_ptRightAnkle.Y - m_ptRightHip.Y) * 4 / 10;
        m_ptLeftKnee.Y = m_ptRightKnee.Y;
        // compute BELT coordinates from hips coordinates
        int legLength = Math.Abs(m_ptLeftHip.Y - m_ptLeftAnkle.Y);
        m_ptRightBelt.Y = (int)(m_ptLeftHip.Y - legLength * CalcPostureProp.GetBelt2HipRatio());
        m_ptLeftBelt.Y = m_ptRightBelt.Y;

        m_bUnderBodyPositionDetected = false;
        m_bKneePositionDetected = false;
        m_bUpperBodyPositionDetected = false;

        Adjust(width, (int)height);
    }


  public  bool InitUpperPostion(int width, int height, CCalibInf CalibInf )
    {
        int middleX = (int)CalibInf.GetFPLcenterX();
        int hipwidth = m_ptLeftHip.X - m_ptRightHip.X;
        int leglength = Math.Abs(m_ptLeftHip.Y - m_ptLeftAnkle.Y);

        if (hipwidth > 100)
        {
            hipwidth = 100;
        }

        m_ptRightShoulder.X = (int)(m_ptRightHip.X - (int)(hipwidth * 0.3));
        m_ptLeftShoulder.X = (int)(m_ptLeftHip.X + (int)(hipwidth * 0.3));
        m_ptRightEar.X = m_ptRightHip.X;
        m_ptLeftEar.X = m_ptLeftHip.X;
        m_ptChin.X = (m_ptLeftHip.X + m_ptRightHip.X) / 2;
        m_ptGlabella.X = (m_ptLeftHip.X + m_ptRightHip.X) / 2;

        m_ptRightShoulder.Y = m_ptRightHip.Y - leglength * 3 / 4;
        m_ptLeftShoulder.Y = m_ptRightHip.Y - leglength * 3 / 4;
        m_ptRightEar.Y = m_ptRightHip.Y - leglength;
        m_ptLeftEar.Y = m_ptRightHip.Y - leglength;
        m_ptChin.Y = m_ptRightHip.Y - leglength * 9 / 10;
        m_ptGlabella.Y = m_ptRightHip.Y - leglength * 11 / 10;

        Adjust(width, height);
        return true;
    }

public void GetRightHipPosition(Point ptPos ) 
{
	ptPos = m_ptRightHip;
}

public void GetRightKneePosition(Point ptPos ) 
{
	ptPos = m_ptRightKnee;
}

public void GetRightAnklePosition(Point ptPos ) 
{
	ptPos = m_ptRightAnkle;
}

public void GetLeftHipPosition(Point ptPos ) 
{
	ptPos = m_ptLeftHip;
}

public void GetLeftKneePosition(Point ptPos ) 
{
	ptPos = m_ptLeftKnee;
}

public void GetLeftAnklePosition(Point ptPos ) 
{
	ptPos = m_ptLeftAnkle;
}

public void GetRightBeltPosition(Point ptPos ) 
{
	ptPos = m_ptRightBelt;
}

public void GetLeftBeltPosition(Point ptPos ) 
{
	ptPos = m_ptLeftBelt;
}

public void GetRightShoulderPosition(Point ptPos ) 
{
	ptPos = m_ptRightShoulder;
}

public void GetLeftShoulderPosition(Point ptPos ) 
{
	ptPos = m_ptLeftShoulder;
}

public void GetRightEarPosition(Point ptPos ) 
{
	ptPos = m_ptRightEar;
}

public void GetLeftEarPosition(Point ptPos ) 
{
	ptPos = m_ptLeftEar;
}

public void GetChinPosition(Point ptPos ) 
{
	ptPos = m_ptChin;
}

public void GetGlabellaPosition(Point ptPos ) 
{
	ptPos = m_ptGlabella;
}

public void SetRightHipPosition(  Point ptPos )
{
    m_ptRightHip = ptPos;
}

public void SetRightKneePosition(  Point ptPos )
{
    m_ptRightKnee = ptPos;
}

public void SetRightAnklePosition(  Point ptPos )
{
    m_ptRightAnkle = ptPos;
}

public void SetLeftHipPosition(  Point ptPos )
{
    m_ptLeftHip = ptPos;
}

public void SetLeftKneePosition(  Point ptPos )
{
    m_ptLeftKnee = ptPos;
}

public void SetLeftAnklePosition(  Point ptPos )
{
    m_ptLeftAnkle = ptPos;
}

public void SetRightBeltPosition(  Point ptPos )
{
    m_ptRightBelt = ptPos;
}

public void SetLeftBeltPosition(  Point ptPos )
{
    m_ptLeftBelt = ptPos;
}

public void SetRightShoulderPosition(  Point ptPos )
{
    m_ptRightShoulder = ptPos;
}

        public void SetLeftShoulderPosition(  Point ptPos )
{
    m_ptLeftShoulder = ptPos;
}

        public void SetRightEarPosition(  Point ptPos )
{
    m_ptRightEar = ptPos;
}

        public void SetLeftEarPosition(  Point ptPos )
{
    m_ptLeftEar = ptPos;
}

        public void SetChinPosition(  Point ptPos )
{
    m_ptChin = ptPos;
}

        public void SetGlabellaPosition(  Point ptPos )
{
    m_ptGlabella = ptPos;
}

        public void SetAllPositionDetected()
{
    m_bUnderBodyPositionDetected = true;
    m_bKneePositionDetected = true;
    m_bUpperBodyPositionDetected = true;
}

        public void SetUnderBodYPositionDetected()
{
    m_bUnderBodyPositionDetected = true;
}

void SetKneePositionDetected()
{
    m_bKneePositionDetected = true;
}

        public void CopYRightHipPosition(FrontBodyPosition rSrc )
{
    m_ptRightHip = rSrc.m_ptRightHip;
}

        public void CopYRightKneePosition(FrontBodyPosition rSrc )
{
    m_ptRightKnee = rSrc.m_ptRightKnee;
}

        public void CopYRightAnklePosition(FrontBodyPosition rSrc )
{
    m_ptRightAnkle = rSrc.m_ptRightAnkle;
}

        public void CopYLeftHipPosition(FrontBodyPosition rSrc )
{
    m_ptLeftHip = rSrc.m_ptLeftHip;
}

        public void CopYLeftKneePosition(FrontBodyPosition rSrc )
{
    m_ptLeftKnee = rSrc.m_ptLeftKnee;
}

        public void CopYLeftAnklePosition(FrontBodyPosition rSrc )
{
    m_ptLeftAnkle = rSrc.m_ptLeftAnkle;
}

        public void CopYRightBeltPosition(FrontBodyPosition rSrc )
{
    m_ptRightBelt = rSrc.m_ptRightBelt;
}

        public void CopYLeftBeltPosition(FrontBodyPosition rSrc )
{
    m_ptLeftBelt = rSrc.m_ptLeftBelt;
}

        public void CopYRightShoulderPosition(FrontBodyPosition rSrc )
{
    m_ptRightShoulder = rSrc.m_ptRightShoulder;
}

        public void CopYLeftShoulderPosition(FrontBodyPosition rSrc )
{
    m_ptLeftShoulder = rSrc.m_ptLeftShoulder;
}

        public void CopYRightEarPosition(FrontBodyPosition rSrc )
{
    m_ptRightEar = rSrc.m_ptRightEar;
}

        public void CopYLeftEarPosition(FrontBodyPosition rSrc )
{
    m_ptLeftEar = rSrc.m_ptLeftEar;
}

        public  void CopyChinPosition(FrontBodyPosition rSrc )
{
    m_ptChin = rSrc.m_ptChin;
}

        public void CopYGlabellaPosition( FrontBodyPosition  rSrc )
{
    m_ptGlabella = rSrc.m_ptGlabella;
}

        public void CalcKneePosition(int width, int height, double ratio, double error)
{
    //double error = CalcPostureProp.GetKneeRatioError();
    //double ratio = CalcPostureProp.GetBelt2KneeRatio();

    m_ptLeftKnee.Y = (int)(m_ptLeftAnkle.Y * (1.0 - (ratio + error)) + m_ptLeftHip.Y * (ratio + error));
    m_ptLeftKnee.X= (int)(m_ptLeftAnkle.X * (1.0 - (ratio + error)) + m_ptLeftHip.X * (ratio + error));
    m_ptRightKnee.Y = (int)(m_ptRightAnkle.Y * (1.0 - (ratio + error)) + m_ptRightHip.Y * (ratio + error));
    m_ptRightKnee.X = (int)(m_ptRightAnkle.X * (1.0 - (ratio + error)) + m_ptRightHip.X * (ratio + error));

    Adjust(width, height);
}

        public void CalcUpperPosition(int width, int height, double dRateShoulder, double dRateEar, double dRateChin, double dRateGlabella)
{
    int hipwidth = m_ptLeftHip.X - m_ptRightHip.X;
    int leglength = Math.Abs(m_ptLeftHip.Y - m_ptLeftAnkle.Y);

    if (hipwidth > 100)
    {
        hipwidth = 100;
    }

    m_ptRightShoulder.X = (int)(m_ptRightHip.X - (int)(hipwidth * 0.3));
    m_ptLeftShoulder.X = (int)(m_ptLeftHip.X + (int)(hipwidth * 0.3));
    m_ptRightEar.X = m_ptRightHip.X;
    m_ptLeftEar.X = m_ptLeftHip.X;
    m_ptChin.X = (m_ptLeftHip.X + m_ptRightHip.X) / 2;
    m_ptGlabella.X = (m_ptLeftHip.X + m_ptRightHip.X) / 2;

    m_ptRightShoulder.Y = m_ptRightHip.Y - (int)(leglength * dRateShoulder);
    m_ptLeftShoulder.Y = m_ptRightHip.Y - (int)(leglength * dRateShoulder);
    m_ptRightEar.Y = m_ptRightHip.Y - (int)(leglength * dRateEar);
    m_ptLeftEar.Y = m_ptRightHip.Y - (int)(leglength * dRateEar);
    m_ptChin.Y = m_ptRightHip.Y - (int)(leglength * dRateChin);
    m_ptGlabella.Y = m_ptRightHip.Y- (int)(leglength * dRateGlabella);

    Adjust(width, height);
}

        public void DetectKneePos(int width, int height, double dBelt2KneeRatio)
{
    // ‡AƒXƒ‹ƒg‚Ì‚‚³-‘«Žñ‚ÌˆÊ’u‚©‚ç•G‚Ì‚‚³‚ðÝ’è
    //int lleglength = abs( m_iLeftAnkleY - m_iLeftHipY );
    //int rleglength = abs( m_iRightAnkleY - m_iRightHipY );

    m_bKneePositionDetected = false;    // •K‚¸Žè“®‚ÅˆÊ’uŒˆ‚ß‚µ‚Ä‚à‚ç‚¤

    m_ptLeftKnee.Y = (int)(m_ptLeftAnkle.Y * (1.0 - dBelt2KneeRatio) + m_ptLeftHip.Y * dBelt2KneeRatio);
    m_ptLeftKnee.X = (int)(m_ptLeftAnkle.X * (1.0 - dBelt2KneeRatio) + m_ptLeftHip.X * dBelt2KneeRatio);
    m_ptRightKnee.Y = (int)(m_ptRightAnkle.Y * (1.0 - dBelt2KneeRatio) + m_ptRightHip.Y * dBelt2KneeRatio);
    m_ptRightKnee.X = (int)(m_ptRightAnkle.X * (1.0 - dBelt2KneeRatio) + m_ptRightHip.X * dBelt2KneeRatio);

    Adjust(width, height);
}

        public void CalcBeltPrepare(int width, int height, double dHipMko, double dBelt2HipRatio)
{
    double[] lineParm = new double[2];
    lineParm[0] = (double)(m_ptLeftBelt.Y - m_ptRightBelt.Y) / (m_ptLeftBelt.X - m_ptRightBelt.X);
    lineParm[1] = (double)m_ptRightBelt.Y - lineParm[0] * (double)m_ptRightBelt.X;

    // ‡E¶‰E’[‚©‚ç‚W‚O“‚ðŒÒŠÖß‚Æ‚µ‚ÄÝ’è
    int beltwidth = (int)Math.Abs(m_ptLeftBelt.X - m_ptRightBelt.X);

    m_ptRightHip.X = (int)(m_ptRightBelt.X + beltwidth * dHipMko);
    m_ptLeftHip.X = (int)(m_ptRightBelt.X + beltwidth * (1.0 - dHipMko));

    // ‰¼‚ÌˆÊ’u
    m_ptRightHip.Y = (int)(lineParm[0] * (double)(m_ptRightHip.X) + lineParm[1]);
    m_ptLeftHip.Y = (int)(lineParm[0] * (double)(m_ptLeftHip.X) + lineParm[1]);

    int rleglength = (int)Math.Abs(m_ptRightHip.Y - m_ptRightAnkle.Y);
    int lleglength = (int)Math.Abs(m_ptLeftHip.Y - m_ptLeftAnkle.Y);

    m_ptRightHip.Y = (int)(m_ptRightHip.Y + lleglength * dBelt2HipRatio);
    m_ptLeftHip.Y = (int)(m_ptLeftHip.Y + rleglength * dBelt2HipRatio);

    Adjust(width, height);
}

        public void Adjust(int width, int height)
{
    if (m_ptRightHip.X < 0)
    {
        m_ptRightHip.X = 0;
    }
    if (m_ptRightHip.X >= width)
    {
        m_ptRightHip.X = width - 1;
    }
    if (m_ptRightHip.Y < 0)
    {
        m_ptRightHip.Y = 0;
    }
    if (m_ptRightHip.Y >= height)
    {
        m_ptRightHip.Y = height - 1;
    }
    if (m_ptLeftHip.X < 0)
    {
        m_ptLeftHip.X = 0;
    }
    if (m_ptLeftHip.X >= width)
    {
        m_ptLeftHip.X = width - 1;
    }
    if (m_ptLeftHip.Y < 0)
    {
        m_ptLeftHip.Y = 0;
    }
    if (m_ptLeftHip.Y >= height)
    {
        m_ptLeftHip.Y = height - 1;
    }
    if (m_ptRightKnee.X < 0)
    {
        m_ptRightKnee.X = 0;
    }
    if (m_ptRightKnee.X >= width)
    {
        m_ptRightKnee.X = width - 1;
    }
    if (m_ptRightKnee.Y < 0)
    {
        m_ptRightKnee.Y = 0;
    }
    if (m_ptRightKnee.Y >= height)
    {
        m_ptRightKnee.Y = height - 1;
    }
    if (m_ptLeftKnee.X < 0)
    {
        m_ptLeftKnee.X = 0;
    }
    if (m_ptLeftKnee.X >= width)
    {
        m_ptLeftKnee.X = width - 1;
    }
    if (m_ptLeftKnee.Y < 0)
    {
        m_ptLeftKnee.Y = 0;
    }
    if (m_ptLeftKnee.Y >= height)
    {
        m_ptLeftKnee.Y = height - 1;
    }
    if (m_ptRightAnkle.X < 0)
    {
        m_ptRightAnkle.X = 0;
    }
    if (m_ptRightAnkle.X >= width)
    {
        m_ptRightAnkle.X = width - 1;
    }
    if (m_ptRightAnkle.Y < 0)
    {
        m_ptRightAnkle.Y = 0;
    }
    if (m_ptRightAnkle.Y >= height)
    {
        m_ptRightAnkle.Y = height - 1;
    }
    if (m_ptLeftAnkle.X < 0)
    {
        m_ptLeftAnkle.X = 0;
    }
    if (m_ptLeftAnkle.X >= width)
    {
        m_ptLeftAnkle.X = width - 1;
    }
    if (m_ptLeftAnkle.Y < 0)
    {
        m_ptLeftAnkle.Y = 0;
    }
    if (m_ptLeftAnkle.Y >= height)
    {
        m_ptLeftAnkle.Y = height - 1;
    }
    if (m_ptRightBelt.X < 0)
    {
        m_ptRightBelt.X = 0;
    }
    if (m_ptRightBelt.X >= width)
    {
        m_ptRightBelt.X = width - 1;
    }
    if (m_ptRightBelt.Y < 0)
    {
        m_ptRightBelt.Y = 0;
    }
    if (m_ptRightBelt.Y >= height)
    {
        m_ptRightBelt.Y = height - 1;
    }
    if (m_ptLeftBelt.X < 0)
    {
        m_ptLeftBelt.X = 0;
    }
    if (m_ptLeftBelt.X >= width)
    {
        m_ptLeftBelt.X = width - 1;
    }
    if (m_ptLeftBelt.Y < 0)
    {
        m_ptLeftBelt.Y = 0;
    }
    if (m_ptLeftBelt.Y >= height)
    {
        m_ptLeftBelt.Y = height - 1;
    }
    if (m_ptRightShoulder.X < 0)
    {
        m_ptRightShoulder.X = 0;
    }
    if (m_ptRightShoulder.X >= width)
    {
        m_ptRightShoulder.X = width - 1;
    }
    if (m_ptRightShoulder.Y < 0)
    {
        m_ptRightShoulder.Y = 0;
    }
    if (m_ptRightShoulder.Y >= height)
    {
        m_ptRightShoulder.Y = height - 1;
    }
    if (m_ptLeftShoulder.X < 0)
    {
        m_ptLeftShoulder.X = 0;
    }
    if (m_ptLeftShoulder.X >= width)
    {
        m_ptLeftShoulder.X = width - 1;
    }
    if (m_ptLeftShoulder.Y < 0)
    {
        m_ptLeftShoulder.Y = 0;
    }
    if (m_ptLeftShoulder.Y >= height)
    {
        m_ptLeftShoulder.Y = height - 1;
    }
    if (m_ptRightEar.X < 0)
    {
        m_ptRightEar.X = 0;
    }
    if (m_ptRightEar.X >= width)
    {
        m_ptRightEar.X = width - 1;
    }
    if (m_ptRightEar.Y < 0)
    {
        m_ptRightEar.Y = 0;
    }
    if (m_ptRightEar.Y >= height)
    {
        m_ptRightEar.Y = height - 1;
    }
    if (m_ptLeftEar.X < 0)
    {
        m_ptLeftEar.X = 0;
    }
    if (m_ptLeftEar.X >= width)
    {
        m_ptLeftEar.X = width - 1;
    }
    if (m_ptLeftEar.Y < 0)
    {
        m_ptLeftEar.Y = 0;
    }
    if (m_ptLeftEar.Y >= height)
    {
        m_ptLeftEar.Y = height - 1;
    }
    if (m_ptChin.X < 0)
    {
        m_ptChin.X = 0;
    }
    if (m_ptChin.X >= width)
    {
        m_ptChin.X = width - 1;
    }
    if (m_ptChin.Y < 0)
    {
        m_ptChin.Y = 0;
    }
    if (m_ptChin.Y >= height)
    {
        m_ptChin.Y = height - 1;
    }
    if (m_ptGlabella.X < 0)
    {
        m_ptGlabella.X = 0;
    }
    if (m_ptGlabella.X >= width)
    {
        m_ptGlabella.X = width - 1;
    }
    if (m_ptGlabella.Y < 0)
    {
        m_ptGlabella.Y = 0;
    }
    if (m_ptGlabella.Y >= height)
    {
        m_ptGlabella.Y = height - 1;
    }
}

        public int GetRightHipX() 
{
	return m_ptRightHip.X;
}

        public int GetRightHipY() 
{
	return m_ptRightHip.Y;
}

        public int GetLeftHipX() 
{
	return m_ptLeftHip.X;
}

        public int GetLeftHipY() 
{
	return m_ptLeftHip.Y;
}

        public int GetRightKneeX() 
{
	return m_ptRightKnee.X;
}

        public int GetRightKneeY() 
{
	return m_ptRightKnee.Y;
}

        public int GetLeftKneeX() 
{
	return m_ptLeftKnee.X;
}

        public int GetLeftKneeY() 
{
	return m_ptLeftKnee.Y;
}

        public int GetRightAnkleX() 
{
	return m_ptRightAnkle.X;
}

        public int GetRightAnkleY() 
{
	return m_ptRightAnkle.Y;
}

        public int GetLeftAnkleX() 
{
	return m_ptLeftAnkle.X;
}

        public int GetLeftAnkleY() 
{
	return m_ptLeftAnkle.Y;
}

        public int GetRightBeltX() 
{
	return m_ptRightBelt.X;
}

        public int GetRightBeltY() 
{
	return m_ptRightBelt.Y;
}

        public int GetLeftBeltX() 
{
	return m_ptLeftBelt.X;
}

        public int GetLeftBeltY() 
{
	return m_ptLeftBelt.Y;
}

        public int GetRightShoulderX() 
{
	return m_ptRightShoulder.X;
}

        public int GetRightShoulderY() 
{
	return m_ptRightShoulder.Y;
}

        public int GetLeftShoulderX() 
{
	return m_ptLeftShoulder.X;
}

        public int GetLeftShoulderY() 
{
	return m_ptLeftShoulder.Y;
}

        public int GetRightEarX() 
{
	return m_ptRightEar.X;
}

        public int GetRightEarY() 
{
	return m_ptRightEar.Y;
}

        public int GetLeftEarX() 
{
	return m_ptLeftEar.X;
}

        public int GetLeftEarY() 
{
	return m_ptLeftEar.Y;
}

        public int GetChinX() 
{
	return m_ptChin.X;
}

        public int GetChinY() 
{
	return m_ptChin.Y;
}

        public int GetGlabellaX() 
{
	return m_ptGlabella.X;
}

        public int GetGlabellaY() 
{
	return m_ptGlabella.Y;
}

        public bool IsUnderBodyPositionDetected() 
{
	return m_bUnderBodyPositionDetected;
}

        public bool IsKneePositionDetected() 
{
	return m_bKneePositionDetected;
}

        public bool IsUpperBodyPositionDetected() 
{
	return m_bUpperBodyPositionDetected;
}

        public int CalcDataSizeOfStandingStatementBlock() 
{
int iRet = 0;
/*iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightHipX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightHipY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftHipX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftHipY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightKneeX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightKneeY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftKneeX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftKneeY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightAnkleX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightAnkleY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftAnkleX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftAnkleY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightBeltX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightBeltY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftBeltX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftBeltY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightShoulderX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightShoulderY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftShoulderX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftShoulderY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightEarX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingRightEarY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftEarX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingLeftEarY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingChinX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingChinY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingGlabellaX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingGlabellaY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingUnderBodYPositionDetected" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingKneePositionDetected" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "StandingUpperBodYPositionDetected" ) + sizeof(int);*/
	return iRet;
}

        public int WriteStandingStatementBlock(string pbYteOut, int iOffset, int iSize) 
{
	/*iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightHipX", GetRightHipX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightHipY", GetRightHipY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftHipX", GetLeftHipX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftHipY", GetLeftHipY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightKneeX", GetRightKneeX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightKneeY", GetRightKneeY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftKneeX", GetLeftKneeX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftKneeY", GetLeftKneeY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightAnkleX", GetRightAnkleX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightAnkleY", GetRightAnkleY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftAnkleX", GetLeftAnkleX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftAnkleY", GetLeftAnkleY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightBeltX", GetRightBeltX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightBeltY", GetRightBeltY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftBeltX", GetLeftBeltX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftBeltY", GetLeftBeltY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightShoulderX", GetRightShoulderX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightShoulderY", GetRightShoulderY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftShoulderX", GetLeftShoulderX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftShoulderY", GetLeftShoulderY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightEarX", GetRightEarX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingRightEarY", GetRightEarY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftEarX", GetLeftEarX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingLeftEarY", GetLeftEarY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingChinX", GetChinX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingChinY", GetChinY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingGlabellaX", GetGlabellaX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingGlabellaY", GetGlabellaY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingUnderBodYPositionDetected", IsUnderBodYPositionDetected() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingKneePositionDetected", IsKneePositionDetected() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize,  "StandingUpperBodYPositionDetected", IsUpperBodYPositionDetected() );*/
	return iOffset;
}

int EXecuteStandingIntegerAssignmentStatement(  string pchSYmbolName, int iSYmbolNameLength, int iValue)
{
 /*   if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightHipX"))
    {
        m_ptRightHip.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightHipY"))
    {
        m_ptRightHip.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftHipX"))
    {
        m_ptLeftHip.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftHipY"))
    {
        m_ptLeftHip.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightKneeX"))
    {
        m_ptRightKnee.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightKneeY"))
    {
        m_ptRightKnee.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftKneeX"))
    {
        m_ptLeftKnee.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftKneeY"))
    {
        m_ptLeftKnee.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightAnkleX"))
    {
        m_ptRightAnkle.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightAnkleY"))
    {
        m_ptRightAnkle.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftAnkleX"))
    {
        m_ptLeftAnkle.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftAnkleY"))
    {
        m_ptLeftAnkle.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightBeltX"))
    {
        m_ptRightBelt.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightBeltY"))
    {
        m_ptRightBelt.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftBeltX"))
    {
        m_ptLeftBelt.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftBeltY"))
    {
        m_ptLeftBelt.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightShoulderX"))
    {
        m_ptRightShoulder.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightShoulderY"))
    {
        m_ptRightShoulder.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftShoulderX"))
    {
        m_ptLeftShoulder.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftShoulderY"))
    {
        m_ptLeftShoulder.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightEarX"))
    {
        m_ptRightEar.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingRightEarY"))
    {
        m_ptRightEar.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftEarX"))
    {
        m_ptLeftEar.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingLeftEarY"))
    {
        m_ptLeftEar.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingChinX"))
    {
        m_ptChin.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingChinY"))
    {
        m_ptChin.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingGlabellaX"))
    {
        m_ptGlabella.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingGlabellaY"))
    {
        m_ptGlabella.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingUnderBodYPositionDetected"))
    {
        m_bUnderBodYPositionDetected = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingKneePositionDetected"))
    {
        m_bKneePositionDetected = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "StandingUpperBodYPositionDetected"))
    {
        m_bUpperBodYPositionDetected = iValue;
        return 1;
    }*/
    return 0;
}

int CalcDataSizeOfKneedownStatementBlock( ) 
{
	int iRet = 0;
/*iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightHipX" )  + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightHipY" )  + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftHipX" )  + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftHipY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightKneeX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightKneeY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftKneeX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftKneeY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightAnkleX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightAnkleY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftAnkleX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftAnkleY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightBeltX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightBeltY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftBeltX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftBeltY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightShoulderX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightShoulderY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftShoulderX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftShoulderY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightEarX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownRightEarY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftEarX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownLeftEarY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownChinX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownChinY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownGlabellaX" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownGlabellaY" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownUnderBodYPositionDetected" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownKneePositionDetected" ) + sizeof(int);
	iRet += CalcDataSizeOfShortAssignmentStatement(  "KneedownUpperBodYPositionDetected" ) + sizeof(int);*/
	return iRet;
}

int WriteKneedownStatementBlock(string pbYteOut, int iOffset, int iSize) 
{
	/*iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightHipX", GetRightHipX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightHipY", GetRightHipY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftHipX", GetLeftHipX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftHipY", GetLeftHipY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightKneeX", GetRightKneeX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightKneeY", GetRightKneeY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftKneeX", GetLeftKneeX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftKneeY", GetLeftKneeY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightAnkleX", GetRightAnkleX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightAnkleY", GetRightAnkleY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftAnkleX", GetLeftAnkleX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftAnkleY", GetLeftAnkleY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightBeltX", GetRightBeltX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightBeltY", GetRightBeltY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftBeltX", GetLeftBeltX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftBeltY", GetLeftBeltY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightShoulderX", GetRightShoulderX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightShoulderY", GetRightShoulderY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftShoulderX", GetLeftShoulderX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftShoulderY", GetLeftShoulderY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightEarX", GetRightEarX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownRightEarY", GetRightEarY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftEarX", GetLeftEarX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownLeftEarY", GetLeftEarY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownChinX", GetChinX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownChinY", GetChinY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownGlabellaX", GetGlabellaX() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownGlabellaY", GetGlabellaY() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownUnderBodYPositionDetected", IsUnderBodYPositionDetected() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownKneePositionDetected", IsKneePositionDetected() );
	iOffset = WriteShortAssignmentStatement(pbYteOut, iOffset, iSize, "KneedownUpperBodYPositionDetected", IsUpperBodYPositionDetected() );*/
	return iOffset;
}

int EXecuteKneedownIntegerAssignmentStatement(  string pchSYmbolName, int iSYmbolNameLength, int iValue)
{
 /*   if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightHipX"))
    {
        m_ptRightHip.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightHipY"))
    {
        m_ptRightHip.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftHipX"))
    {
        m_ptLeftHip.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftHipY"))
    {
        m_ptLeftHip.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightKneeX"))
    {
        m_ptRightKnee.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightKneeY"))
    {
        m_ptRightKnee.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftKneeX"))
    {
        m_ptLeftKnee.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftKneeY"))
    {
        m_ptLeftKnee.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightAnkleX"))
    {
        m_ptRightAnkle.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightAnkleY"))
    {
        m_ptRightAnkle.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftAnkleX"))
    {
        m_ptLeftAnkle.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftAnkleY"))
    {
        m_ptLeftAnkle.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightBeltX"))
    {
        m_ptRightBelt.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightBeltY"))
    {
        m_ptRightBelt.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftBeltX"))
    {
        m_ptLeftBelt.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftBeltY"))
    {
        m_ptLeftBelt.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightShoulderX"))
    {
        m_ptRightShoulder.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightShoulderY"))
    {
        m_ptRightShoulder.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftShoulderX"))
    {
        m_ptLeftShoulder.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftShoulderY"))
    {
        m_ptLeftShoulder.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightEarX"))
    {
        m_ptRightEar.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownRightEarY"))
    {
        m_ptRightEar.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftEarX"))
    {
        m_ptLeftEar.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownLeftEarY"))
    {
        m_ptLeftEar.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownChinX"))
    {
        m_ptChin.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownChinY"))
    {
        m_ptChin.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownGlabellaX"))
    {
        m_ptGlabella.X = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownGlabellaY"))
    {
        m_ptGlabella.Y = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownUnderBodYPositionDetected"))
    {
        m_bUnderBodYPositionDetected = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownKneePositionDetected"))
    {
        m_bKneePositionDetected = iValue;
        return 1;
    }
    if (CompareSYmbol(pchSYmbolName, iSYmbolNameLength, "KneedownUpperBodYPositionDetected"))
    {
        m_bUpperBodYPositionDetected = iValue;
        return 1;
    }*/
    return 0;
}

    }
}
