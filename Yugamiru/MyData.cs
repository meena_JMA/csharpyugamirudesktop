﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MyData
    {

        int m_ID;       // ID
        string m_Name;     // –¼‘O
        int m_Sex;      // «•Ê 0:UNKNOWN 1:’j 2:—
                        //int			m_DoB;		// ¶”NŒŽ“ú
        int m_iBirthDay;
        int m_iBirthMonth;
        int m_iBirthYear;
        string m_Time;     // ‘ª’è“úŽž YYYYMMDDhhmmss
        double m_Height; // g’·
        string m_Comment;  // ƒRƒƒ“ƒg
        int m_iBenchmarkDistance;

        FrontBodyPosition m_FrontBodyPositionStanding = new FrontBodyPosition(); // ŠÖß‚ÌÀ•Wi‘ª’èŠJŽnŽžj
        FrontBodyPosition m_FrontBodyPositionKneedown = new FrontBodyPosition(); // ŠÖß‚ÌÀ•Wi‘ª’èŠ®—¹Žžj
        SideBodyPosition m_SideBodyPosition = new SideBodyPosition();               

        int version;
        public MyData()
        {

            m_ID = 0;
            m_Name = "";
            m_Sex = 0;
            //m_DoB	 = 0;
            m_iBirthDay = 0;
            m_iBirthMonth = 0;
            m_iBirthYear = 0;
            m_Height = 0.0;
            m_Comment = "";
            m_iBenchmarkDistance = 70;

            //version = VER_2006Summer_RELEASE; Meenakshi
        }

        ~MyData()
        {
        }

        public void SetDoB(string strY, string strM, string strD)
        {
            //	m_DoB = atoi(strY)*10000 + atoi(strM)*100 + atoi(strD);
            m_iBirthDay = Convert.ToInt32(strD);
            m_iBirthMonth = Convert.ToInt32(strM);
            m_iBirthYear = Convert.ToInt32(strY);
        }

        public void GetDoB(string strY, string strM, string strD)
        {
            //	CString str;
            //	str.Format("%d", m_DoB);

            //	strY = str.Mid(0, 4);
            //	strM = str.Mid(4, 2);
            //	strD = str.Mid(6, 2);
            strY = Convert.ToString(m_iBirthYear);
            strM = Convert.ToString(m_iBirthMonth);
            strD = Convert.ToString(m_iBirthDay);
        }

        /* ‘ª’è“úŽžEŽžŠÔ‚ð”NEŒŽE“úEŽžŠÔ‚É•ª‰ð‚µ‚ÄƒZƒbƒg */
        public void GetAcqDate(string strY, string strM, string strD, string strT)
        {
            string str;
            str = m_Time;

            //	strY = "20";

            //	strY += str.Mid(0, 2);
            strY = str.Substring(0, 2);
            strM = str.Substring(3, 2);
            strD = str.Substring(6, 2);
            strT = str.Substring(9, 5);

        }

        /* DOBƒf[ƒ^‚ð”NEŒŽE“ú‚É•ª‰ð‚µ‚ÄƒZƒbƒg */
        /*public int GetAge() 
        {
        //	int birth = m_DoB;
            int d0, d1, m0, m1, y0, y1;
        //	d1 = birth%100;	birth/=100;
        //	m1 = birth%100;	birth/=100;
        //	y1 = birth;

        d1 = m_iBirthDay;
            m1 = m_iBirthMonth;
            y1 = m_iBirthYear;

            //struct tm * today;
        //time_t t;
                DateTime dt1 = new DateTime(y1,m1,d1);
                    DateTime dt2 = new DateTime(GetLocalTime);

            time(&t);
        today = localtime(&t);
        y0 = today->tm_year + 1900;
            m0 = today->tm_mon + 1;
            d0 = today->tm_mday;

            if (d0<d1) m0--;
            if (m0<m1) y0--;

            return ( y0 - y1 );
        }*/

        /*
        @”N—î‚ð‚R¢‘ã‚É•ª‰ð
        @25ÎˆÈ‰ºA55ÎˆÈ‰ºA‚»‚êˆÈã
        @‚½‚¾‚µAŒ»’iŠK‚Å‚Í”»’è‚Í¢‘ã‚Åˆá‚¢‚ª–³‚¢
        */
        /*public int GetGene() 
        {
            if( GetAge() < 26){
                return 0;
            }
            else if( GetAge() < 56){
                return 1;
            }
            else{
                return 2;
            }
        }*/

        public bool IsDetected()
        {
            if (!m_FrontBodyPositionStanding.IsUnderBodyPositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionStanding.IsKneePositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionStanding.IsUpperBodyPositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionKneedown.IsUnderBodyPositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionKneedown.IsKneePositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionKneedown.IsUpperBodyPositionDetected())
            {
                return false;
            }
            return true;
        }

        /* ƒf[ƒ^‚Ì‰Šú‰» */
        public void ClearData()
        {
            m_ID = 0;
            m_Name = "";
            m_Sex = 0;
            //	m_DoB = 0;
            m_iBirthDay = 0;
            m_iBirthMonth = 0;
            m_iBirthYear = 0;
            m_Height = 0.0;
            m_iBenchmarkDistance = 70;
            m_Comment = "";
        }


        public void InitBodyBalance(int width, int height, CCalibInf CalibInf, CCalcPostureProp CalcPostureProp)
        {
            m_FrontBodyPositionStanding.Init(width, height, CalibInf, CalcPostureProp);
            m_FrontBodyPositionKneedown.Init(width, height, CalibInf, CalcPostureProp);
            //m_SideBodyPosition.Init(width, height, CalibInf, CalcPostureProp);

            m_FrontBodyPositionStanding.InitUpperPostion(width, height, CalibInf);
            m_FrontBodyPositionKneedown.InitUpperPostion(width, height, CalibInf);
            //m_SideBodyPosition.InitUpperPostion(width, height, CalibInf);
        }

        public void CalcBodyAngleStanding(FrontBodyAngle angleStanding, double dCameraAngle)
        {
            angleStanding.CalcByPosition(true, m_FrontBodyPositionStanding, dCameraAngle);
        }

        public void CalcBodyAngleKneedown(FrontBodyAngle angleKneedown, double dCameraAngle)
        {
            angleKneedown.CalcByPosition(false, m_FrontBodyPositionKneedown, dCameraAngle);

        }

        public void CalcBodyAngleSide(SideBodyAngle angleSide, double dCameraAngle) 
        {
            angleSide.CalcByPosition( m_SideBodyPosition, dCameraAngle ); 
        }
        /*
        public void CalcStandingFrontBodyResultData(FrontBodyResultData resultStanding, double dCameraAngle, const CResultActionScriptMgr &ResultActionScriptMgr) const */
        /* CResultPropArray &ResultPropArray */
        /*{
            FrontBodyAngle bodyAngleStanding;
        bodyAngleStanding.CalcByPosition( true, &m_FrontBodyPositionStanding, dCameraAngle );

            resultStanding.Reset();
            int sex = ((m_Sex == 2) ? 1 : 0);
        int gene = GetGene();
        resultStanding.Calc( sex, gene, bodyAngleStanding, ResultActionScriptMgr );
        }

        void CData::CalcKneedownFrontBodyResultData(CFrontBodyResultData &resultKneedown, double dCameraAngle, const CResultActionScriptMgr &ResultActionScriptMgr /* CResultPropArray &ResultPropArray *//* ) const*/
                                                                                                                                                                                                          /*{
                                                                                                                                                                                                              CFrontBodyAngle bodyAngleKneedown;
                                                                                                                                                                                                          bodyAngleKneedown.CalcByPosition( FALSE, &m_FrontBodyPositionKneedown, dCameraAngle );

                                                                                                                                                                                                              resultKneedown.Reset();
                                                                                                                                                                                                              int sex = ((m_Sex == 2) ? 1 : 0);
                                                                                                                                                                                                          int gene = GetGene();
                                                                                                                                                                                                          resultKneedown.Calc( sex, gene, bodyAngleKneedown, ResultActionScriptMgr );
                                                                                                                                                                                                          }

                                                                                                                                                                                                          void CData::CalcSideBodyResultData(CSideBodyResultData &resultSide, double dCameraAngle,
                                                                                                                                                                                                                          const CResultActionScriptMgr &ResultActionScriptMgr ) const
                                                                                                                                                                                                          {
                                                                                                                                                                                                              CSideBodyAngle bodyAngleSide;
                                                                                                                                                                                                          bodyAngleSide.CalcByPosition( &m_SideBodyPosition, dCameraAngle );

                                                                                                                                                                                                              resultSide.Reset();
                                                                                                                                                                                                              int sex = ((m_Sex == 2) ? 1 : 0);
                                                                                                                                                                                                          int gene = GetGene();
                                                                                                                                                                                                          resultSide.Calc( sex, gene, bodyAngleSide, ResultActionScriptMgr );
                                                                                                                                                                                                          }

                                                                                                                                                                                                          void CData::CalcBodyBalanceResultData(
                                                                                                                                                                                                                  CResultData &result,
                                                                                                                                                                                                                  double dCameraAngle,
                                                                                                                                                                                                                  const CResultActionScriptMgr &ResultActionScriptMgr,
                                                                                                                                                                                                                  const CResultActionScriptMgr &ResultActionScriptMgrSide ) const
                                                                                                                                                                                                          {
                                                                                                                                                                                                              CFrontBodyAngle bodyAngleStanding;
                                                                                                                                                                                                          bodyAngleStanding.CalcByPosition( TRUE, &m_FrontBodyPositionStanding, dCameraAngle );

                                                                                                                                                                                                              CFrontBodyAngle bodyAngleKneedown;
                                                                                                                                                                                                          bodyAngleKneedown.CalcByPosition( FALSE, &m_FrontBodyPositionKneedown, dCameraAngle );

                                                                                                                                                                                                              CSideBodyAngle bodyAngleSide;
                                                                                                                                                                                                          bodyAngleSide.CalcByPosition( &m_SideBodyPosition, dCameraAngle );


                                                                                                                                                                                                              result.Reset();

                                                                                                                                                                                                              int sex = ((m_Sex == 2) ? 1 : 0);
                                                                                                                                                                                                          int gene = GetGene();
                                                                                                                                                                                                          result.Calc( 
                                                                                                                                                                                                                  sex, 
                                                                                                                                                                                                                  gene, 
                                                                                                                                                                                                                  bodyAngleStanding,
                                                                                                                                                                                                                  bodyAngleKneedown,
                                                                                                                                                                                                                  bodyAngleSide,
                                                                                                                                                                                                                  ResultActionScriptMgr,
                                                                                                                                                                                                                  ResultActionScriptMgrSide );
                                                                                                                                                                                                          }

                                                                                                                                                                                                          int CData::CalcYumgamiPoint(
                                                                                                                                                                                                                  double dCameraAngle,
                                                                                                                                                                                                                  const CResultActionScriptMgr &ResultActionScriptMgr,
                                                                                                                                                                                                                  const CResultActionScriptMgr &ResultActionScriptMgrSide ) const
                                                                                                                                                                                                          {
                                                                                                                                                                                                              CResultData result;

                                                                                                                                                                                                              CalcBodyBalanceResultData(
                                                                                                                                                                                                                  result,
                                                                                                                                                                                                                  dCameraAngle,
                                                                                                                                                                                                                  ResultActionScriptMgr,
                                                                                                                                                                                                                  ResultActionScriptMgrSide );
                                                                                                                                                                                                              return result.CalcYugamiPoint();
                                                                                                                                                                                                          }

                                                                                                                                                                                                          int CData::CalcYumgamiPointRank(
                                                                                                                                                                                                                  double dCameraAngle,
                                                                                                                                                                                                                  const CResultActionScriptMgr &ResultActionScriptMgr,
                                                                                                                                                                                                                  const CResultActionScriptMgr &ResultActionScriptMgrSide ) const
                                                                                                                                                                                                          {
                                                                                                                                                                                                              CResultData result;

                                                                                                                                                                                                              CalcBodyBalanceResultData(
                                                                                                                                                                                                                  result,
                                                                                                                                                                                                                  dCameraAngle,
                                                                                                                                                                                                                  ResultActionScriptMgr,
                                                                                                                                                                                                                  ResultActionScriptMgrSide );
                                                                                                                                                                                                              return result.CalcYugamiPointRank();
                                                                                                                                                                                                          }*/


        public int GetID()
        {
            return m_ID;
        }

        public void SetID(int pchID)
        {
            m_ID = pchID;
        }

        public string GetName()
        {
            return m_Name;
        }

        public void SetName(string pchName)
        {
            m_Name = pchName;
        }

        public int GetSex()
        {
            return m_Sex;
        }

        public void SetSex(int iSex)
        {
            m_Sex = iSex;
        }

        public double GetHeight()
        {
            return m_Height;
        }

        public string GetMeasurementTime()
        {
            return m_Time;
        }

        public void SetMeasurementTime(string pchMeasurementTime)
        {
            m_Time = pchMeasurementTime;
        }


        public void SetHeight(float fHeight)
        {
            m_Height = fHeight;
        }

        public string GetComment()
        {
            return m_Comment;
        }

        public void SetComment(string pchComment)
        {
            m_Comment = pchComment;
        }

        public int GetBenchmarkDistance()
        {
            return m_iBenchmarkDistance;
        }

        public void SetBenchmarkDistance(int iBenchmarkDistance)
        {
            m_iBenchmarkDistance = iBenchmarkDistance;
        }

        public int GetVersion()
        {
            return version;
        }

        public void GetStandingFrontBodyPosition(FrontBodyPosition FrontBodyPositionStanding)
        {
            FrontBodyPositionStanding = m_FrontBodyPositionStanding;
        }

        public void GetKneedownFrontBodyPosition(FrontBodyPosition FrontBodyPositionKneedown)
        {
            FrontBodyPositionKneedown = m_FrontBodyPositionKneedown;
        }

        /*public void GetSideBodyPosition(SideBodyPosition SideBodyPosition ) const
        {
            SideBodyPosition = m_SideBodyPosition;
        }*/

        public void SetStandingFrontBodyPosition(FrontBodyPosition FrontBodyPositionStanding)
        {
            m_FrontBodyPositionStanding = FrontBodyPositionStanding;
        }

        public void SetKneedownFrontBodyPosition(FrontBodyPosition FrontBodyPositionKneedown)
        {
            m_FrontBodyPositionKneedown = FrontBodyPositionKneedown;
        }

        /*public void SetSideBodyPosition( CSideBodyPosition &SideBodyPosition )
        {
            m_SideBodyPosition = SideBodyPosition;
        }*/
        /*
        void CData::InitSideBodyPosition(int width, int height, const CCalibInf &CalibInf, const CCalcPostureProp &CalcPostureProp )
        {
            m_SideBodyPosition.Init(width, height, CalibInf, CalcPostureProp);
            m_SideBodyPosition.InitUpperPostion(width, height, CalibInf);
        }

        int CData::CalcDataSizeOfStatementBlock(void ) const
        {
            CString strTmp;
        CString strBirthYear;
        CString strBirthMonth;
        CString strBirthDay;

        int iRet = 0;
        iRet += sizeof(int);	// ƒuƒƒbƒN‘S‘Ì‚ÌƒTƒCƒY.
            iRet += CalcDataSizeOfStringAssignmentStatement( "UserID", GetID() ) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement( "UserName", GetName() ) + sizeof(int);

            GetDoB(strBirthYear, strBirthMonth, strBirthDay );
        iRet += CalcDataSizeOfStringAssignmentStatement( "UserBirthYear", strBirthYear ) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement( "UserBirthMonth", strBirthMonth ) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement( "UserBirthDay", strBirthDay ) + sizeof(int);
            strTmp.Format( "%d", GetSex() );
            iRet += CalcDataSizeOfStringAssignmentStatement( "UserSex", strTmp ) + sizeof(int);
            strTmp.Format( "%.1f", GetHeight() );
            iRet += CalcDataSizeOfStringAssignmentStatement( "UserHeight", strTmp ) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement( "MeasurementTime", GetMeasurementTime() ) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement( "Comment", GetComment() ) + sizeof(int);
            strTmp.Format( "%d", GetBenchmarkDistance() );
            iRet += CalcDataSizeOfStringAssignmentStatement( "BenchmarkDistance", strTmp ) + sizeof(int);
            iRet += m_FrontBodyPositionStanding.CalcDataSizeOfStandingStatementBlock();
            iRet += m_FrontBodyPositionKneedown.CalcDataSizeOfKneedownStatementBlock();
            iRet += m_SideBodyPosition.CalcDataSizeOfStatementBlock();
            return iRet;
        }

        int CData::WriteStatementBlock(unsigned char* pbyteOut, int iOffset, int iSize) const
        {
            CString strTmp;
        CString strBirthYear;
        CString strBirthMonth;
        CString strBirthDay;


            ASSERT(iOffset<iSize );
        //int iBlockSize = CalcDataSizeOfStatementBlock();
        //iOffset += WriteIntDataToMemory( pbyteOut + iOffset, iBlockSize );
        iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserID", GetID() );

            ASSERT(iOffset<iSize );
        iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserName", GetName() );

            ASSERT(iOffset<iSize );

            GetDoB(strBirthYear, strBirthMonth, strBirthDay );
        iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserBirthYear", strBirthYear );

            ASSERT(iOffset<iSize );
        iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserBirthMonth", strBirthMonth );

            ASSERT(iOffset<iSize );
        iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserBirthDay", strBirthDay );

            ASSERT(iOffset<iSize );
        strTmp.Format( "%d", GetSex() );
            iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserSex", strTmp );

            ASSERT(iOffset<iSize );
        strTmp.Format( "%.1f", GetHeight() );
            iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserHeight", strTmp );

            ASSERT(iOffset<iSize );
        iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "MeasurementTime", GetMeasurementTime() );

            ASSERT(iOffset<iSize );
        iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "Comment", GetComment() );

            ASSERT(iOffset<iSize );
        strTmp.Format( "%d", GetBenchmarkDistance() );
            iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "BenchmarkDistance", strTmp );

            ASSERT(iOffset<iSize );
        iOffset = m_FrontBodyPositionStanding.WriteStandingStatementBlock( pbyteOut, iOffset, iSize );

            ASSERT(iOffset<iSize );
        iOffset = m_FrontBodyPositionKneedown.WriteKneedownStatementBlock( pbyteOut, iOffset, iSize );

            ASSERT(iOffset<iSize );
        iOffset = m_SideBodyPosition.WriteStatementBlock( pbyteOut, iOffset, iSize );

            ASSERT(iOffset<iSize );
            return iOffset;
        }

        int CData::ReadStatementBlock( const unsigned char* pbyteIn)
        {
            int iBlockSize = 0;
            int iOffset = 0;
            iOffset += ReadIntDataFromMemory(pbyteIn + iOffset, iBlockSize);
            while (iOffset < iBlockSize)
            {
                int iStatementSize = 0;
                iOffset += ReadIntDataFromMemory(pbyteIn + iOffset, iStatementSize);
                int iOpeCode = 0;
                iOffset += ReadUnsignedCharDataFromMemory(pbyteIn + iOffset, iOpeCode);
                int iSymbolSize = 0;
                iOffset += ReadUnsignedCharDataFromMemory(pbyteIn + iOffset, iSymbolSize);
                const char* pchSymbol = ( const char* )(pbyteIn + iOffset);
                iOffset += iSymbolSize;
                if (iOpeCode == 1)
                {
                    // StringAssignmentStatement.
                    int iValueSize = 0;
                    iOffset += ReadUnsignedShortDataFromMemory(pbyteIn + iOffset, iValueSize);
                    const char* pchValue = ( const char* )(pbyteIn + iOffset);
                    iOffset += iValueSize;
                    ExecuteStringAssignmentStatement(pchSymbol, iSymbolSize, pchValue, iValueSize);
                }
                else if (iOpeCode == 2)
                {
                    // ShortAssignmentStatement.
                    int iValue = 0;
                    iOffset += ReadShortDataFromMemory(pbyteIn + iOffset, iValue);
                    ExecuteIntegerAssignmentStatement(pchSymbol, iSymbolSize, iValue);
                }
                else if (iOpeCode == 3)
                {
                    // ImageAssignmentStatement.
                    int iImageSize = 0;
                    iOffset += ReadIntDataFromMemory(pbyteIn + iOffset, iImageSize);
                    const unsigned char* pbyteImage = pbyteIn + iOffset;
                    iOffset += iImageSize;
                    ExecuteImageAssignmentStatement(pchSymbol, iSymbolSize, pbyteImage, iImageSize);
                }
            }
            return iOffset;
        }

        int CData::ExecuteIntegerAssignmentStatement( const char* pchSymbolName, int iSymbolNameLength, int iValue)
        {
            if (m_FrontBodyPositionStanding.ExecuteStandingIntegerAssignmentStatement(pchSymbolName, iSymbolNameLength, iValue))
            {
                return 1;
            }
            if (m_FrontBodyPositionKneedown.ExecuteKneedownIntegerAssignmentStatement(pchSymbolName, iSymbolNameLength, iValue))
            {
                return 1;
            }
            if (m_SideBodyPosition.ExecuteIntegerAssignmentStatement(pchSymbolName, iSymbolNameLength, iValue))
            {
                return 1;
            }
            return 0;
        }

        int CData::ExecuteStringAssignmentStatement( const char* pchSymbolName, int iSymbolNameLength, const char* pchValue, int iValueLength)
        {
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "UserID"))
            {
                AssignToString(m_ID, pchValue, iValueLength);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "UserName"))
            {
                AssignToString(m_Name, pchValue, iValueLength);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "UserBirthYear"))
            {
                CString strTmp;
                AssignToString(strTmp, pchValue, iValueLength);
                m_iBirthYear = atoi(strTmp);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "UserBirthMonth"))
            {
                CString strTmp;
                AssignToString(strTmp, pchValue, iValueLength);
                m_iBirthMonth = atoi(strTmp);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "UserBirthDay"))
            {
                CString strTmp;
                AssignToString(strTmp, pchValue, iValueLength);
                m_iBirthDay = atoi(strTmp);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "UserSex"))
            {
                CString strTmp;
                AssignToString(strTmp, pchValue, iValueLength);
                m_Sex = atoi(strTmp);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "UserHeight"))
            {
                CString strTmp;
                AssignToString(strTmp, pchValue, iValueLength);
                m_Height = (float)atof(strTmp);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "MeasurementTime"))
            {
                AssignToString(m_Time, pchValue, iValueLength);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "Comment"))
            {
                AssignToString(m_Comment, pchValue, iValueLength);
                return 1;
            }
            if (CompareSymbol(pchSymbolName, iSymbolNameLength, "BenchmarkDistance"))
            {
                CString strTmp;
                AssignToString(strTmp, pchValue, iValueLength);
                m_iBenchmarkDistance = atoi(strTmp);
                return 1;
            }
            return 0;
        }

        int CData::ExecuteImageAssignmentStatement( const char* pchSymbolName, int iSymbolNameLength, const unsigned char* pbyteImage, int iImageSize)
        {
            return 0;
        }*/

    }
}
