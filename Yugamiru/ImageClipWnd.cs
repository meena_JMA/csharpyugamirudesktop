﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Yugamiru
{
    class ImageClipWnd
    {
        // ImageClipWnd.cpp : ŽÀ‘•ƒtƒ@ƒCƒ‹
        //

        /*# include "stdafx.h"
        # include "ImageClipWnd.h"
        # include <stdio.h>
        # include <winuser.h>
        # include "highgui.h"
        # include "cvexJPEGStreamIO.h"
        # include <Shlwapi.h> 

        # ifdef _DEBUG
        #define new DEBUG_NEW
        #undef THIS_FILE
                static char THIS_FILE[] = __FILE__;
        #endif

        // CImageClipWnd

        IMPLEMENT_DYNAMIC(CImageClipWnd, CWnd)*/

        string m_pbyteBits;
        Bitmap m_bmi;
        Graphics m_pDCOffscreen;
        Bitmap m_pbmOffscreen;
        Bitmap m_pbmOffscreenOld;
        int m_iOffscreenWidth;
        int m_iOffscreenHeight;
        int m_iBackgroundWidth;
        int m_iBackgroundHeight;
        int m_iDestRectWidth;
        int m_iDestRectHeight;
        int m_iDestRectUpperLeftCornerX;
        int m_iDestRectUpperLeftCornerY;
        int m_iSelectionFrameWidth;
        int m_iSelectionFrameHeight;
        int m_iSelectionFrameUpperLeftCornerX;
        int m_iSelectionFrameUpperLeftCornerY;
        int m_iMouseCaptureMode;
        bool m_bDragAcceptFilesFlag;
        string m_strDragFileName;

        ImageClipWnd()
        {

            m_pbyteBits= null;

    m_pDCOffscreen = null;

            m_pbmOffscreen = null;

            m_pbmOffscreenOld = null;

            m_iOffscreenWidth = 0;

    m_iOffscreenHeight = 0;

            m_iBackgroundWidth = 0;

            m_iBackgroundHeight = 0;

            m_iDestRectWidth = 0;

            m_iDestRectHeight = 0;

            m_iDestRectUpperLeftCornerX = 0;

            m_iDestRectUpperLeftCornerY = 0;

            m_iSelectionFrameWidth = 0;

            m_iSelectionFrameHeight = 0;

            m_iSelectionFrameUpperLeftCornerX = 0;

            m_iSelectionFrameUpperLeftCornerY = 0;

            m_iMouseCaptureMode = 0;

            m_bDragAcceptFilesFlag = false;
        
            //memset(&m_bmi, 0, sizeof(m_bmi));
        }

        ~ImageClipWnd()
        {
            Cleanup();
        }

        void Cleanup( )
        {
            if (m_pbyteBits != null)
            {
                //delete[] m_pbyteBits;
                m_pbyteBits = null;
            }
            if (m_pDCOffscreen != null)
            {
                if (m_pbmOffscreenOld != null)
                {
                    //m_pDCOffscreen->SelectObject(m_pbmOffscreenOld);
                    m_pbmOffscreenOld = null;
                }
                //delete m_pDCOffscreen;
                m_pDCOffscreen = null;
            }
            if (m_pbmOffscreen != null)
            {
                //delete m_pbmOffscreen;
                m_pbmOffscreen = null;
            }
        }

/*


BEGIN_MESSAGE_MAP(CImageClipWnd, CWnd)

    ON_WM_PAINT()

    ON_WM_DESTROY()

    ON_WM_SIZE()

    ON_WM_ERASEBKGND()

    ON_WM_CREATE()

    ON_WM_LBUTTONDOWN()

    ON_WM_LBUTTONUP()

    ON_WM_MOUSEMOVE()

    ON_WM_DROPFILES()
END_MESSAGE_MAP()

    

// CImageClipWnd ƒƒbƒZ[ƒW ƒnƒ“ƒhƒ‰



        void OnPaint()
        {
            Graphics dc; // device context for painting

            Rectangle rcClient;
            GetClientRect(rcClient);
            if ((m_pDCOffscreen != NULL) &&
                (rcClient.Width() == m_iOffscreenWidth) &&
                (rcClient.Height() == m_iOffscreenHeight))
            {
                dc.BitBlt(0, 0, rcClient.Width(), rcClient.Height(), m_pDCOffscreen, 0, 0, SRCCOPY);
            }
            else
            {
                dc.FillSolidRect(&rcClient, RGB(0, 0, 0));
            }
        }

        void CImageClipWnd::OnDestroy()
        {
            Cleanup();
            CWnd::OnDestroy();
        }

        void CImageClipWnd::OnSize(UINT nType, int cx, int cy)
        {
            CWnd::OnSize(nType, cx, cy);
            if (m_pDCOffscreen != NULL)
            {
                if ((cx == m_iOffscreenWidth) && (cy == m_iOffscreenHeight))
                {
                    // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ÌÄ¶¬•s—v.
                    return;
                }
                // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ðÄ¶¬.
                if (m_pbmOffscreenOld != NULL)
                {
                    m_pDCOffscreen->SelectObject(m_pbmOffscreenOld);
                    m_pbmOffscreenOld = NULL;
                }
            }
            else
            {
                // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ðV‹K¶¬.
                m_pDCOffscreen = new CDC;
                CDC* pDC = GetDC();
                m_pDCOffscreen->CreateCompatibleDC(pDC);
                ReleaseDC(pDC);
                pDC = NULL;
            }

            if (m_pbmOffscreen == NULL)
            {
                m_pbmOffscreen = new CBitmap;
            }
            if (m_pbmOffscreen != NULL)
            {
                delete m_pbmOffscreen;
                m_pbmOffscreen = NULL;
            }
            m_pbmOffscreen = new CBitmap;
            CDC* pDC = GetDC();
            m_pbmOffscreen->CreateCompatibleBitmap(pDC, cx, cy);
            ReleaseDC(pDC);
            pDC = NULL;
            m_pbmOffscreenOld = m_pDCOffscreen->SelectObject(m_pbmOffscreen);
            if (m_pbyteBits != NULL)
            {
                m_pDCOffscreen->SetStretchBltMode(HALFTONE);

        ::StretchDIBits(m_pDCOffscreen->GetSafeHdc(), 0, 0, cx, cy,
            0, (m_iBackgroundHeight - 1 - (0 + m_iBackgroundHeight - 1)),
            m_iBackgroundWidth, m_iBackgroundHeight,
            m_pbyteBits, &m_bmi, DIB_RGB_COLORS, SRCCOPY);
            }
            else
            {
                m_pDCOffscreen->FillSolidRect(0, 0, cx, cy, RGB(0, 0, 0));
            }
            m_iOffscreenWidth = cx;
            m_iOffscreenHeight = cy;
        }

        BOOL CImageClipWnd::OnEraseBkgnd(CDC* pDC)
        {
            return TRUE;
        }

        BOOL CImageClipWnd::SetBackgroundBitmap( const char* pchFileName )
{

    IplImage* piplimageSrc = ::cvLoadImage(pchFileName, CV_LOAD_IMAGE_COLOR);
	if ( piplimageSrc == NULL ){
		return FALSE;
	}
    BOOL bRet = SetBackgroundBitmap(piplimageSrc);
	if ( piplimageSrc != NULL ){
		::cvReleaseImage( &piplimageSrc );
    piplimageSrc = NULL;	
	}
	return bRet;
}

BOOL CImageClipWnd::SetBackgroundBitmap( const ITEMIDLIST* pItemIDListImageFile)
{
    DWORD dwTick1 = GetTickCount();

    IShellFolder* pShellFolderDesktop = NULL;
    if (SHGetDesktopFolder(&pShellFolderDesktop) != S_OK)
    {
        return FALSE;
    }
    IStream* pStream = NULL;
    HRESULT hRes = pShellFolderDesktop->BindToObject(pItemIDListImageFile, NULL, IID_IStream, (void**)&pStream);
    if (hRes != S_OK)
    {
        if (pShellFolderDesktop != NULL)
        {
            pShellFolderDesktop->Release();
            pShellFolderDesktop = NULL;
        }
        return FALSE;
    }
    if (pShellFolderDesktop != NULL)
    {
        pShellFolderDesktop->Release();
        pShellFolderDesktop = NULL;
    }
    LARGE_INTEGER li;
    li.HighPart = 0;
    li.LowPart = 0;
    // Œø—¦‰»‚Ì‚½‚ßAƒƒ‚ƒŠƒXƒgƒŠ[ƒ€‚Ö‚ÌƒRƒs[‚ª‰Â”\‚È‚ç‚ÎAƒRƒs[‚ðs‚¤B.
    IStream* pStreamMem = NULL;
    if (CreateStreamOnHGlobal(NULL, TRUE, &pStreamMem) == S_OK)
    {
        // ƒƒ‚ƒŠƒXƒgƒŠ[ƒ€¶¬¬Œ÷.
        //OutputDebugString("CreateStreamOnHGlobal Succeeded\n");
        ULARGE_INTEGER uli;
        uli.HighPart = 0xFFFFFFFF;
        uli.LowPart = 0xFFFFFFFF;
        if (pStream->CopyTo(pStreamMem, uli, NULL, NULL) == S_OK)
        {
            //OutputDebugString("CopyTo Succeeded\n");
            // ƒƒ‚ƒŠƒXƒgƒŠ[ƒ€‚ÉƒRƒs[¬Œ÷.
            if (pStreamMem->Seek(li, SEEK_SET, NULL) == S_OK)
            {
                //OutputDebugString("Seek Succeeded\n");
                // æ“ª‚ÉƒV[ƒN¬Œ÷.
                // Œ´ƒXƒgƒŠ[ƒ€‚ð‰ð•ú‚µA¡Œã‚ÍŒ´ƒXƒgƒŠ[ƒ€‚Ì‘ã‚í‚è‚É.
                // ƒƒ‚ƒŠƒXƒgƒŠ[ƒ€‚ðŽg—p‚·‚éB.
                pStream->Release();
                pStream = pStreamMem;
                pStreamMem = NULL;
            }
            else
            {
                // ƒƒ‚ƒŠƒXƒgƒŠ[ƒ€‚ÌƒV[ƒN‚ÉŽ¸”s‚µ‚½.
                // Œ´ƒXƒgƒŠ[ƒ€‚ÌƒV[ƒN‚ðs‚¤.
                if (pStream->Seek(li, SEEK_SET, NULL) != S_OK)
                {
                    if (pStreamMem != NULL)
                    {
                        pStreamMem->Release();
                        pStreamMem = NULL;
                    }
                    return FALSE;
                }
            }
        }
        if (pStreamMem != NULL)
        {
            pStreamMem->Release();
            pStreamMem = NULL;
        }

    }

    //ULARGE_INTEGER uli;
    //if ( IStream_Size( pStream, &uli ) == S_OK ){
    //	char szMessage[100];
    //	sprintf( &(szMessage[0]), "Stream Size %I64u\n", uli );
    //	OutputDebugString( &(szMessage[0]) );
    //}

    //DWORD dwTick1 = GetTickCount();

    IplImage* piplimageSrc = cvexJPEGDecodeFromStream(pStream, 0);
    if (pStream != NULL)
    {
        pStream->Release();
        pStream = NULL;
    }
#if 0
	DWORD dwTick2 = GetTickCount();
	{
		char szMessage[100];
		sprintf( &(szMessage[0]), "Tick %d\n", dwTick2-dwTick1 );
		OutputDebugString( &(szMessage[0]) );
	}
#endif
    if (piplimageSrc == NULL)
    {
        return FALSE;
    }
    BOOL bRet = SetBackgroundBitmap(piplimageSrc);
    if (piplimageSrc != NULL)
    {

        ::cvReleaseImage(&piplimageSrc);
        piplimageSrc = NULL;
    }
    return bRet;
}

BOOL CImageClipWnd::SetBackgroundBitmap(IplImage* piplimageSrc)
{
    if (piplimageSrc == NULL)
    {
        return FALSE;
    }
    int iBmpWidthStep = (piplimageSrc->width * 3 + 3) / 4 * 4;
    int iBmpBitsSize = iBmpWidthStep * piplimageSrc->height;
    if (iBmpBitsSize <= 0)
    {
        return FALSE;
    }
    if (m_pbyteBits != NULL)
    {
        delete[] m_pbyteBits;
        m_pbyteBits = NULL;
    }
    m_pbyteBits = new unsigned char[iBmpBitsSize];
    if (m_pbyteBits == NULL)
    {
        return FALSE;
    }
    int i = 0;
    int j = 0;
    for (i = 0; i < piplimageSrc->height; i++)
    {
        const unsigned char* pbyteSrc = (const unsigned char*)(piplimageSrc->imageData + i * piplimageSrc->widthStep);
    unsigned char* pbyteDest = m_pbyteBits + (piplimageSrc->height - 1 - i) * iBmpWidthStep;
    for (j = 0; j < piplimageSrc->width * piplimageSrc->nChannels; j++)
    {
        pbyteDest[j] = pbyteSrc[j];
    }
}

m_bmi.bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);
	m_bmi.bmiHeader.biWidth			= piplimageSrc->width;
	m_bmi.bmiHeader.biHeight		= piplimageSrc->height;
	m_bmi.bmiHeader.biPlanes		= 1;
	m_bmi.bmiHeader.biBitCount		= 24;
	m_bmi.bmiHeader.biCompression	= 0;
	m_bmi.bmiHeader.biSizeImage		= 0;
	m_bmi.bmiHeader.biXPelsPerMeter	= 0;
	m_bmi.bmiHeader.biYPelsPerMeter	= 0;
	m_bmi.bmiHeader.biClrUsed		= 0;
	m_bmi.bmiHeader.biClrImportant	= 0;

	m_bmi.bmiColors[0].rgbBlue		= 255;
	m_bmi.bmiColors[0].rgbGreen		= 255;
	m_bmi.bmiColors[0].rgbRed		= 255;
	m_bmi.bmiColors[0].rgbReserved	= 255;

	m_iBackgroundWidth	= piplimageSrc->width;
	m_iBackgroundHeight	= piplimageSrc->height;

	int iExtendedBitmapWidth = m_iBackgroundWidth + m_iBackgroundHeight * 1024 / 1280;
m_iDestRectWidth = m_iBackgroundWidth* m_iOffscreenWidth / iExtendedBitmapWidth;
	m_iDestRectHeight = m_iBackgroundHeight* m_iOffscreenWidth / iExtendedBitmapWidth;
	m_iDestRectUpperLeftCornerX = m_iOffscreenWidth / 2 - m_iDestRectWidth / 2;
	m_iDestRectUpperLeftCornerY = m_iOffscreenHeight / 2 - m_iDestRectHeight / 2;

	m_iSelectionFrameWidth = m_iBackgroundHeight* 1024 / 1280;
	m_iSelectionFrameHeight = m_iBackgroundHeight;
	m_iSelectionFrameUpperLeftCornerX = m_iBackgroundWidth / 2 - m_iSelectionFrameWidth / 2;
	m_iSelectionFrameUpperLeftCornerY = m_iBackgroundHeight / 2 - m_iSelectionFrameHeight / 2;

	return TRUE;
}

BOOL CImageClipWnd::SetBackgroundBitmap(int iWidth, int iHeight, const unsigned char* pbyteBits)
{
    m_bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    m_bmi.bmiHeader.biWidth = iWidth;
    m_bmi.bmiHeader.biHeight = iHeight;
    m_bmi.bmiHeader.biPlanes = 1;
    m_bmi.bmiHeader.biBitCount = 24;
    m_bmi.bmiHeader.biCompression = 0;
    m_bmi.bmiHeader.biSizeImage = 0;
    m_bmi.bmiHeader.biXPelsPerMeter = 0;
    m_bmi.bmiHeader.biYPelsPerMeter = 0;
    m_bmi.bmiHeader.biClrUsed = 0;
    m_bmi.bmiHeader.biClrImportant = 0;

    m_bmi.bmiColors[0].rgbBlue = 255;
    m_bmi.bmiColors[0].rgbGreen = 255;
    m_bmi.bmiColors[0].rgbRed = 255;
    m_bmi.bmiColors[0].rgbReserved = 255;

    m_iBackgroundWidth = iWidth;
    m_iBackgroundHeight = iHeight;

    int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
    int iBmpBitsSize = iBmpWidthStep * iHeight;
    if (iBmpBitsSize <= 0)
    {
        return FALSE;
    }

    if (m_pbyteBits != NULL)
    {
        delete[] m_pbyteBits;
        m_pbyteBits = NULL;
    }
    m_pbyteBits = new unsigned char[iBmpBitsSize];
    if (m_pbyteBits == NULL)
    {
        return FALSE;
    }
    int i = 0;
    for (i = 0; i < iBmpBitsSize; i++)
    {
        m_pbyteBits[i] = pbyteBits[i];
    }

    int iExtendedBitmapWidth = m_iBackgroundWidth + m_iBackgroundHeight * 1024 / 1280;
    m_iDestRectWidth = m_iBackgroundWidth * m_iOffscreenWidth / iExtendedBitmapWidth;
    m_iDestRectHeight = m_iBackgroundHeight * m_iOffscreenWidth / iExtendedBitmapWidth;
    m_iDestRectUpperLeftCornerX = m_iOffscreenWidth / 2 - m_iDestRectWidth / 2;
    m_iDestRectUpperLeftCornerY = m_iOffscreenHeight / 2 - m_iDestRectHeight / 2;

    m_iSelectionFrameWidth = m_iBackgroundHeight * 1024 / 1280;
    m_iSelectionFrameHeight = m_iBackgroundHeight;
    m_iSelectionFrameUpperLeftCornerX = m_iBackgroundWidth / 2 - m_iSelectionFrameWidth / 2;
    m_iSelectionFrameUpperLeftCornerY = m_iBackgroundHeight / 2 - m_iSelectionFrameHeight / 2;

    return TRUE;
}

BOOL CImageClipWnd::SetBackgroundBitmap(int iWidth, int iHeight)
{
    m_bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    m_bmi.bmiHeader.biWidth = iWidth;
    m_bmi.bmiHeader.biHeight = iHeight;
    m_bmi.bmiHeader.biPlanes = 1;
    m_bmi.bmiHeader.biBitCount = 24;
    m_bmi.bmiHeader.biCompression = 0;
    m_bmi.bmiHeader.biSizeImage = 0;
    m_bmi.bmiHeader.biXPelsPerMeter = 0;
    m_bmi.bmiHeader.biYPelsPerMeter = 0;
    m_bmi.bmiHeader.biClrUsed = 0;
    m_bmi.bmiHeader.biClrImportant = 0;

    m_bmi.bmiColors[0].rgbBlue = 255;
    m_bmi.bmiColors[0].rgbGreen = 255;
    m_bmi.bmiColors[0].rgbRed = 255;
    m_bmi.bmiColors[0].rgbReserved = 255;

    m_iBackgroundWidth = iWidth;
    m_iBackgroundHeight = iHeight;

    int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
    int iBmpBitsSize = iBmpWidthStep * iHeight;
    if (iBmpBitsSize <= 0)
    {
        return FALSE;
    }

    if (m_pbyteBits != NULL)
    {
        delete[] m_pbyteBits;
        m_pbyteBits = NULL;
    }
    m_pbyteBits = new unsigned char[iBmpBitsSize];
    if (m_pbyteBits == NULL)
    {
        return FALSE;
    }
    int i = 0;
    for (i = 0; i < iBmpBitsSize; i++)
    {
        m_pbyteBits[i] = 0;
    }

    int iExtendedBitmapWidth = m_iBackgroundWidth + m_iBackgroundHeight * 1024 / 1280;
    m_iDestRectWidth = m_iBackgroundWidth * m_iOffscreenWidth / iExtendedBitmapWidth;
    m_iDestRectHeight = m_iBackgroundHeight * m_iOffscreenWidth / iExtendedBitmapWidth;
    m_iDestRectUpperLeftCornerX = m_iOffscreenWidth / 2 - m_iDestRectWidth / 2;
    m_iDestRectUpperLeftCornerY = m_iOffscreenHeight / 2 - m_iDestRectHeight / 2;

    m_iSelectionFrameWidth = m_iBackgroundHeight * 1024 / 1280;
    m_iSelectionFrameHeight = m_iBackgroundHeight;
    m_iSelectionFrameUpperLeftCornerX = m_iBackgroundWidth / 2 - m_iSelectionFrameWidth / 2;
    m_iSelectionFrameUpperLeftCornerY = m_iBackgroundHeight / 2 - m_iSelectionFrameHeight / 2;

    return TRUE;
}

void CImageClipWnd::UpdateOffscreen(void )
{
    if (m_pDCOffscreen == NULL)
    {
        return;
    }

    if (m_pbyteBits != NULL)
    {

        m_pDCOffscreen->FillSolidRect(0, 0, m_iOffscreenWidth, m_iOffscreenHeight, RGB(0, 0, 0));
        m_pDCOffscreen->SetStretchBltMode(HALFTONE);


        ::StretchDIBits(
            m_pDCOffscreen->GetSafeHdc(),
            m_iDestRectUpperLeftCornerX,
            m_iDestRectUpperLeftCornerY,
            m_iDestRectWidth,
            m_iDestRectHeight,
            0,
            (m_iBackgroundHeight - 1 - (0 + m_iBackgroundHeight - 1)),
            m_iBackgroundWidth,
            m_iBackgroundHeight,
            m_pbyteBits,
            &m_bmi,
            DIB_RGB_COLORS,
            SRCCOPY);

        CPen penNew(PS_SOLID, 0, RGB(255, 0, 0) );
        CPen* ppenOld = m_pDCOffscreen->SelectObject(&penNew);
        int iXPos = m_iSelectionFrameUpperLeftCornerX * m_iDestRectWidth / m_iBackgroundWidth + m_iDestRectUpperLeftCornerX;
        int iYPos = m_iSelectionFrameUpperLeftCornerY * m_iDestRectHeight / m_iBackgroundHeight + m_iDestRectUpperLeftCornerY;
        int iWidth = m_iSelectionFrameWidth * m_iDestRectWidth / m_iBackgroundWidth;
        int iHeight = m_iSelectionFrameHeight * m_iDestRectHeight / m_iBackgroundHeight;

        m_pDCOffscreen->MoveTo(iXPos, iYPos);
        m_pDCOffscreen->LineTo(iXPos + iWidth, iYPos);
        m_pDCOffscreen->LineTo(iXPos + iWidth, iYPos + iHeight);
        m_pDCOffscreen->LineTo(iXPos, iYPos + iHeight);
        m_pDCOffscreen->LineTo(iXPos, iYPos);
        m_pDCOffscreen->SelectObject(ppenOld);
    }
}

int CImageClipWnd::GetBackgroundWidth(void ) const
{
	return m_iBackgroundWidth;
}

int CImageClipWnd::GetBackgroundHeight(void ) const
{
	return m_iBackgroundHeight;
}

int CImageClipWnd::GetSelectedBitmapWidth(void ) const
{
	return 1024;
}

int CImageClipWnd::GetSelectedBitmapHeight(void ) const
{
	return 1280;
}

int CImageClipWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CWnd::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }
    DragAcceptFiles(m_bDragAcceptFilesFlag);
    return 0;
}

void OnLButtonDown(int nFlags, Point point)
{
    SetCapture();
    m_iMouseCaptureMode = 1;
    int iSelectionFrameCenterX = (point.x - m_iDestRectUpperLeftCornerX) * m_iBackgroundWidth / m_iDestRectWidth;
    if (iSelectionFrameCenterX < 0)
    {
        iSelectionFrameCenterX = 0;
    }
    if (iSelectionFrameCenterX >= m_iBackgroundWidth)
    {
        iSelectionFrameCenterX = m_iBackgroundWidth;
    }
    m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX - m_iSelectionFrameWidth / 2;
}

void CImageClipWnd::OnLButtonUp(UINT nFlags, CPoint point)
{
    ReleaseCapture();
    m_iMouseCaptureMode = 0;
}

void CImageClipWnd::OnMouseMove(UINT nFlags, Point point)
{
    if (m_iMouseCaptureMode)
    {
        int iSelectionFrameCenterX = (point.x - m_iDestRectUpperLeftCornerX) * m_iBackgroundWidth / m_iDestRectWidth;
        if (iSelectionFrameCenterX < 0)
        {
            iSelectionFrameCenterX = 0;
        }
        if (iSelectionFrameCenterX >= m_iBackgroundWidth)
        {
            iSelectionFrameCenterX = m_iBackgroundWidth;
        }
        m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX - m_iSelectionFrameWidth / 2;

        UpdateOffscreen();
        CRect rcClient;
        GetClientRect(rcClient);
        InvalidateRect(&rcClient);
        UpdateWindow();
    }
}

int CImageClipWnd::CalcBackgroundBitmapSize(void ) const
{
	return( ( m_iBackgroundWidth* 3 + 3 ) / 4 * 4 * m_iBackgroundHeight );
}

int CImageClipWnd::CalcSelectedBitmapSize(void ) const
{
	return( ( GetSelectedBitmapWidth() * 3 + 3 ) / 4 * 4 * GetSelectedBitmapHeight() );
}

void CImageClipWnd::GetBackgroundBitmap(HDC hDCRef, unsigned char* pbyteBits) const
{
	int iBmpWidthStep = (m_iBackgroundWidth * 3 + 3) / 4 * 4;
int iBmpBitsSize = iBmpWidthStep * m_iBackgroundHeight;
	if ( iBmpBitsSize > 0 ){
		int i = 0;
		for( i = 0; i<iBmpBitsSize; i++ ){
			pbyteBits[i] = m_pbyteBits[i];
		}
	}
}

void CImageClipWnd::GetSelectedBitmap(HDC hDCRef, unsigned char* pbyteBits) const
{
	BITMAPINFO bmi;
bmi.bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth			= GetSelectedBitmapWidth();
bmi.bmiHeader.biHeight			= GetSelectedBitmapHeight();
bmi.bmiHeader.biPlanes			= 1;
	bmi.bmiHeader.biBitCount		= 24;
	bmi.bmiHeader.biCompression		= 0;
	bmi.bmiHeader.biSizeImage		= 0;
	bmi.bmiHeader.biXPelsPerMeter	= 0;
	bmi.bmiHeader.biYPelsPerMeter	= 0;
	bmi.bmiHeader.biClrUsed			= 0;
	bmi.bmiHeader.biClrImportant	= 0;

	CDC* pDCRef = CDC::FromHandle(hDCRef);

CDC dcMem;
dcMem.CreateCompatibleDC( pDCRef );
	CBitmap bmMem;
bmMem.CreateCompatibleBitmap( pDCRef, GetSelectedBitmapWidth(), GetSelectedBitmapHeight() );
	CBitmap* pbmOld = dcMem.SelectObject(&bmMem);
dcMem.SetStretchBltMode( HALFTONE );
	::StretchDIBits(dcMem.GetSafeHdc(),
		0, 0, GetSelectedBitmapWidth(), GetSelectedBitmapHeight(),
		m_iSelectionFrameUpperLeftCornerX, 
		( m_iBackgroundHeight - 1 - ( m_iSelectionFrameUpperLeftCornerY + m_iSelectionFrameHeight - 1 )), 
		m_iSelectionFrameWidth,
		m_iSelectionFrameHeight, 
		m_pbyteBits, &m_bmi, DIB_RGB_COLORS, SRCCOPY );
	dcMem.SelectObject( pbmOld );
	pbmOld = NULL;
	::GetDIBits(hDCRef, (HBITMAP)(bmMem.GetSafeHandle()), 0, GetSelectedBitmapHeight(), pbyteBits, &bmi, DIB_RGB_COLORS );
}

void CImageClipWnd::RotateClockwiseBackgroundBitmap(HDC hDCRef)
{
    int iSrcImageWidth = m_iBackgroundWidth;
    int iSrcImageHeight = m_iBackgroundHeight;
    int iSrcImageWidthStep = (iSrcImageWidth * 3 + 3) / 4 * 4;
    int iSrcImageAllocSize = iSrcImageWidthStep * iSrcImageHeight;
    if (iSrcImageAllocSize <= 0)
    {
        return;
    }
    int iDestImageWidth = m_iBackgroundHeight;
    int iDestImageHeight = m_iBackgroundWidth;
    int iDestImageWidthStep = (iDestImageWidth * 3 + 3) / 4 * 4;
    int iDestImageAllocSize = iDestImageWidthStep * iDestImageHeight;
    if (iDestImageAllocSize <= 0)
    {
        return;
    }
    unsigned char* pbyteBitsSrc = new unsigned char[iSrcImageAllocSize];
    unsigned char* pbyteBitsDest = new unsigned char[iDestImageAllocSize];
    if ((pbyteBitsSrc == NULL) || (pbyteBitsDest == NULL))
    {
        if (pbyteBitsSrc != NULL)
        {
            delete[] pbyteBitsSrc;
            pbyteBitsSrc = NULL;
        }
        if (pbyteBitsDest != NULL)
        {
            delete[] pbyteBitsDest;
            pbyteBitsDest = NULL;
        }
    }
    GetBackgroundBitmap(hDCRef, pbyteBitsSrc);
    int i = 0;
    int j = 0;
    for (i = 0; i < iDestImageHeight; i++)
    {
        const unsigned char* pbyteSrcBase = pbyteBitsSrc + 3 * i;
        unsigned char* pbyteDestBase = pbyteBitsDest + (iDestImageHeight - 1 - i) * iDestImageWidthStep;
        for (j = 0; j < iDestImageWidth; j++)
        {
            const unsigned char* pbyteSrc = pbyteSrcBase + j * iSrcImageWidthStep;
            unsigned char* pbyteDest = pbyteDestBase + j * 3;
            pbyteDest[0] = pbyteSrc[0];
            pbyteDest[1] = pbyteSrc[1];
            pbyteDest[2] = pbyteSrc[2];
        }
    }
    if (pbyteBitsSrc != NULL)
    {
        delete[] pbyteBitsSrc;
        pbyteBitsSrc = NULL;
    }
    SetBackgroundBitmap(iDestImageWidth, iDestImageHeight, pbyteBitsDest);
    if (pbyteBitsDest != NULL)
    {
        delete[] pbyteBitsDest;
        pbyteBitsDest = NULL;
    }
}

void CImageClipWnd::RotateAntiClockwiseBackgroundBitmap(HDC hDCRef)
{
    int iSrcImageWidth = m_iBackgroundWidth;
    int iSrcImageHeight = m_iBackgroundHeight;
    int iSrcImageWidthStep = (iSrcImageWidth * 3 + 3) / 4 * 4;
    int iSrcImageAllocSize = iSrcImageWidthStep * iSrcImageHeight;
    if (iSrcImageAllocSize <= 0)
    {
        return;
    }
    int iDestImageWidth = m_iBackgroundHeight;
    int iDestImageHeight = m_iBackgroundWidth;
    int iDestImageWidthStep = (iDestImageWidth * 3 + 3) / 4 * 4;
    int iDestImageAllocSize = iDestImageWidthStep * iDestImageHeight;
    if (iDestImageAllocSize <= 0)
    {
        return;
    }
    unsigned char* pbyteBitsSrc = new unsigned char[iSrcImageAllocSize];
    unsigned char* pbyteBitsDest = new unsigned char[iDestImageAllocSize];
    if ((pbyteBitsSrc == NULL) || (pbyteBitsDest == NULL))
    {
        if (pbyteBitsSrc != NULL)
        {
            delete[] pbyteBitsSrc;
            pbyteBitsSrc = NULL;
        }
        if (pbyteBitsDest != NULL)
        {
            delete[] pbyteBitsDest;
            pbyteBitsDest = NULL;
        }
    }
    GetBackgroundBitmap(hDCRef, pbyteBitsSrc);
    int i = 0;
    int j = 0;
    for (i = 0; i < iDestImageHeight; i++)
    {
        const unsigned char* pbyteSrcBase = pbyteBitsSrc + 3 * (iSrcImageWidth - 1 - i);
        unsigned char* pbyteDestBase = pbyteBitsDest + (iDestImageHeight - 1 - i) * iDestImageWidthStep;
        for (j = 0; j < iDestImageWidth; j++)
        {
            const unsigned char* pbyteSrc = pbyteSrcBase + (iSrcImageHeight - 1 - j) * iSrcImageWidthStep;
            unsigned char* pbyteDest = pbyteDestBase + j * 3;
            pbyteDest[0] = pbyteSrc[0];
            pbyteDest[1] = pbyteSrc[1];
            pbyteDest[2] = pbyteSrc[2];
        }
    }
    if (pbyteBitsSrc != NULL)
    {
        delete[] pbyteBitsSrc;
        pbyteBitsSrc = NULL;
    }
    SetBackgroundBitmap(iDestImageWidth, iDestImageHeight, pbyteBitsDest);
    if (pbyteBitsDest != NULL)
    {
        delete[] pbyteBitsDest;
        pbyteBitsDest = NULL;
    }
}

void CImageClipWnd::SetDragAcceptFilesFlag(BOOL bAccept)
{
    m_bDragAcceptFilesFlag = bAccept;
}

const char* CImageClipWnd::GetDragFileName(void ) const
{
	return m_strDragFileName;
}

void CImageClipWnd::OnDropFiles(HDROP hDropInfo)
{
    if (DragQueryFile(hDropInfo, 0xFFFFFFFF, NULL, 0) > 0)
    {
        int iFileNameSize = DragQueryFile(hDropInfo, 0, NULL, 0);
        if (iFileNameSize > 0)
        {
            char* pchFileName = new char[iFileNameSize + 1];
            if (pchFileName != NULL)
            {
                DragQueryFile(hDropInfo, 0, pchFileName, iFileNameSize + 1);
                m_strDragFileName = pchFileName;
                delete[] pchFileName;
                pchFileName = NULL;
                if (GetParent() != NULL)
                {
                    GetParent()->SendMessage(WM_COMMAND, GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                }
            }
        }
    }

    ::DragFinish(hDropInfo);
    hDropInfo = NULL;
}*/

    }
}
