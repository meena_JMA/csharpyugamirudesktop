﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public static class Constant
    {
        public const int BODYPOSITIONTYPEID_STANDING = 1;
        public const int BODYPOSITIONTYPEID_KNEEDOWN = 2;
        public const int BODYPOSITIONTYPEID_COMMON = 3;
    }
}
