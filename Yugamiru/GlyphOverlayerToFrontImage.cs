﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Yugamiru
{
    public class GlyphOverlayerToFrontImage
    {
        int ARROW_OFFSET = 30;
        //int ARROW_LENGTH = 50;
        //int ARROW_WIDTH = 8;
        int ARROW_LENGTH = 5;
        int ARROW_WIDTH = 2;


        bool m_bValidCenterLine;
        int m_uiCenterLineStyle;
        Color m_crCenterLineColor;
        int m_uiCenterLineWidth;
        int m_iCenterLineXPos;
        int m_iCenterLineTop;
        int m_iCenterLineBottom;

        // ŠÖßŠÔƒ‰ƒCƒ“•\Ž¦.
        bool m_bValidJointConnectionLine;
        int m_uiJointConnectionLineStyle;
        Color m_crJointConnectionLineColor;
        int m_uiJointConnectionLineWidth;

        // ŠÖß“_ƒ}[ƒJ[ƒTƒCƒY
        int m_iMarkerSize;
        int m_iArrowWidth;
        int m_iArrowLength;
        int m_iArrowInvisible;

        // ŠÖßˆÊ’u.
        FrontBodyPosition m_FrontBodyPosition;

        // ”»’èŒ‹‰Ê
        FrontBodyResultData m_FrontBodyResultData;

        static void MyDrawLine(Graphics pDC, Point st, Point end, int style /* =PS_SOLID */, Color rgb /*=RGB(0,0,0)*/, int width /*=1*/)
        {
            //	MoveToEx(hDC, st.x, st.y, NULL);
            //	LineTo(hDC, end.x, end.y);

            /*    unsigned type[8];
                int c = CDashLine::GetPattern(type, false, width, style);
                CDashLine line(*pDC, type, c);
                CPen penNew(style, width, rgb );

                CPen* ppenOld = pDC->SelectObject(&penNew);
                line.MoveTo(st.x, st.y);
            line.LineTo(end.x, end.y);

            pDC->SelectObject(ppenOld );*/
        }


        public GlyphOverlayerToFrontImage()
        {
            m_bValidCenterLine = false;

            m_uiCenterLineStyle = 0;

            m_crCenterLineColor = new Color();

            m_uiCenterLineWidth = 0;

            m_iCenterLineXPos = 0;

            m_iCenterLineTop = 0;

            m_iCenterLineBottom = 0;

            m_bValidJointConnectionLine = false;

            m_uiJointConnectionLineStyle = 0;

            m_crJointConnectionLineColor = new Color();

            m_uiJointConnectionLineWidth = 0;

            m_iMarkerSize = 0;

            m_iArrowWidth = 0;

            m_iArrowLength = 0;

            m_iArrowInvisible = 0;

            m_FrontBodyPosition = new FrontBodyPosition();

            m_FrontBodyResultData = new FrontBodyResultData();

        }




        public GlyphOverlayerToFrontImage(GlyphOverlayerToFrontImage rSrc)
        {
            m_bValidCenterLine = rSrc.m_bValidCenterLine;

            m_uiCenterLineStyle = rSrc.m_uiCenterLineStyle;

            m_crCenterLineColor = rSrc.m_crCenterLineColor;

            m_uiCenterLineWidth = rSrc.m_uiCenterLineWidth;

            m_iCenterLineXPos = rSrc.m_iCenterLineXPos;

            m_iCenterLineTop = rSrc.m_iCenterLineTop;

            m_iCenterLineBottom = rSrc.m_iCenterLineBottom;

            m_bValidJointConnectionLine = rSrc.m_bValidJointConnectionLine;

            m_uiJointConnectionLineStyle = rSrc.m_uiJointConnectionLineStyle;

            m_crJointConnectionLineColor = rSrc.m_crJointConnectionLineColor;

            m_uiJointConnectionLineWidth = rSrc.m_uiJointConnectionLineWidth;

            m_iMarkerSize = rSrc.m_iMarkerSize;

            m_iArrowWidth = rSrc.m_iArrowWidth;

            m_iArrowLength = rSrc.m_iArrowLength;

            m_iArrowInvisible = rSrc.m_iArrowInvisible;

            m_FrontBodyPosition = rSrc.m_FrontBodyPosition;

        }




        /*  m_FrontBodyResultData(rSrc.m_FrontBodyResultData )
              {
              }*/

        ~GlyphOverlayerToFrontImage()
        {
        }

        /*GlyphOverlayerToFrontImage::operator=( const CGlyphOverlayerToFrontImage &rSrc )
        {
            m_bValidCenterLine = rSrc.m_bValidCenterLine;
            m_uiCenterLineStyle = rSrc.m_uiCenterLineStyle;
            m_crCenterLineColor = rSrc.m_crCenterLineColor;
            m_uiCenterLineWidth = rSrc.m_uiCenterLineWidth;
            m_iCenterLineXPos = rSrc.m_iCenterLineXPos;
            m_iCenterLineTop = rSrc.m_iCenterLineTop;
            m_iCenterLineBottom = rSrc.m_iCenterLineBottom;
            m_bValidJointConnectionLine = rSrc.m_bValidJointConnectionLine;
            m_uiJointConnectionLineStyle = rSrc.m_uiJointConnectionLineStyle;
            m_crJointConnectionLineColor = rSrc.m_crJointConnectionLineColor;
            m_uiJointConnectionLineWidth = rSrc.m_uiJointConnectionLineWidth;
            m_iMarkerSize = rSrc.m_iMarkerSize;
            m_iArrowWidth = rSrc.m_iArrowWidth;
            m_iArrowLength = rSrc.m_iArrowLength;
            m_iArrowInvisible = rSrc.m_iArrowInvisible;
            m_FrontBodyPosition = rSrc.m_FrontBodyPosition;
            m_FrontBodyResultData = rSrc.m_FrontBodyResultData;
            return *this;
        }*/

        void SetCenterLineData(
            bool bValid, int uiStyle, Color crColor, int uiWidth,
            int iXPos, int iTop, int iBottom)
        {
            m_bValidCenterLine = bValid;
            m_uiCenterLineStyle = uiStyle;
            m_crCenterLineColor = crColor;
            m_uiCenterLineWidth = uiWidth;
            m_iCenterLineXPos = iXPos;
            m_iCenterLineTop = iTop;
            m_iCenterLineBottom = iBottom;
        }

        void SetJointConnectionLineData(bool bValid, int uiStyle, Color crColor, int uiWidth)
        {
            m_bValidJointConnectionLine = bValid;
            m_uiJointConnectionLineStyle = uiStyle;
            m_crJointConnectionLineColor = crColor;
            m_uiJointConnectionLineWidth = uiWidth;
        }

        void SetMarkerSize(int iMarkerSize)
        {
            m_iMarkerSize = iMarkerSize;
        }

        void SetFrontBodyPosition(FrontBodyPosition FrontBodyPosition)
        {
            m_FrontBodyPosition = FrontBodyPosition;
        }

        /* void SetFrontBodyResultData( FrontBodyResultData FrontBodyResultData )
         {
             m_FrontBodyResultData = FrontBodyResultData;
         }*/

        void DrawCenterLine(Graphics pDC)
        {
            if (m_bValidCenterLine)
            {
                Point ptStart = new Point(m_iCenterLineXPos, m_iCenterLineTop);
                Point ptEnd = new Point(m_iCenterLineXPos, m_iCenterLineBottom);
                MyDrawLine(pDC, ptStart, ptEnd,
                    m_uiCenterLineStyle, m_crCenterLineColor, m_uiCenterLineWidth);
            }
        }

        void DrawMarker(Graphics pDC, Point ptCenter, Color col, bool bFill)
        {
            Pen penNew = new Pen(col);
            Brush brNew = Brushes.AliceBlue;

            /*   CPen* ppenOld = pDC->SelectObject(&penNew);
               CBrush* pbrOld = NULL;
               HBRUSH hbrOld = NULL;
               if (bFill)
               {
                   pbrOld = pDC->SelectObject(&brNew);
               }
               else
               {
                   hbrOld = (HBRUSH)(pDC->SelectObject((HBRUSH)GetStockObject(NULL_BRUSH)));
               }*/
            pDC.DrawEllipse(penNew,
                ptCenter.X - m_iMarkerSize,
                ptCenter.Y - m_iMarkerSize,
                ptCenter.X + m_iMarkerSize + 1,
                ptCenter.Y + m_iMarkerSize + 1);
            /*    if (ppenOld != NULL)
                {
                    pDC->SelectObject(ppenOld);
                    ppenOld = NULL;
                }
                if (pbrOld != NULL)
                {
                    pDC->SelectObject(pbrOld);
                    pbrOld = NULL;
                }
                if (hbrOld != NULL)
                {
                    pDC->SelectObject(hbrOld);
                    hbrOld = NULL;
                }*/
        }

        void DrawLineBetweenMarkers(Graphics pDC, Point ptStart, Point ptEnd)
        {
            if (m_bValidJointConnectionLine)
            {
                MyDrawLine(pDC, ptStart, ptEnd,
                    m_uiJointConnectionLineStyle,
                    m_crJointConnectionLineColor,
                    m_uiJointConnectionLineWidth);
            }
        }

        void DrawArrow(Graphics pDC, int iXPos, int iYPos, int iDirAndNorm, int iVertical)
        {
            if (m_iArrowInvisible == 0)
            {
                return;
            }
            if (iDirAndNorm == 0)
            {
                return;
            }
            Point ptStart = new Point(0, 0);
            Point ptEnd = new Point(0, 0);
            int iDir = 0;
            int iNorm = 0;
            int iLength = 0;

            int iArrowLength = m_iArrowLength;
            if (iArrowLength == 0)
            {
                iArrowLength = ARROW_LENGTH;
                //iArrowLength = 5;
            }
            int iArrowWidth = m_iArrowWidth;
            if (iArrowWidth == 0)
            {
                 iArrowWidth = ARROW_WIDTH;
                //iArrowWidth = 2;
            }

            if (iVertical > 0)
            {
                // ‚’¼ƒxƒNƒgƒ‹‚Ìê‡
                if (iDirAndNorm > 0)
                {
                    iDir = 1;
                    iNorm = iDirAndNorm;
                    iLength = iArrowLength * iNorm;
                    ptStart.X = iXPos;
                    ptStart.Y = iYPos - iLength / 2;
                    ptEnd.X = iXPos;
                    ptEnd.Y = iYPos + iLength / 2;
                }
                else
                {
                    
                    iDir = -1;
                    iNorm = -iDirAndNorm;
                    iLength = iArrowLength * iNorm;
                    ptStart.X = iXPos;
                    ptStart.Y = iYPos + iLength / 2;
                    ptEnd.X = iXPos;
                    ptEnd.Y = iYPos - iLength / 2;
                }
            }
            else
            {
                //iArrowLength = 20;
                // …•½ƒxƒNƒgƒ‹‚Ìê‡
                if (iDirAndNorm > 0)
                {
                    iDir = 1;
                    iNorm = iDirAndNorm;
                    iLength = iArrowLength * iNorm;
                    ptStart.X = iXPos - iLength / 2;
                    ptStart.Y = iYPos;
                    ptEnd.X = iXPos + iLength / 2;
                    ptEnd.Y = iYPos;
                }
                else
                {
                    iDir = -1;
                    iNorm = -iDirAndNorm;
                    iLength = iArrowLength * iNorm;
                    ptStart.X = iXPos + iLength / 2;
                    ptStart.Y = iYPos;
                    ptEnd.X = iXPos - iLength / 2;
                    ptEnd.Y = iYPos;
                }
            }

            Color colArrow = Color.Black;
            switch (iNorm)
            {
                case 1:
                    colArrow = Color.Yellow;
                    break;
                case 2:
                case 3:
                    colArrow = Color.Red;
                    break;
            }

            Pen penNew = new Pen(colArrow, iArrowWidth);
            //Pen* ppenOld = pDC->SelectObject(&penNew);

            //Ž²
            pDC.DrawLine(penNew, ptStart.X, ptStart.Y, ptEnd.X, ptEnd.Y);
            // pDC->MoveTo(ptStart.x, ptStart.y);
            // pDC->LineTo(ptEnd.x, ptEnd.y);

            //ž¶
            double dArrowHeadLength = (double)iLength / 3;
            double theta = Math.Atan2((double)(ptStart.Y - ptEnd.Y), (double)(ptStart.X - ptEnd.X));
            double dt = 3.14 / 6.0;
            double dClockwiseTheta = theta - dt;
            double dAnticlockwiseTheta = theta + dt;
            // I“_‚©‚çŽn“_‚ÉŒü‚©‚¤ƒxƒNƒgƒ‹‚ðAI“_‚ð’†S‚ÉŽžŒv‰ñ‚è‚É‚R‚O“x‰ñ“]‚·‚éB
            // ‚³‚ç‚É‚»‚ÌƒxƒNƒgƒ‹‚ð•ûŒü‚ð•Û‚Á‚½‚Ü‚ÜAŒ³‚ÌƒxƒNƒgƒ‹‚Ì‚R•ª‚Ì‚P‚É‚·‚éB
            // ‚±‚¤‚µ‚Ä‹‚ß‚½ƒxƒNƒgƒ‹‚ÆAI“_‚ðŒ‹‚Ô.
            Point ptClockwise = new Point(0, 0);
            ptClockwise.X = ptEnd.X + (int)(dArrowHeadLength * Math.Cos(dClockwiseTheta));
            ptClockwise.Y = ptEnd.Y + (int)(dArrowHeadLength * Math.Sin(dClockwiseTheta));
            pDC.DrawLine(penNew, ptEnd.X, ptEnd.Y, ptClockwise.X, ptClockwise.Y);
            //pDC->LineTo(ptClockwise.x, ptClockwise.y);
            // I“_‚©‚çŽn“_‚ÉŒü‚©‚¤ƒxƒNƒgƒ‹‚ðAI“_‚ð’†S‚É”½ŽžŒv‰ñ‚è‚É‚R‚O“x‰ñ“]‚·‚éB
            // ‚³‚ç‚É‚»‚ÌƒxƒNƒgƒ‹‚ð•ûŒü‚ð•Û‚Á‚½‚Ü‚ÜAŒ³‚ÌƒxƒNƒgƒ‹‚Ì‚R•ª‚Ì‚P‚É‚·‚éB
            // ‚±‚¤‚µ‚Ä‹‚ß‚½ƒxƒNƒgƒ‹‚ÆAI“_‚ðŒ‹‚Ô.
            Point ptAnticlockwise = new Point(0, 0);
            ptAnticlockwise.X = ptEnd.X + (int)(dArrowHeadLength * Math.Cos(dAnticlockwiseTheta));
            ptAnticlockwise.Y = ptEnd.Y + (int)(dArrowHeadLength * Math.Sin(dAnticlockwiseTheta));
            pDC.DrawLine(penNew, ptEnd.X, ptEnd.Y, ptAnticlockwise.X, ptAnticlockwise.Y);

            //  pDC->MoveTo(ptEnd.x, ptEnd.y);
            //  pDC->LineTo(ptAnticlockwise.x, ptAnticlockwise.y);

            // pDC->SelectObject(ppenOld);
            // ppenOld = NULL;

        }

        public void DrawVerticalArrow(Graphics pDC, int iXPos, int iYPos, int iDirAndNorm)
        {
            DrawArrow(pDC, iXPos, iYPos, iDirAndNorm, 1);
        }

        public void DrawHorizontalArrow(Graphics pDC, int iXPos, int iYPos, int iDirAndNorm)
        {
            DrawArrow(pDC, iXPos, iYPos, iDirAndNorm, 0);
        }

        public void DrawHipArrow(Graphics pDC, int iXPos, int iYPos, int iDirAndNorm)
        {
            if (m_iArrowInvisible == 0)
            {
                return;
            }
            if (iDirAndNorm == 0)
            {
                return;
            }
            Point ptStart = new Point();
            Point ptEnd = new Point();
            int iDir = 0;
            int iNorm = 0;
            int iLength = 0;

            int iArrowLength = m_iArrowLength;
            if (iArrowLength == 0)
            {
                iArrowLength = ARROW_LENGTH;
                //iArrowLength = 20;
            }
            int iArrowWidth = m_iArrowWidth;
            if (iArrowWidth == 0)
            {
                iArrowWidth = ARROW_WIDTH;
                //iArrowWidth = 2;
            }

            if (iDirAndNorm > 0)
            {
                iDir = 1;
                iNorm = iDirAndNorm;
                iLength = iArrowLength * iNorm;
                ptStart.X = iXPos;
                ptStart.Y = iYPos;
                ptEnd.X = iXPos + iLength;
                ptEnd.Y = iYPos;
            }
            else
            {
                iDir = -1;
                iNorm = -iDirAndNorm;
                iLength = iArrowLength * iNorm;
                ptStart.X = iXPos;
                ptStart.Y = iYPos;
                ptEnd.X = iXPos - iLength;
                ptEnd.Y = iYPos;
            }

            // F‚Íí‚ÉÔ.
            Color colArrow = Color.Red;
            Pen penNew = new Pen(colArrow, iArrowWidth);
            //CPen* ppenOld = pDC->SelectObject(&penNew);

            //Ž²
            pDC.DrawLine(penNew, ptStart.X, ptStart.Y, ptEnd.X, ptEnd.Y);
            //pDC->MoveTo(ptStart.x, ptStart.y);
            //pDC->LineTo(ptEnd.x, ptEnd.y);

            //ž¶
            double dArrowHeadLength = (double)iLength / 3;
            double theta = Math.Atan2((double)(ptStart.Y - ptEnd.Y), (double)(ptStart.X - ptEnd.X));
            double dt = 3.14 / 6.0;
            double dClockwiseTheta = theta - dt;
            double dAnticlockwiseTheta = theta + dt;
            // I“_‚©‚çŽn“_‚ÉŒü‚©‚¤ƒxƒNƒgƒ‹‚ðAI“_‚ð’†S‚ÉŽžŒv‰ñ‚è‚É‚R‚O“x‰ñ“]‚·‚éB
            // ‚³‚ç‚É‚»‚ÌƒxƒNƒgƒ‹‚ð•ûŒü‚ð•Û‚Á‚½‚Ü‚ÜAŒ³‚ÌƒxƒNƒgƒ‹‚Ì‚R•ª‚Ì‚P‚É‚·‚éB
            // ‚±‚¤‚µ‚Ä‹‚ß‚½ƒxƒNƒgƒ‹‚ÆAI“_‚ðŒ‹‚Ô.
            Point ptClockwise = new Point();
            ptClockwise.X = ptEnd.X + (int)(dArrowHeadLength * Math.Cos(dClockwiseTheta));
            ptClockwise.Y = ptEnd.Y + (int)(dArrowHeadLength * Math.Sin(dClockwiseTheta));
            pDC.DrawLine(penNew, ptEnd.X, ptEnd.Y, ptClockwise.X, ptClockwise.Y);
            //pDC->MoveTo(ptEnd.x, ptEnd.y);
            //pDC->LineTo(ptClockwise.x, ptClockwise.y);
            // I“_‚©‚çŽn“_‚ÉŒü‚©‚¤ƒxƒNƒgƒ‹‚ðAI“_‚ð’†S‚É”½ŽžŒv‰ñ‚è‚É‚R‚O“x‰ñ“]‚·‚éB
            // ‚³‚ç‚É‚»‚ÌƒxƒNƒgƒ‹‚ð•ûŒü‚ð•Û‚Á‚½‚Ü‚ÜAŒ³‚ÌƒxƒNƒgƒ‹‚Ì‚R•ª‚Ì‚P‚É‚·‚éB
            // ‚±‚¤‚µ‚Ä‹‚ß‚½ƒxƒNƒgƒ‹‚ÆAI“_‚ðŒ‹‚Ô.
            Point ptAnticlockwise = new Point();
            ptAnticlockwise.X = ptEnd.X + (int)(dArrowHeadLength * Math.Cos(dAnticlockwiseTheta));
            ptAnticlockwise.Y = ptEnd.Y + (int)(dArrowHeadLength * Math.Sin(dAnticlockwiseTheta));
            pDC.DrawLine(penNew, ptEnd.X, ptEnd.Y, ptAnticlockwise.X, ptAnticlockwise.Y);
            //pDC->MoveTo(ptEnd.x, ptEnd.y);
            //pDC->LineTo(ptAnticlockwise.x, ptAnticlockwise.y);

            //pDC->SelectObject(ppenOld);
            //ppenOld = NULL;
        }

        public void Draw(Graphics pDC, FrontBodyPosition m_FrontBodyPosition,FrontBodyAngle m_FrontBodyAngle,
            double ratioX, double ratioY)
        {
            int iArrowLength = m_iArrowLength;
            if (iArrowLength == 0)
            {
                iArrowLength = ARROW_LENGTH;
            }

            // ’†Sü•`‰æ.
            DrawCenterLine(pDC);

            // ŠÖßÚ‘±ü•`‰æ.
            Point ptRightHip = new Point((int)(m_FrontBodyPosition.m_ptRightHip.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptRightHip.Y * ratioY));
            Point ptLeftHip = new Point((int)(m_FrontBodyPosition.m_ptLeftHip.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptLeftHip.Y * ratioY));
            Point ptRightKnee = new Point((int)(m_FrontBodyPosition.m_ptRightKnee.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptRightKnee.Y * ratioY));
            Point ptLeftKnee = new Point((int)(m_FrontBodyPosition.m_ptLeftKnee.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptLeftKnee.Y * ratioY));
            Point ptRightAnkle = new Point((int)(m_FrontBodyPosition.m_ptRightAnkle.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptRightAnkle.Y * ratioY));
            Point ptLeftAnkle = new Point((int)(m_FrontBodyPosition.m_ptLeftAnkle.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptLeftAnkle.Y * ratioY));
            Point ptRightShoulder = new Point((int)(m_FrontBodyPosition.m_ptRightShoulder.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptRightShoulder.Y * ratioY));
            Point ptLeftShoulder = new Point((int)(m_FrontBodyPosition.m_ptLeftShoulder.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptLeftShoulder.Y * ratioY));
            Point ptRightEar = new Point((int)(m_FrontBodyPosition.m_ptRightEar.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptRightEar.Y * ratioY));
            Point ptLeftEar = new Point((int)(m_FrontBodyPosition.m_ptLeftEar.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptLeftEar.Y * ratioY));
            Point ptChin = new Point((int)(m_FrontBodyPosition.m_ptChin.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptChin.Y * ratioY));
            Point ptGlabella = new Point((int)(m_FrontBodyPosition.m_ptGlabella.X * ratioX),
                (int)(m_FrontBodyPosition.m_ptGlabella.Y * ratioY));


            bool bUnderBodyPositionDetected = m_FrontBodyPosition.IsUnderBodyPositionDetected();
            bool bKneePositionDetected = m_FrontBodyPosition.IsKneePositionDetected();
            bool bUpperBodyPositionDetected = m_FrontBodyPosition.IsUpperBodyPositionDetected();

            m_FrontBodyPosition.GetRightHipPosition(ptRightHip);
            m_FrontBodyPosition.GetLeftHipPosition(ptLeftHip);
            m_FrontBodyPosition.GetRightKneePosition(ptRightKnee);
            m_FrontBodyPosition.GetLeftKneePosition(ptLeftKnee);
            m_FrontBodyPosition.GetRightAnklePosition(ptRightAnkle);
            m_FrontBodyPosition.GetLeftAnklePosition(ptLeftAnkle);
            m_FrontBodyPosition.GetRightShoulderPosition(ptRightShoulder);
            m_FrontBodyPosition.GetLeftShoulderPosition(ptLeftShoulder);
            m_FrontBodyPosition.GetRightEarPosition(ptRightEar);
            m_FrontBodyPosition.GetLeftEarPosition(ptLeftEar);
            m_FrontBodyPosition.GetChinPosition(ptChin);
            m_FrontBodyPosition.GetGlabellaPosition(ptGlabella);

            /* Commented By Meena
            DrawLineBetweenMarkers(pDC, ptRightHip, ptLeftHip);
            DrawLineBetweenMarkers(pDC, ptRightShoulder, ptLeftShoulder);
            DrawLineBetweenMarkers(pDC, ptRightEar, ptLeftEar);
            DrawLineBetweenMarkers(pDC, ptChin, ptGlabella);
            DrawLineBetweenMarkers(pDC, ptRightShoulder, ptRightHip);
            DrawLineBetweenMarkers(pDC, ptRightHip, ptRightKnee);
            DrawLineBetweenMarkers(pDC, ptRightKnee, ptRightAnkle);
            DrawLineBetweenMarkers(pDC, ptLeftShoulder, ptLeftHip);
            DrawLineBetweenMarkers(pDC, ptLeftHip, ptLeftKnee);
            DrawLineBetweenMarkers(pDC, ptLeftKnee, ptLeftAnkle);

            // ŠÖßƒ}[ƒJ[•`‰æ.
            DrawMarker(pDC, ptRightHip, Color.Red, bUnderBodyPositionDetected);
            DrawMarker(pDC, ptLeftHip, Color.Red, bUnderBodyPositionDetected);
            DrawMarker(pDC, ptRightKnee, Color.Lime, bKneePositionDetected);
            DrawMarker(pDC, ptLeftKnee, Color.Lime, bKneePositionDetected);
            DrawMarker(pDC, ptRightAnkle, Color.Blue, bUnderBodyPositionDetected);
            DrawMarker(pDC, ptLeftAnkle, Color.Blue, bUnderBodyPositionDetected);
            //DrawMarker(pDC, ptRightShoulder, RGB(200, 200, 0), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptRightShoulder, Color.GreenYellow, bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptLeftShoulder, RGB(200, 200, 0), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptLeftShoulder, Color.GreenYellow, bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptRightEar, RGB(126, 0, 126), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptRightEar, Color.Magenta, bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptLeftEar, RGB(126, 0, 126), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptLeftEar, Color.Magenta, bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptChin, RGB(126, 126, 0), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptChin, Color.YellowGreen, bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptGlabella, RGB(126, 126, 0), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptGlabella, Color.YellowGreen, bUpperBodyPositionDetected);
            */
            // ¶‰E‘ÎÌˆÊ’u‚É‚ ‚éŠÖßƒ}[ƒJ[‚Ì’†“_‚ð‹‚ß‚é.
            Point ptCenterOfHip = new Point();
            Point ptCenterOfShoulder = new Point();
            Point ptCenterOfEar = new Point();
            ptCenterOfHip.X = (ptRightHip.X + ptLeftHip.X) / 2;
            ptCenterOfHip.Y = (ptRightHip.Y + ptLeftHip.Y) / 2;
            ptCenterOfShoulder.X = (ptRightShoulder.X + ptLeftShoulder.X) / 2;
            ptCenterOfShoulder.Y = (ptRightShoulder.Y + ptLeftShoulder.Y) / 2;
            ptCenterOfEar.X = (ptRightEar.X + ptLeftEar.X) / 2;
            ptCenterOfEar.Y = (ptRightEar.Y + ptLeftEar.Y) / 2;

            // –îˆó•`‰æ.
            DrawHipArrow(pDC, ptCenterOfHip.X, ptCenterOfHip.Y - ARROW_OFFSET,
                m_FrontBodyResultData.GetCenterBalance());
                //(int)m_FrontBodyAngle.GetCenter());
            DrawVerticalArrow(pDC, ptRightHip.X - ARROW_OFFSET, ptCenterOfHip.Y,
                +m_FrontBodyResultData.GetHip());
                //+(int)m_FrontBodyAngle.GetHip());
            DrawVerticalArrow(pDC, ptLeftHip.X + ARROW_OFFSET, ptCenterOfHip.Y,
                -m_FrontBodyResultData.GetHip());
                //-(int)m_FrontBodyAngle.GetHip());
            int iRightKneeOffset = iArrowLength * Math.Abs(m_FrontBodyResultData.GetRightKnee()) / 2;
            DrawHorizontalArrow(pDC, ptRightKnee.X - (iRightKneeOffset + ARROW_OFFSET),
                ptRightKnee.Y,
                -m_FrontBodyResultData.GetRightKnee());
                //-(int)m_FrontBodyAngle.m_dRightThigh);
            int iLeftKneeOffset = iArrowLength * Math.Abs(m_FrontBodyResultData.GetLeftKnee()) / 2;
            DrawHorizontalArrow(pDC, ptLeftKnee.X + (iLeftKneeOffset + ARROW_OFFSET),
                ptLeftKnee.Y,
                +m_FrontBodyResultData.GetLeftKnee());
                //+(int)m_FrontBodyAngle.m_dLeftThigh);
            DrawHorizontalArrow(pDC, ptCenterOfShoulder.X, ptCenterOfShoulder.Y + ARROW_OFFSET,
                m_FrontBodyResultData.GetBodyCenter());
                //(int)m_FrontBodyAngle.m_dBody2Center);
            DrawVerticalArrow(pDC, ptRightShoulder.X - ARROW_OFFSET, ptCenterOfShoulder.Y,
                +m_FrontBodyResultData.GetShoulderBal());
                //+(int)m_FrontBodyAngle.GetShoulder());

            DrawVerticalArrow(pDC, ptLeftShoulder.X + ARROW_OFFSET, ptCenterOfShoulder.Y,
                -m_FrontBodyResultData.GetShoulderBal());
                //-(int)m_FrontBodyAngle.GetShoulder());
            DrawHorizontalArrow(pDC, ptCenterOfEar.X, ptCenterOfEar.Y + ARROW_OFFSET,
                m_FrontBodyResultData.GetHeadCenter());
                //(int)m_FrontBodyAngle.GetHead());
            DrawVerticalArrow(pDC, ptRightEar.X - ARROW_OFFSET, ptCenterOfEar.Y,
                +m_FrontBodyResultData.GetEarBal());
                //+(int)m_FrontBodyAngle.GetEar());
            DrawVerticalArrow(pDC, ptLeftEar.X + ARROW_OFFSET, ptCenterOfEar.Y,
                -m_FrontBodyResultData.GetEarBal());
                //-(int)m_FrontBodyAngle.GetEar());
        }

        public int GetArrowWidth()
        {
            return m_iArrowWidth;
        }

        public int GetArrowLength()
        {
            return m_iArrowLength;
        }

        public int IsArrowInvisible()
        {
            return m_iArrowInvisible;
        }

        public void SetArrowWidth(int iArrowWidth)
        {
            m_iArrowWidth = iArrowWidth;
        }

        public void SetArrowLength(int iArrowLength)
        {
            m_iArrowLength = iArrowLength;
        }

        public void SetArrowInvisible(int iArrowInvisible)
        {
            m_iArrowInvisible = iArrowInvisible;
        }

    }
}
