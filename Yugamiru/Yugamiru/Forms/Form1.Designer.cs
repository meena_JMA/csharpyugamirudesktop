﻿namespace Yugamiru
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.mainpage = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.startinitial = new System.Windows.Forms.PictureBox();
            this.openresultbutton = new System.Windows.Forms.PictureBox();
            this.quitapp = new System.Windows.Forms.PictureBox();
            this.settingsbutton = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Year = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.Day = new System.Windows.Forms.ComboBox();
            this.Month = new System.Windows.Forms.ComboBox();
            this.Gender = new System.Windows.Forms.ComboBox();
            this.user_Name = new System.Windows.Forms.TextBox();
            this.user_ID = new System.Windows.Forms.TextBox();
            this.nextbutton = new System.Windows.Forms.PictureBox();
            this.returnbutton = new System.Windows.Forms.PictureBox();
            this.startinitialscreen = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.usernamelabel = new System.Windows.Forms.Label();
            this.useridlabel = new System.Windows.Forms.Label();
            this.Speechimagetext = new System.Windows.Forms.PictureBox();
            this.enteruprightimage = new System.Windows.Forms.PictureBox();
            this.nextimage = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.returnimage = new System.Windows.Forms.PictureBox();
            this.openimage = new System.Windows.Forms.PictureBox();
            this.rotateimage = new System.Windows.Forms.PictureBox();
            this.Loadimage = new System.Windows.Forms.PictureBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.labelforiconsscreen = new System.Windows.Forms.Label();
            this.originalsizebeltankles = new System.Windows.Forms.PictureBox();
            this.minimumbeltankles = new System.Windows.Forms.PictureBox();
            this.maximumbeltankles = new System.Windows.Forms.PictureBox();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.nextdragiconscreen = new System.Windows.Forms.PictureBox();
            this.returndragiconscreen = new System.Windows.Forms.PictureBox();
            this.beltanklescreen = new System.Windows.Forms.PictureBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.labelforsideicon = new System.Windows.Forms.Label();
            this.originalsizesideview = new System.Windows.Forms.PictureBox();
            this.minimizesideview = new System.Windows.Forms.PictureBox();
            this.maximizesideview = new System.Windows.Forms.PictureBox();
            this.vScrollBarsideview = new System.Windows.Forms.VScrollBar();
            this.nextsideicon = new System.Windows.Forms.PictureBox();
            this.returnsideicon = new System.Windows.Forms.PictureBox();
            this.sidescreen = new System.Windows.Forms.PictureBox();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.originalsizefinalscreen = new System.Windows.Forms.PictureBox();
            this.minimizefinalscreen = new System.Windows.Forms.PictureBox();
            this.maximizefinalscreen = new System.Windows.Forms.PictureBox();
            this.vScrollBarfinalscreen = new System.Windows.Forms.VScrollBar();
            this.initialscreenbutton = new System.Windows.Forms.PictureBox();
            this.restartbutton = new System.Windows.Forms.PictureBox();
            this.saveresultbutton = new System.Windows.Forms.PictureBox();
            this.printreportbutton = new System.Windows.Forms.PictureBox();
            this.showreportbutton = new System.Windows.Forms.PictureBox();
            this.checkpositionbutton = new System.Windows.Forms.PictureBox();
            this.changebutton = new System.Windows.Forms.PictureBox();
            this.editid = new System.Windows.Forms.PictureBox();
            this.finalscreen = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.quitbutton = new System.Windows.Forms.PictureBox();
            this.openresult = new System.Windows.Forms.PictureBox();
            this.startbutton = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainpage)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startinitial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openresultbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quitapp)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Year)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.returnbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startinitialscreen)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Speechimagetext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enteruprightimage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextimage)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.returnimage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openimage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotateimage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Loadimage)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.originalsizebeltankles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimumbeltankles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximumbeltankles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextdragiconscreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.returndragiconscreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beltanklescreen)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.originalsizesideview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizesideview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximizesideview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextsideicon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.returnsideicon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sidescreen)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.originalsizefinalscreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizefinalscreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximizefinalscreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialscreenbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.restartbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveresultbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printreportbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showreportbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkpositionbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changebutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalscreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quitbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openresult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startbutton)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1083, 705);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.mainpage);
            this.tabPage1.Controls.Add(this.flowLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1075, 679);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // mainpage
            // 
            this.mainpage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mainpage.Location = new System.Drawing.Point(3, 0);
            this.mainpage.Name = "mainpage";
            this.mainpage.Size = new System.Drawing.Size(1066, 540);
            this.mainpage.TabIndex = 2;
            this.mainpage.TabStop = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel1.Controls.Add(this.startinitial);
            this.flowLayoutPanel1.Controls.Add(this.openresultbutton);
            this.flowLayoutPanel1.Controls.Add(this.quitapp);
            this.flowLayoutPanel1.Controls.Add(this.settingsbutton);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(257, 586);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(507, 56);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // startinitial
            // 
            this.startinitial.Location = new System.Drawing.Point(3, 3);
            this.startinitial.Name = "startinitial";
            this.startinitial.Size = new System.Drawing.Size(113, 50);
            this.startinitial.TabIndex = 1;
            this.startinitial.TabStop = false;
            this.startinitial.Click += new System.EventHandler(this.startinitial_Click);
            // 
            // openresultbutton
            // 
            this.openresultbutton.Location = new System.Drawing.Point(122, 3);
            this.openresultbutton.Name = "openresultbutton";
            this.openresultbutton.Size = new System.Drawing.Size(115, 50);
            this.openresultbutton.TabIndex = 2;
            this.openresultbutton.TabStop = false;
            // 
            // quitapp
            // 
            this.quitapp.Location = new System.Drawing.Point(243, 3);
            this.quitapp.Name = "quitapp";
            this.quitapp.Size = new System.Drawing.Size(118, 50);
            this.quitapp.TabIndex = 3;
            this.quitapp.TabStop = false;
            this.quitapp.Click += new System.EventHandler(this.quitapp_Click);
            // 
            // settingsbutton
            // 
            this.settingsbutton.BackColor = System.Drawing.Color.Silver;
            this.settingsbutton.Location = new System.Drawing.Point(367, 3);
            this.settingsbutton.Name = "settingsbutton";
            this.settingsbutton.Size = new System.Drawing.Size(129, 47);
            this.settingsbutton.TabIndex = 4;
            this.settingsbutton.Text = "Settings";
            this.settingsbutton.UseVisualStyleBackColor = false;
            this.settingsbutton.Click += new System.EventHandler(this.settingsbutton_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.Year);
            this.tabPage2.Controls.Add(this.numericUpDown1);
            this.tabPage2.Controls.Add(this.Day);
            this.tabPage2.Controls.Add(this.Month);
            this.tabPage2.Controls.Add(this.Gender);
            this.tabPage2.Controls.Add(this.user_Name);
            this.tabPage2.Controls.Add(this.user_ID);
            this.tabPage2.Controls.Add(this.nextbutton);
            this.tabPage2.Controls.Add(this.returnbutton);
            this.tabPage2.Controls.Add(this.startinitialscreen);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1075, 679);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Year
            // 
            this.Year.Location = new System.Drawing.Point(454, 322);
            this.Year.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.Year.Name = "Year";
            this.Year.Size = new System.Drawing.Size(96, 20);
            this.Year.TabIndex = 13;
            this.Year.Value = new decimal(new int[] {
            1980,
            0,
            0,
            0});
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(455, 386);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown1.TabIndex = 12;
            // 
            // Day
            // 
            this.Day.FormattingEnabled = true;
            this.Day.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.Day.Location = new System.Drawing.Point(729, 321);
            this.Day.Name = "Day";
            this.Day.Size = new System.Drawing.Size(44, 21);
            this.Day.TabIndex = 11;
            // 
            // Month
            // 
            this.Month.FormattingEnabled = true;
            this.Month.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.Month.Location = new System.Drawing.Point(591, 321);
            this.Month.Name = "Month";
            this.Month.Size = new System.Drawing.Size(65, 21);
            this.Month.TabIndex = 10;
            // 
            // Gender
            // 
            this.Gender.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Gender.FormattingEnabled = true;
            this.Gender.Items.AddRange(new object[] {
            "-",
            "MALE",
            "FEMALE"});
            this.Gender.Location = new System.Drawing.Point(454, 245);
            this.Gender.Name = "Gender";
            this.Gender.Size = new System.Drawing.Size(121, 26);
            this.Gender.TabIndex = 8;
            // 
            // user_Name
            // 
            this.user_Name.Location = new System.Drawing.Point(454, 180);
            this.user_Name.Multiline = true;
            this.user_Name.Name = "user_Name";
            this.user_Name.Size = new System.Drawing.Size(371, 32);
            this.user_Name.TabIndex = 7;
            // 
            // user_ID
            // 
            this.user_ID.Location = new System.Drawing.Point(454, 117);
            this.user_ID.Multiline = true;
            this.user_ID.Name = "user_ID";
            this.user_ID.Size = new System.Drawing.Size(371, 32);
            this.user_ID.TabIndex = 6;
            // 
            // nextbutton
            // 
            this.nextbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nextbutton.Location = new System.Drawing.Point(530, 595);
            this.nextbutton.Name = "nextbutton";
            this.nextbutton.Size = new System.Drawing.Size(113, 42);
            this.nextbutton.TabIndex = 5;
            this.nextbutton.TabStop = false;
            this.nextbutton.Click += new System.EventHandler(this.nextbutton_Click);
            // 
            // returnbutton
            // 
            this.returnbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.returnbutton.ErrorImage = global::Yugamiru.Properties.Resources.gobackgreen_on;
            this.returnbutton.InitialImage = global::Yugamiru.Properties.Resources.gobackgreen_on;
            this.returnbutton.Location = new System.Drawing.Point(243, 594);
            this.returnbutton.Name = "returnbutton";
            this.returnbutton.Size = new System.Drawing.Size(113, 43);
            this.returnbutton.TabIndex = 4;
            this.returnbutton.TabStop = false;
            this.returnbutton.Click += new System.EventHandler(this.returnbutton_Click);
            // 
            // startinitialscreen
            // 
            this.startinitialscreen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.startinitialscreen.Location = new System.Drawing.Point(61, 3);
            this.startinitialscreen.Name = "startinitialscreen";
            this.startinitialscreen.Size = new System.Drawing.Size(972, 589);
            this.startinitialscreen.TabIndex = 0;
            this.startinitialscreen.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.comboBox2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1075, 679);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(324, 471);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "OK";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(246, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Language Selection";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "English",
            "Japanese",
            "Tradional Chinese",
            "Korean"});
            this.comboBox2.Location = new System.Drawing.Point(395, 129);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(178, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SETTINGS";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.usernamelabel);
            this.tabPage5.Controls.Add(this.useridlabel);
            this.tabPage5.Controls.Add(this.Speechimagetext);
            this.tabPage5.Controls.Add(this.enteruprightimage);
            this.tabPage5.Controls.Add(this.nextimage);
            this.tabPage5.Controls.Add(this.flowLayoutPanel3);
            this.tabPage5.Controls.Add(this.Loadimage);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1075, 679);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // usernamelabel
            // 
            this.usernamelabel.AutoSize = true;
            this.usernamelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernamelabel.Location = new System.Drawing.Point(308, 46);
            this.usernamelabel.Name = "usernamelabel";
            this.usernamelabel.Size = new System.Drawing.Size(46, 18);
            this.usernamelabel.TabIndex = 6;
            this.usernamelabel.Text = "label3";
            // 
            // useridlabel
            // 
            this.useridlabel.AutoSize = true;
            this.useridlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.useridlabel.Location = new System.Drawing.Point(194, 46);
            this.useridlabel.Name = "useridlabel";
            this.useridlabel.Size = new System.Drawing.Size(46, 18);
            this.useridlabel.TabIndex = 5;
            this.useridlabel.Text = "label3";
            // 
            // Speechimagetext
            // 
            this.Speechimagetext.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Speechimagetext.Location = new System.Drawing.Point(728, 46);
            this.Speechimagetext.Name = "Speechimagetext";
            this.Speechimagetext.Size = new System.Drawing.Size(126, 40);
            this.Speechimagetext.TabIndex = 4;
            this.Speechimagetext.TabStop = false;
            this.Speechimagetext.Visible = false;
            // 
            // enteruprightimage
            // 
            this.enteruprightimage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enteruprightimage.Location = new System.Drawing.Point(431, 266);
            this.enteruprightimage.Name = "enteruprightimage";
            this.enteruprightimage.Size = new System.Drawing.Size(113, 50);
            this.enteruprightimage.TabIndex = 3;
            this.enteruprightimage.TabStop = false;
            this.enteruprightimage.Click += new System.EventHandler(this.enterfromupright_click);
            // 
            // nextimage
            // 
            this.nextimage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nextimage.Location = new System.Drawing.Point(739, 622);
            this.nextimage.Name = "nextimage";
            this.nextimage.Size = new System.Drawing.Size(138, 50);
            this.nextimage.TabIndex = 2;
            this.nextimage.TabStop = false;
            this.nextimage.Visible = false;
            this.nextimage.Click += new System.EventHandler(this.nextimage_click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel3.Controls.Add(this.returnimage);
            this.flowLayoutPanel3.Controls.Add(this.openimage);
            this.flowLayoutPanel3.Controls.Add(this.rotateimage);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(8, 617);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(432, 59);
            this.flowLayoutPanel3.TabIndex = 1;
            // 
            // returnimage
            // 
            this.returnimage.Location = new System.Drawing.Point(3, 3);
            this.returnimage.Name = "returnimage";
            this.returnimage.Size = new System.Drawing.Size(134, 50);
            this.returnimage.TabIndex = 0;
            this.returnimage.TabStop = false;
            this.returnimage.Click += new System.EventHandler(this.return_click);
            // 
            // openimage
            // 
            this.openimage.Location = new System.Drawing.Point(143, 3);
            this.openimage.Name = "openimage";
            this.openimage.Size = new System.Drawing.Size(132, 50);
            this.openimage.TabIndex = 1;
            this.openimage.TabStop = false;
            // 
            // rotateimage
            // 
            this.rotateimage.Location = new System.Drawing.Point(281, 3);
            this.rotateimage.Name = "rotateimage";
            this.rotateimage.Size = new System.Drawing.Size(145, 50);
            this.rotateimage.TabIndex = 2;
            this.rotateimage.TabStop = false;
            // 
            // Loadimage
            // 
            this.Loadimage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Loadimage.Location = new System.Drawing.Point(3, 12);
            this.Loadimage.Name = "Loadimage";
            this.Loadimage.Size = new System.Drawing.Size(1066, 602);
            this.Loadimage.TabIndex = 0;
            this.Loadimage.TabStop = false;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.labelforiconsscreen);
            this.tabPage7.Controls.Add(this.originalsizebeltankles);
            this.tabPage7.Controls.Add(this.minimumbeltankles);
            this.tabPage7.Controls.Add(this.maximumbeltankles);
            this.tabPage7.Controls.Add(this.vScrollBar1);
            this.tabPage7.Controls.Add(this.nextdragiconscreen);
            this.tabPage7.Controls.Add(this.returndragiconscreen);
            this.tabPage7.Controls.Add(this.beltanklescreen);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1075, 679);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "tabPage7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // labelforiconsscreen
            // 
            this.labelforiconsscreen.AutoSize = true;
            this.labelforiconsscreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelforiconsscreen.Location = new System.Drawing.Point(186, 47);
            this.labelforiconsscreen.Name = "labelforiconsscreen";
            this.labelforiconsscreen.Size = new System.Drawing.Size(60, 24);
            this.labelforiconsscreen.TabIndex = 7;
            this.labelforiconsscreen.Text = "label3";
            // 
            // originalsizebeltankles
            // 
            this.originalsizebeltankles.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.originalsizebeltankles.Location = new System.Drawing.Point(998, 518);
            this.originalsizebeltankles.Name = "originalsizebeltankles";
            this.originalsizebeltankles.Size = new System.Drawing.Size(51, 48);
            this.originalsizebeltankles.TabIndex = 6;
            this.originalsizebeltankles.TabStop = false;
            // 
            // minimumbeltankles
            // 
            this.minimumbeltankles.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.minimumbeltankles.Location = new System.Drawing.Point(998, 464);
            this.minimumbeltankles.Name = "minimumbeltankles";
            this.minimumbeltankles.Size = new System.Drawing.Size(51, 48);
            this.minimumbeltankles.TabIndex = 5;
            this.minimumbeltankles.TabStop = false;
            // 
            // maximumbeltankles
            // 
            this.maximumbeltankles.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.maximumbeltankles.Location = new System.Drawing.Point(998, 122);
            this.maximumbeltankles.Name = "maximumbeltankles";
            this.maximumbeltankles.Size = new System.Drawing.Size(51, 48);
            this.maximumbeltankles.TabIndex = 4;
            this.maximumbeltankles.TabStop = false;
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.vScrollBar1.Location = new System.Drawing.Point(1011, 173);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(26, 297);
            this.vScrollBar1.TabIndex = 3;
            // 
            // nextdragiconscreen
            // 
            this.nextdragiconscreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nextdragiconscreen.Location = new System.Drawing.Point(731, 637);
            this.nextdragiconscreen.Name = "nextdragiconscreen";
            this.nextdragiconscreen.Size = new System.Drawing.Size(118, 42);
            this.nextdragiconscreen.TabIndex = 2;
            this.nextdragiconscreen.TabStop = false;
            this.nextdragiconscreen.Click += new System.EventHandler(this.nextdragicon_Click);
            // 
            // returndragiconscreen
            // 
            this.returndragiconscreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.returndragiconscreen.Location = new System.Drawing.Point(128, 637);
            this.returndragiconscreen.Name = "returndragiconscreen";
            this.returndragiconscreen.Size = new System.Drawing.Size(118, 42);
            this.returndragiconscreen.TabIndex = 1;
            this.returndragiconscreen.TabStop = false;
            this.returndragiconscreen.Click += new System.EventHandler(this.returndragicon_click);
            // 
            // beltanklescreen
            // 
            this.beltanklescreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.beltanklescreen.Location = new System.Drawing.Point(5, 3);
            this.beltanklescreen.Name = "beltanklescreen";
            this.beltanklescreen.Size = new System.Drawing.Size(963, 628);
            this.beltanklescreen.TabIndex = 0;
            this.beltanklescreen.TabStop = false;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.labelforsideicon);
            this.tabPage10.Controls.Add(this.originalsizesideview);
            this.tabPage10.Controls.Add(this.minimizesideview);
            this.tabPage10.Controls.Add(this.maximizesideview);
            this.tabPage10.Controls.Add(this.vScrollBarsideview);
            this.tabPage10.Controls.Add(this.nextsideicon);
            this.tabPage10.Controls.Add(this.returnsideicon);
            this.tabPage10.Controls.Add(this.sidescreen);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(1075, 679);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "tabPage10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // labelforsideicon
            // 
            this.labelforsideicon.AutoSize = true;
            this.labelforsideicon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelforsideicon.Location = new System.Drawing.Point(188, 54);
            this.labelforsideicon.Name = "labelforsideicon";
            this.labelforsideicon.Size = new System.Drawing.Size(60, 24);
            this.labelforsideicon.TabIndex = 9;
            this.labelforsideicon.Text = "label3";
            // 
            // originalsizesideview
            // 
            this.originalsizesideview.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.originalsizesideview.Location = new System.Drawing.Point(996, 530);
            this.originalsizesideview.Name = "originalsizesideview";
            this.originalsizesideview.Size = new System.Drawing.Size(51, 48);
            this.originalsizesideview.TabIndex = 8;
            this.originalsizesideview.TabStop = false;
            // 
            // minimizesideview
            // 
            this.minimizesideview.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.minimizesideview.Location = new System.Drawing.Point(996, 476);
            this.minimizesideview.Name = "minimizesideview";
            this.minimizesideview.Size = new System.Drawing.Size(51, 48);
            this.minimizesideview.TabIndex = 7;
            this.minimizesideview.TabStop = false;
            // 
            // maximizesideview
            // 
            this.maximizesideview.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.maximizesideview.Location = new System.Drawing.Point(996, 125);
            this.maximizesideview.Name = "maximizesideview";
            this.maximizesideview.Size = new System.Drawing.Size(51, 48);
            this.maximizesideview.TabIndex = 6;
            this.maximizesideview.TabStop = false;
            // 
            // vScrollBarsideview
            // 
            this.vScrollBarsideview.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.vScrollBarsideview.Location = new System.Drawing.Point(1007, 176);
            this.vScrollBarsideview.Name = "vScrollBarsideview";
            this.vScrollBarsideview.Size = new System.Drawing.Size(26, 297);
            this.vScrollBarsideview.TabIndex = 4;
            // 
            // nextsideicon
            // 
            this.nextsideicon.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nextsideicon.Location = new System.Drawing.Point(702, 633);
            this.nextsideicon.Name = "nextsideicon";
            this.nextsideicon.Size = new System.Drawing.Size(124, 50);
            this.nextsideicon.TabIndex = 2;
            this.nextsideicon.TabStop = false;
            this.nextsideicon.Click += new System.EventHandler(this.nextsideicon_click);
            // 
            // returnsideicon
            // 
            this.returnsideicon.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.returnsideicon.Location = new System.Drawing.Point(153, 633);
            this.returnsideicon.Name = "returnsideicon";
            this.returnsideicon.Size = new System.Drawing.Size(124, 50);
            this.returnsideicon.TabIndex = 1;
            this.returnsideicon.TabStop = false;
            this.returnsideicon.Click += new System.EventHandler(this.returnsideicon_click);
            // 
            // sidescreen
            // 
            this.sidescreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sidescreen.Location = new System.Drawing.Point(3, 0);
            this.sidescreen.Name = "sidescreen";
            this.sidescreen.Size = new System.Drawing.Size(969, 624);
            this.sidescreen.TabIndex = 0;
            this.sidescreen.TabStop = false;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.originalsizefinalscreen);
            this.tabPage11.Controls.Add(this.minimizefinalscreen);
            this.tabPage11.Controls.Add(this.maximizefinalscreen);
            this.tabPage11.Controls.Add(this.vScrollBarfinalscreen);
            this.tabPage11.Controls.Add(this.initialscreenbutton);
            this.tabPage11.Controls.Add(this.restartbutton);
            this.tabPage11.Controls.Add(this.saveresultbutton);
            this.tabPage11.Controls.Add(this.printreportbutton);
            this.tabPage11.Controls.Add(this.showreportbutton);
            this.tabPage11.Controls.Add(this.checkpositionbutton);
            this.tabPage11.Controls.Add(this.changebutton);
            this.tabPage11.Controls.Add(this.editid);
            this.tabPage11.Controls.Add(this.finalscreen);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(1075, 679);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "tabPage11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // originalsizefinalscreen
            // 
            this.originalsizefinalscreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.originalsizefinalscreen.Location = new System.Drawing.Point(1009, 535);
            this.originalsizefinalscreen.Name = "originalsizefinalscreen";
            this.originalsizefinalscreen.Size = new System.Drawing.Size(51, 48);
            this.originalsizefinalscreen.TabIndex = 12;
            this.originalsizefinalscreen.TabStop = false;
            // 
            // minimizefinalscreen
            // 
            this.minimizefinalscreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.minimizefinalscreen.Location = new System.Drawing.Point(1009, 468);
            this.minimizefinalscreen.Name = "minimizefinalscreen";
            this.minimizefinalscreen.Size = new System.Drawing.Size(51, 48);
            this.minimizefinalscreen.TabIndex = 11;
            this.minimizefinalscreen.TabStop = false;
            // 
            // maximizefinalscreen
            // 
            this.maximizefinalscreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.maximizefinalscreen.Location = new System.Drawing.Point(1009, 117);
            this.maximizefinalscreen.Name = "maximizefinalscreen";
            this.maximizefinalscreen.Size = new System.Drawing.Size(51, 48);
            this.maximizefinalscreen.TabIndex = 10;
            this.maximizefinalscreen.TabStop = false;
            // 
            // vScrollBarfinalscreen
            // 
            this.vScrollBarfinalscreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.vScrollBarfinalscreen.Location = new System.Drawing.Point(1020, 168);
            this.vScrollBarfinalscreen.Name = "vScrollBarfinalscreen";
            this.vScrollBarfinalscreen.Size = new System.Drawing.Size(26, 297);
            this.vScrollBarfinalscreen.TabIndex = 9;
            // 
            // initialscreenbutton
            // 
            this.initialscreenbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.initialscreenbutton.Location = new System.Drawing.Point(936, 626);
            this.initialscreenbutton.Name = "initialscreenbutton";
            this.initialscreenbutton.Size = new System.Drawing.Size(124, 50);
            this.initialscreenbutton.TabIndex = 8;
            this.initialscreenbutton.TabStop = false;
            // 
            // restartbutton
            // 
            this.restartbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.restartbutton.Location = new System.Drawing.Point(806, 626);
            this.restartbutton.Name = "restartbutton";
            this.restartbutton.Size = new System.Drawing.Size(124, 50);
            this.restartbutton.TabIndex = 7;
            this.restartbutton.TabStop = false;
            // 
            // saveresultbutton
            // 
            this.saveresultbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.saveresultbutton.Location = new System.Drawing.Point(676, 626);
            this.saveresultbutton.Name = "saveresultbutton";
            this.saveresultbutton.Size = new System.Drawing.Size(124, 50);
            this.saveresultbutton.TabIndex = 6;
            this.saveresultbutton.TabStop = false;
            // 
            // printreportbutton
            // 
            this.printreportbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.printreportbutton.Location = new System.Drawing.Point(546, 627);
            this.printreportbutton.Name = "printreportbutton";
            this.printreportbutton.Size = new System.Drawing.Size(124, 50);
            this.printreportbutton.TabIndex = 5;
            this.printreportbutton.TabStop = false;
            // 
            // showreportbutton
            // 
            this.showreportbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.showreportbutton.Location = new System.Drawing.Point(416, 627);
            this.showreportbutton.Name = "showreportbutton";
            this.showreportbutton.Size = new System.Drawing.Size(124, 50);
            this.showreportbutton.TabIndex = 4;
            this.showreportbutton.TabStop = false;
            // 
            // checkpositionbutton
            // 
            this.checkpositionbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkpositionbutton.Location = new System.Drawing.Point(286, 627);
            this.checkpositionbutton.Name = "checkpositionbutton";
            this.checkpositionbutton.Size = new System.Drawing.Size(124, 50);
            this.checkpositionbutton.TabIndex = 3;
            this.checkpositionbutton.TabStop = false;
            // 
            // changebutton
            // 
            this.changebutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.changebutton.Location = new System.Drawing.Point(156, 627);
            this.changebutton.Name = "changebutton";
            this.changebutton.Size = new System.Drawing.Size(124, 50);
            this.changebutton.TabIndex = 2;
            this.changebutton.TabStop = false;
            // 
            // editid
            // 
            this.editid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.editid.Location = new System.Drawing.Point(26, 630);
            this.editid.Name = "editid";
            this.editid.Size = new System.Drawing.Size(124, 50);
            this.editid.TabIndex = 1;
            this.editid.TabStop = false;
            // 
            // finalscreen
            // 
            this.finalscreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.finalscreen.Location = new System.Drawing.Point(26, 0);
            this.finalscreen.Name = "finalscreen";
            this.finalscreen.Size = new System.Drawing.Size(966, 646);
            this.finalscreen.TabIndex = 0;
            this.finalscreen.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Silver;
            this.button1.Location = new System.Drawing.Point(367, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 47);
            this.button1.TabIndex = 3;
            this.button1.Text = "Settings";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // quitbutton
            // 
            this.quitbutton.Location = new System.Drawing.Point(243, 3);
            this.quitbutton.Name = "quitbutton";
            this.quitbutton.Size = new System.Drawing.Size(118, 50);
            this.quitbutton.TabIndex = 2;
            this.quitbutton.TabStop = false;
            this.quitbutton.Click += new System.EventHandler(this.quit_click);
            this.quitbutton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox4_MouseDown);
            // 
            // openresult
            // 
            this.openresult.Location = new System.Drawing.Point(122, 3);
            this.openresult.Name = "openresult";
            this.openresult.Size = new System.Drawing.Size(115, 50);
            this.openresult.TabIndex = 1;
            this.openresult.TabStop = false;
            // 
            // startbutton
            // 
            this.startbutton.Location = new System.Drawing.Point(3, 3);
            this.startbutton.Name = "startbutton";
            this.startbutton.Size = new System.Drawing.Size(113, 50);
            this.startbutton.TabIndex = 0;
            this.startbutton.TabStop = false;
            this.startbutton.Click += new System.EventHandler(this.start_click);
            this.startbutton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.startbutton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1083, 733);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainpage)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.startinitial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openresultbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quitapp)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Year)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.returnbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startinitialscreen)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Speechimagetext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enteruprightimage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextimage)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.returnimage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openimage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotateimage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Loadimage)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.originalsizebeltankles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimumbeltankles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximumbeltankles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextdragiconscreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.returndragiconscreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beltanklescreen)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.originalsizesideview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizesideview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximizesideview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextsideicon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.returnsideicon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sidescreen)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.originalsizefinalscreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizefinalscreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximizefinalscreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialscreenbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.restartbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveresultbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printreportbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showreportbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkpositionbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changebutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalscreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quitbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openresult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startbutton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox startinitialscreen;
        private System.Windows.Forms.PictureBox returnbutton;
        private System.Windows.Forms.PictureBox nextbutton;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox user_ID;
        private System.Windows.Forms.TextBox user_Name;
        private System.Windows.Forms.ComboBox Gender;
        private System.Windows.Forms.ComboBox Day;
        private System.Windows.Forms.ComboBox Month;
        private System.Windows.Forms.NumericUpDown Year;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.PictureBox Loadimage;
        private System.Windows.Forms.PictureBox nextimage;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.PictureBox returnimage;
        private System.Windows.Forms.PictureBox openimage;
        private System.Windows.Forms.PictureBox rotateimage;
        private System.Windows.Forms.PictureBox Speechimagetext;
        private System.Windows.Forms.PictureBox enteruprightimage;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.PictureBox nextsideicon;
        private System.Windows.Forms.PictureBox returnsideicon;
        private System.Windows.Forms.PictureBox sidescreen;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.PictureBox finalscreen;
        private System.Windows.Forms.PictureBox editid;
        private System.Windows.Forms.PictureBox initialscreenbutton;
        private System.Windows.Forms.PictureBox restartbutton;
        private System.Windows.Forms.PictureBox saveresultbutton;
        private System.Windows.Forms.PictureBox printreportbutton;
        private System.Windows.Forms.PictureBox showreportbutton;
        private System.Windows.Forms.PictureBox checkpositionbutton;
        private System.Windows.Forms.PictureBox changebutton;
        private System.Windows.Forms.PictureBox originalsizesideview;
        private System.Windows.Forms.PictureBox minimizesideview;
        private System.Windows.Forms.PictureBox maximizesideview;
        private System.Windows.Forms.VScrollBar vScrollBarsideview;
        private System.Windows.Forms.PictureBox originalsizefinalscreen;
        private System.Windows.Forms.PictureBox minimizefinalscreen;
        private System.Windows.Forms.PictureBox maximizefinalscreen;
        private System.Windows.Forms.VScrollBar vScrollBarfinalscreen;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox mainpage;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.PictureBox originalsizebeltankles;
        private System.Windows.Forms.PictureBox minimumbeltankles;
        private System.Windows.Forms.PictureBox maximumbeltankles;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.PictureBox nextdragiconscreen;
        private System.Windows.Forms.PictureBox returndragiconscreen;
        private System.Windows.Forms.PictureBox beltanklescreen;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox quitbutton;
        private System.Windows.Forms.PictureBox openresult;
        private System.Windows.Forms.PictureBox startbutton;
        private System.Windows.Forms.PictureBox startinitial;
        private System.Windows.Forms.PictureBox openresultbutton;
        private System.Windows.Forms.PictureBox quitapp;
        private System.Windows.Forms.Button settingsbutton;
        private System.Windows.Forms.Label labelforiconsscreen;
        private System.Windows.Forms.Label labelforsideicon;
        private System.Windows.Forms.Label useridlabel;
        private System.Windows.Forms.Label usernamelabel;
    }
}

