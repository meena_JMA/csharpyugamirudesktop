﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class TextBuffer
    {
        string m_lpszText;
        int m_iIndex;

        public TextBuffer( string lpszText )
        {
            m_lpszText = lpszText;
                m_iIndex = 0;
        }

   
      

    TextBuffer( TextBuffer rSrc ) 
        {
            m_lpszText = rSrc.m_lpszText;
            
    m_iIndex = rSrc.m_iIndex;
        }

  ~TextBuffer( )
{
}
/*
CTextBuffer &CTextBuffer::operator=( const CTextBuffer &rSrc )
{
	m_lpszText	= rSrc.m_lpszText;
	m_iIndex	= rSrc.m_iIndex;
	return( *this );
}*/

    public MultiChar GetCurrentMultiChar( ) 
{
	MultiChar MultiChar = new MultiChar();
	
	if ( m_lpszText == string.Empty ){
		return MultiChar;
	}
            //  if ( IsDBCSLeadByteEx( 932, (BYTE)*(m_lpszText + m_iIndex ) ) ){
            /*  MultiChar.m_achChar[0] = (m_lpszText + m_iIndex);
              MultiChar.m_achChar[1] = m_lpszText+m_iIndex.ToString() + 1 ;
              MultiChar.m_iValidCount = 2;*/
            //}
            //else{
            //MultiChar.m_achChar[0] = *( m_lpszText + m_iIndex );
            MultiChar.m_achChar[0] = m_lpszText[0];

        MultiChar.m_achChar[1] = '\0';
		MultiChar.m_iValidCount = 1;
	//}
	return MultiChar;
}

public void IncrementPointer()
{
    if (m_lpszText == string.Empty)
    {
        return;
    }
  //  if (IsDBCSLeadByteEx(932, (BYTE) * (m_lpszText + m_iIndex)))
    {
        m_iIndex += 2;
    }
  /*  else
    {
        m_iIndex++;
    }*/
}

public bool IsEOF( ) 
{
	if ( m_lpszText == string.Empty ){
		return( true );
	}
	/*if ( *( m_lpszText + m_iIndex ) == '\0' ){
		return( true );
	}*/
	return( false );
}

public void SkipSpaceOrTab()
{
    MultiChar MultiChar;

    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsEqual('\0'))
        {
            return;
        }
        if (MultiChar.IsEqual(' '))
        {
            IncrementPointer();
        }
        else if (MultiChar.IsEqual('\t'))
        {
            IncrementPointer();
        }
        else
        {
            break;
        }
    }
}

public bool ReadNewLine()
{
    MultiChar MultiChar;
    MultiChar = GetCurrentMultiChar();
    /*if (MultiChar.IsEqual('0x0D'))
    {
        IncrementPointer();
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsEqual('0x0A'))
        {
            IncrementPointer();
        }
        return (true);
    }
    else if (MultiChar.IsEqual('0x0A'))
    {
        IncrementPointer();
        return (true);
    }*/
    return (false);
}

public bool ReadValue(int iValue )
{
    MultiChar MultiChar;
    iValue = 0;
    int iCharCount = 0;
    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsHankakuDigit())
        {
            iValue = iValue * 10 + MultiChar.GetHankakuDigitValue();
            iCharCount++;
            IncrementPointer();
        }
        else
        {
            break;
        }
    }
    if (iCharCount <= 0)
    {
        return (false);
    }
    return (true);
}

public bool ReadValue(String strValue )
{
            strValue = null;
            MultiChar MultiChar;
    int iCharCount = 0;
    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsHankakuDigit())
        {
            strValue.PadLeft(MultiChar.m_achChar[0]);
            iCharCount++;
            IncrementPointer();
        }
        else
        {
            break;
        }
    }
    if (iCharCount <= 0)
    {
        return (false);
    }
    return (true);
}

public bool ReadSignedValue(int iValue )
{
    MultiChar MultiChar;
    int iSign = 1;
    iValue = 0;
    int iCharCount = 0;
    int iIndexOld = m_iIndex;

    MultiChar = GetCurrentMultiChar();
    if (MultiChar.IsEqual('+'))
    {
        iSign = 1;
        IncrementPointer();
    }
    else if (MultiChar.IsEqual('-'))
    {
        iSign = -1;
        IncrementPointer();
    }
    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsHankakuDigit())
        {
            iValue = iValue * 10 + MultiChar.GetHankakuDigitValue();
            iCharCount++;
            IncrementPointer();
        }
        else
        {
            break;
        }
    }
    if (iCharCount <= 0)
    {
        m_iIndex = iIndexOld;
        return (false);
    }
    else
    {
        iValue = iValue * iSign;
    }
    return (true);
}

public bool ReadSignedValue(String strValue )
{
    strValue = null;

    MultiChar MultiChar;
    int iCharCount = 0;
    int iIndexOld = m_iIndex;

    MultiChar = GetCurrentMultiChar();
    if (MultiChar.IsEqual('+'))
    {
        strValue.PadLeft('+');
        IncrementPointer();
    }
    else if (MultiChar.IsEqual('-'))
    {
        strValue.PadLeft('-');//need to check pad left-meena
        IncrementPointer();
    }
    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsHankakuDigit())
        {
            strValue.PadLeft(MultiChar.m_achChar[0]);
            iCharCount++;
            IncrementPointer();
        }
        else
        {
            break;
        }
    }
    if (iCharCount <= 0)
    {
        m_iIndex = iIndexOld;
        return (false);
    }

    return (true);
}

public bool ReadRealValue(String strValue )
{
    strValue = "";

    MultiChar MultiChar;
    int iCharCount = 0;
    int iHeadDigitIsZero = 0;
    int iIndexOld = m_iIndex;

    MultiChar = GetCurrentMultiChar();
    if (MultiChar.IsEqual('+'))
    {
        strValue.PadLeft('+');
        IncrementPointer();
    }
    else if (MultiChar.IsEqual('-'))
    {
        strValue.PadLeft('-');
        IncrementPointer();
    }
    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsHankakuDigit())
        {
            strValue.PadLeft(MultiChar.m_achChar[0]);
            if (iCharCount == 0)
            {
                if (MultiChar.IsEqual('0'))
                {
                    iHeadDigitIsZero = 1;
                }
            }
            else
            {
                if (iHeadDigitIsZero > 0)
                {
                    m_iIndex = iIndexOld;
                    return (false);
                }
            }
            iCharCount++;
            IncrementPointer();
        }
        else
        {
            break;
        }
    }
    if (iCharCount <= 0)
    {
        m_iIndex = iIndexOld;
        return (false);
    }
    MultiChar = GetCurrentMultiChar();
    if (MultiChar.IsEqual('.'))
    {
        // ¬”“_‚ª‚ ‚ê‚Î‚Ü‚¾‘±‚¯‚é.
        // ‚½‚¾‚µA¬”“_‚Ì‘O‚É”Žš‚ª‚¢‚­‚Â‚ ‚Á‚½‚Æ‚µ‚Ä‚àA.
        // ¬”“_‚ÌŒã‚É‚àÅ’áˆê‚Â”Žš‚ª‚È‚­‚Ä‚Í‚È‚ç‚È‚¢.
        strValue.PadLeft(MultiChar.m_achChar[0]);
        IncrementPointer();
    }
    else
    {
        return (true);
    }

    iCharCount = 0;
    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsHankakuDigit())
        {
            strValue.PadLeft(MultiChar.m_achChar[0]);
            iCharCount++;
            IncrementPointer();
        }
        else
        {
            break;
        }
    }
    if (iCharCount <= 0)
    {
        m_iIndex = iIndexOld;
        return (false);
    }

    return (true);
}

public bool ReadComma()
{
    MultiChar MultiChar;
    MultiChar = GetCurrentMultiChar();
    if (MultiChar.IsEqual(','))
    {
        IncrementPointer();
        return (true);
    }
    return (false);
}

public bool ReadSemicolon()
{
    MultiChar MultiChar;
    MultiChar = GetCurrentMultiChar();
    if (MultiChar.IsEqual(';'))
    {
        IncrementPointer();
        return (true);
    }
    return (false);
}

public bool ReadLeftParenthesis( )
{
    MultiChar MultiChar;
    MultiChar = GetCurrentMultiChar();
    if (MultiChar.IsEqual('('))
    {
        IncrementPointer();
        return (true);
    }
    return (false);
}

public bool ReadRightParenthesis( )
{
    MultiChar MultiChar;
    MultiChar = GetCurrentMultiChar();
    if (MultiChar.IsEqual(')'))
    {
        IncrementPointer();
        return (true);
    }
    return (false);
}

public bool ReadString(String strText )
{
    strText = null;

    MultiChar MultiChar;
    MultiChar = GetCurrentMultiChar();
    int iIndexOld = m_iIndex;

    if (!MultiChar.IsEqual('\"'))
    {
        m_iIndex = iIndexOld;
        return (false);
    }
    IncrementPointer();

    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if (MultiChar.IsEqual('\0'))
        {
            m_iIndex = iIndexOld;
            return (false);
        }
        if (MultiChar.IsEqual('\"'))
        {
            IncrementPointer();
            break;
        }
        if (MultiChar.m_iValidCount >= 1)
        {
            strText.PadLeft(MultiChar.m_achChar[0]);
        }
        if (MultiChar.m_iValidCount >= 2)
        {
            strText.PadLeft(MultiChar.m_achChar[1]);
        }
        IncrementPointer();
    }
    return (true);
}

public bool ReadSymbol(String strText )
{
    strText = null;

    MultiChar MultiChar;
    MultiChar = GetCurrentMultiChar();
    int iIndexOld = m_iIndex;

    if ((!MultiChar.IsHankakuAlphabet()) && (!MultiChar.IsEqual('_')))
    {
        m_iIndex = iIndexOld;
        return (false);
    }
    strText.PadLeft(MultiChar.m_achChar[0]);
    IncrementPointer();

    while (true)
    {
        MultiChar = GetCurrentMultiChar();
        if ((MultiChar.IsHankakuDigit()) ||
            (MultiChar.IsHankakuAlphabet()) ||
            (MultiChar.IsEqual('_')))
        {
            strText.PadLeft(MultiChar.m_achChar[0]);
            IncrementPointer();
        }
        else
        {
            break;
        }
    }
    return (true);
}

    }
}
