﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class ResultActionScriptElement
    {
        public int m_iScriptOpecodeID;         // ƒIƒyƒR[ƒh‚h‚c.
        public int m_iBodyPositionTypeID;      // ‘ÌˆÊƒ^ƒCƒv‚h‚c.
        public int m_iBodyAngleID;             // Šp“x‚h‚c.
        public int m_iMinValueOperatorID;      // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
        public int m_iMaxValueOperatorID;      // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
        public double m_dMinValue;             // Å¬’l.
        public double m_dMaxValue;             // Å‘å’l.
        public int m_iResultID;                // Œ‹‰Ê‚h‚c.
        public int m_iResultValue;             // Œ‹‰ÊÝ’è’l.
        public string m_strRangeSymbol;			// ”ÍˆÍƒVƒ“ƒ{ƒ‹.
        // Šp“xƒAƒNƒVƒ‡ƒ“ƒXƒNƒŠƒvƒg‚Ìƒ}ƒXƒ^[ƒf[ƒ^‚ÌƒXƒL[ƒ}.
        SymbolFunc m_SymbolFunc;
        public ResultActionScriptElement( )
        {
            m_SymbolFunc = new SymbolFunc();
            m_iScriptOpecodeID = m_SymbolFunc.ANGLESCRIPTOPECODEID_NOP;     // ƒIƒyƒR[ƒh‚h‚c.

            m_iBodyPositionTypeID = m_SymbolFunc.BODYPOSITIONTYPEID_NONE;       // ‘ÌˆÊƒ^ƒCƒv‚h‚c.

            m_iBodyAngleID = m_SymbolFunc.BODYANGLEID_NONE;				// Šp“x‚h‚c.

            m_iMinValueOperatorID = m_SymbolFunc.COMPAREOPERATORID_NONE;    // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.

            m_iMaxValueOperatorID = m_SymbolFunc.COMPAREOPERATORID_NONE;        // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.

            m_dMinValue = 0.0;                              // Å¬’l.

            m_dMaxValue = 0.0;                          // Å‘å’l.

            m_iResultID = m_SymbolFunc.RESULTID_NONE;                           // Œ‹‰Ê‚h‚c.

            m_iResultValue = 0;                             // Œ‹‰ÊÝ’è’l.

            m_strRangeSymbol = string.Empty;                              // ”ÍˆÍƒVƒ“ƒ{ƒ‹.


        }
    /*
        CResultActionScriptElement::CResultActionScriptElement( const CResultActionScriptElement &rSrc ) :

    m_iScriptOpecodeID(rSrc.m_iScriptOpecodeID),			// ƒIƒyƒR[ƒh‚h‚c.

    m_iBodyPositionTypeID(rSrc.m_iBodyPositionTypeID),		// ‘ÌˆÊƒ^ƒCƒv‚h‚c.

    m_iBodyAngleID(rSrc.m_iBodyAngleID),					// Šp“x‚h‚c.

    m_iMinValueOperatorID(rSrc.m_iMinValueOperatorID),		// Å¬’l”äŠr‰‰ŽZŽq‚h‚c.

    m_iMaxValueOperatorID(rSrc.m_iMaxValueOperatorID),		// Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.

    m_dMinValue(rSrc.m_dMinValue),							// Å¬’l.

    m_dMaxValue(rSrc.m_dMaxValue),							// Å‘å’l.

    m_iResultID(rSrc.m_iResultID),							// Œ‹‰Ê‚h‚c.

    m_iResultValue(rSrc.m_iResultValue),					// Œ‹‰ÊÝ’è’l.

    m_strRangeSymbol(rSrc.m_strRangeSymbol)                 // ”ÍˆÍƒVƒ“ƒ{ƒ‹.
        {
        }

        CResultActionScriptElement::~CResultActionScriptElement( void )
{
}

CResultActionScriptElement &CResultActionScriptElement::operator=( const CResultActionScriptElement &rSrc )
{
	m_iScriptOpecodeID		= rSrc.m_iScriptOpecodeID;			// ƒIƒyƒR[ƒh‚h‚c.
	m_iBodyPositionTypeID	= rSrc.m_iBodyPositionTypeID;		// ‘ÌˆÊƒ^ƒCƒv‚h‚c.
	m_iBodyAngleID			= rSrc.m_iBodyAngleID;				// Šp“x‚h‚c.
	m_iMinValueOperatorID	= rSrc.m_iMinValueOperatorID;		// Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
	m_iMaxValueOperatorID	= rSrc.m_iMaxValueOperatorID;		// Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
	m_dMinValue				= rSrc.m_dMinValue;					// Å¬’l.
	m_dMaxValue				= rSrc.m_dMaxValue;					// Å‘å’l.
	m_iResultID				= rSrc.m_iResultID;					// Œ‹‰Ê‚h‚c.
	m_iResultValue			= rSrc.m_iResultValue;				// Œ‹‰ÊÝ’è’l.
	m_strRangeSymbol		= rSrc.m_strRangeSymbol;			// ”ÍˆÍƒVƒ“ƒ{ƒ‹.

	return *this;
}
*/
 public   void ResolveRange(int iMinValueOperatorID, int iMaxValueOperatorID, double dMinValue, double dMaxValue)
    {
        m_iMinValueOperatorID = iMinValueOperatorID;    // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
        m_iMaxValueOperatorID = iMaxValueOperatorID;    // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
        m_dMinValue = dMinValue;            // Å¬’l.
        m_dMaxValue = dMaxValue;            // Å‘å’l.
    }

    public bool IsSatisfied(double dValue) 
{
	if ( m_iMinValueOperatorID == m_SymbolFunc.COMPAREOPERATORID_GREATER ){
		if ( !( dValue > m_dMinValue ) ){
			return false;
		}	
	}

	if ( m_iMinValueOperatorID == m_SymbolFunc.COMPAREOPERATORID_GREATEROREQUAL ){
		if ( !(dValue >= m_dMinValue ) ){
			return false;
		}
	}

	if ( m_iMaxValueOperatorID == m_SymbolFunc.COMPAREOPERATORID_LESS ){
		if ( !( dValue<m_dMaxValue ) ){
			return false;
		}	
	}

	if ( m_iMaxValueOperatorID == m_SymbolFunc.COMPAREOPERATORID_LESSOREQUAL ){
		if ( !( dValue <= m_dMaxValue ) ){
			return false;
		}	
	}

	return true;
}

public int ReadFromString(string lpszText)
{
    string strFunctionNameSymbol = null;

    TextBuffer TextBuffer= new TextBuffer(lpszText);

    TextBuffer.SkipSpaceOrTab();
    if (TextBuffer.IsEOF())
    {
                return (m_SymbolFunc.READFROMSTRING_NULLLINE);
                //return true;
    }

    if (TextBuffer.ReadNewLine())
    {
        if (TextBuffer.IsEOF())
        {
                    return (m_SymbolFunc.READFROMSTRING_NULLLINE);
                    //return true;
        }
    }

    if (!TextBuffer.ReadSymbol(strFunctionNameSymbol))
    {
                 return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
                //return false;
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadLeftParenthesis())
    {
                return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
                //return false;
    }
    if (strFunctionNameSymbol == "StartAngleCondition")
    {
        string strBodyPositionTypeSymbol = null;

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(strBodyPositionTypeSymbol))
        {
                    return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
                    //return false;
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadRightParenthesis())
        {
                    return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
                    //return false;
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSemicolon())
        {
                    return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
                    //return false;
        }

        TextBuffer.SkipSpaceOrTab();
        TextBuffer.ReadNewLine();
        if (!TextBuffer.IsEOF())
        {
                    return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
                    //return false;
        }

        m_iScriptOpecodeID =m_SymbolFunc.ANGLESCRIPTOPECODEID_START;
        m_iBodyPositionTypeID = m_SymbolFunc.GetBodyPositionTypeIDBySymbol(strBodyPositionTypeSymbol);
        if (m_iBodyPositionTypeID == m_SymbolFunc.BODYPOSITIONTYPEID_NONE)
        {
            return (m_SymbolFunc.READFROMSTRING_BODYPOSITIONTYPE_INVALID);
           
        }
        return (m_SymbolFunc.READFROMSTRING_SYNTAX_OK);
    }
    else if (strFunctionNameSymbol == "CheckAngleCondition")
    {
        string strBodyAngleSymbol = null;
        string strRangeSymbol= null;

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(strBodyAngleSymbol))
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(strRangeSymbol))
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadRightParenthesis())
        {
            return (m_SymbolFunc.READFROMSTRING_RESULT_INVALID);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSemicolon())
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        TextBuffer.ReadNewLine();
        if (!TextBuffer.IsEOF())
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        m_iScriptOpecodeID = m_SymbolFunc.ANGLESCRIPTOPECODEID_CHECK;
        m_iBodyAngleID = m_SymbolFunc.GetBodyAngleIDBySymbol(strBodyAngleSymbol);
        if (m_iBodyAngleID == m_SymbolFunc.BODYANGLEID_NONE)
        {
            return (m_SymbolFunc.READFROMSTRING_BODYANGLE_INVALID);
        }
        m_strRangeSymbol = strRangeSymbol;
        return (m_SymbolFunc.READFROMSTRING_SYNTAX_OK);
    }
    else if (strFunctionNameSymbol == "EndAngleCondition")
    {
        string strResultSymbol = null;
        string strValue = null;

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(strResultSymbol))
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSignedValue(strValue))
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadRightParenthesis())
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSemicolon())
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        TextBuffer.ReadNewLine();
        if (!TextBuffer.IsEOF())
        {
            return (m_SymbolFunc.READFROMSTRING_SYNTAX_ERROR);
        }

        m_iScriptOpecodeID = m_SymbolFunc.ANGLESCRIPTOPECODEID_END;
        m_iResultID = m_SymbolFunc.GetResultIDBySymbol(strResultSymbol);
        if (m_iResultID == m_SymbolFunc.RESULTID_NONE)
        {
            return (m_SymbolFunc.READFROMSTRING_RESULT_INVALID);
        }
        m_iResultValue = int.Parse(strValue);
        return (m_SymbolFunc.READFROMSTRING_SYNTAX_OK);
    }

    return (m_SymbolFunc.READFROMSTRING_OPECODE_INVALID);
}

    }
}
