﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//#define PI	3.141592;
namespace Yugamiru
{
    public class FrontBodyAngle
    {
        
        public double m_dHip;                  //@œ”ÕŠp“x
        public double m_dRightThigh;           //@‘å‘ÚŠp“x
        public double m_dLeftThigh;            //@‘å‘ÚŠp“x
        public double m_dRightShank;           //@‰º‘ÚŠp“x
        public double m_dLeftShank;            //@‰º‘ÚŠp“x
        public double m_dCenter;               //@‘«-œ”Õ’†S
        public double m_dHead;                 //@‘ÌŠ²’†S-“ª•”’†SŠp“x
        public double m_dEar;                  //@Ž¨Šp“x
        public double m_dShoulder;             //@Œ¨Šp“x
        public double m_dBody2Loins;           //@‘ÌŠ²(Œ¨’†S)-œ”Õ’†S@Šp“x
        public double m_dBody2Center;			//@‘ÌŠ²iŒ¨’†Sj-‘«’†S@Šp“x
      

        public bool m_bStanding = false;

        public FrontBodyAngle()
        {     
        }
     
         ~FrontBodyAngle(  )
        {
        }

    public void CalcByPosition(bool bStanding, FrontBodyPosition pBodyPos, double dCameraAngle)
    {
        m_bStanding = bStanding;
        Clear();
        CalcUnderBodyByPosition(pBodyPos, dCameraAngle);
        CalcCenterByPosition(pBodyPos);
        CalcUpperBodyByPosition(pBodyPos, dCameraAngle);
    }

    public void Clear()
    {
        m_dHip = -999;          //@œ”ÕŠp“x
        m_dRightThigh = -999;           //@‘å‘ÚŠp“x
        m_dLeftThigh = -999;            //@‘å‘ÚŠp“x
        m_dRightShank = -999;           //@‰º‘ÚŠp“x
        m_dLeftShank = -999;            //@‰º‘ÚŠp“x
        m_dCenter = -999;           //@‘«-œ”Õ’†S

        m_dHead = -999;         //@‘ÌŠ²’†S-“ª•”’†SŠp“x
        m_dEar = -999;          //@Ž¨Šp“x
        m_dShoulder = -999;         //@Œ¨Šp“x
        m_dBody2Loins = -999;           //@‘ÌŠ²(Œ¨’†S)-œ”Õ’†S@Šp“x
        m_dBody2Center = -999;          //@‘ÌŠ²iŒ¨’†Sj-‘«’†S@Šp“x
    }

    public void CalcUnderBodyByPosition(FrontBodyPosition pBodyPos, double dCameraAngle)
    {
        // ŠÖßˆÊ’u‚©‚çŠp“x‚ðŽZo‚µAƒJƒƒ‰‚ÌŒX‚«•ª·‚µˆø‚­

        double hipL = Math.Sqrt(Math.Pow((double)(pBodyPos.GetLeftHipY() - pBodyPos.GetRightHipY()), 2.0) + Math.Pow((double)(pBodyPos.GetLeftHipX() - pBodyPos.GetRightHipX()), 2.0));
        double lthighL = Math.Sqrt(Math.Pow((double)(pBodyPos.GetLeftHipY() - pBodyPos.GetLeftKneeY()), 2.0) + Math.Pow((double)(pBodyPos.GetLeftHipX() - pBodyPos.GetLeftKneeX()), 2.0));
        double rthighL = Math.Sqrt(Math.Pow((double)(pBodyPos.GetRightHipY() - pBodyPos.GetRightKneeY()), 2.0) + Math.Pow((double)(pBodyPos.GetRightHipX() - pBodyPos.GetRightKneeX()), 2.0));
        double lshankL = Math.Sqrt(Math.Pow((double)(pBodyPos.GetLeftKneeY() - pBodyPos.GetLeftAnkleY()), 2.0) + Math.Pow((double)(pBodyPos.GetLeftKneeX() - pBodyPos.GetLeftAnkleX()), 2.0));
        double rshankL = Math.Sqrt(Math.Pow((double)(pBodyPos.GetRightKneeY() - pBodyPos.GetRightAnkleY()), 2.0) + Math.Pow((double)(pBodyPos.GetRightKneeX() - pBodyPos.GetRightAnkleX()), 2.0));

        // œ”Õ‚ÌŒX‚«
        m_dHip = Math.Atan2((double)-(pBodyPos.GetLeftHipY() - pBodyPos.GetRightHipY()),
                        (double)(pBodyPos.GetLeftHipX() - pBodyPos.GetRightHipX()))
                                * 180.0 / Math.PI - dCameraAngle;
        // •G‚ÌŒX‚«
        double a1, a2;
        a1 = Math.Atan2((double)(pBodyPos.GetLeftKneeX() - pBodyPos.GetLeftHipX()),
                    (double)-(pBodyPos.GetLeftHipY() - pBodyPos.GetLeftKneeY()))
                                * 180.0 / Math.PI;
        a2 = Math.Atan2((double)(pBodyPos.GetLeftKneeX() - pBodyPos.GetLeftAnkleX()),
                    (double)-(pBodyPos.GetLeftKneeY() - pBodyPos.GetLeftAnkleY()))
                                * 180.0 / Math.PI;
        m_dLeftThigh = a1 + a2;

        a1 = Math.Atan2((double)(pBodyPos.GetRightKneeX() - pBodyPos.GetRightHipX()),
                    (double)-(pBodyPos.GetRightHipY() - pBodyPos.GetRightKneeY()))
                                * 180.0 / Math.PI;
        a2 = Math.Atan2((double)(pBodyPos.GetRightKneeX() - pBodyPos.GetRightAnkleX()),
                    (double)-(pBodyPos.GetRightKneeY() - pBodyPos.GetRightAnkleY()))
                                * 180.0 / Math.PI;
        m_dRightThigh = (a1 + a2) * -1.0;

        //‰º‘Ú‚ÌŒX‚«
        m_dLeftShank = Math.Atan2((double)(pBodyPos.GetLeftKneeX() - pBodyPos.GetLeftAnkleX()),
                              (double)-(pBodyPos.GetLeftKneeY() - pBodyPos.GetLeftAnkleY()))
                                * 180.0 / Math.PI - dCameraAngle;

        m_dRightShank = Math.Atan2((double)(pBodyPos.GetRightKneeX() - pBodyPos.GetRightAnkleX()),
                               (double)-(pBodyPos.GetRightKneeY() - pBodyPos.GetRightAnkleY()))
                                * -180.0 / Math.PI + dCameraAngle;
    }

    public void CalcCenterByPosition(FrontBodyPosition pBodyPos)
    {
            // —§ˆÊŽž‚Ìœ”Õ’†S‚ÆdS(‘«’†S)‚ÌŒX‚«‚ð‹‚ß‚é
            
            int[] hipcenter = new int[2];
        int[] fplcenter = new int[2];

        hipcenter[0] = (int)((pBodyPos.GetLeftHipX() + pBodyPos.GetRightHipX()) * 0.5);
        hipcenter[1] = (int)((pBodyPos.GetLeftHipY() + pBodyPos.GetRightHipY()) * 0.5);

        fplcenter[0] = (int)((pBodyPos.GetLeftAnkleX() + pBodyPos.GetRightAnkleX()) * 0.5);
        fplcenter[1] = (int)((pBodyPos.GetLeftAnkleY() + pBodyPos.GetRightAnkleY()) * 0.5);

        m_dCenter = Math.Atan2((double)(hipcenter[0] - fplcenter[0]),
                          (double)-(hipcenter[1] - fplcenter[1]))
                          * 180.0 / Math.PI;

        // X‹rO‹r‚Ì“x‡‚¢‚ð”»’è‚·‚é

        // ‘S‘Ì‚Ì˜c‚Ý‚ð”»’è‚·‚éœ”Õ’†S-•G’†S-‘«’†S‚ª’¼ü‚Æ‚Ç‚ê‚¾‚¯(‚Ç‚Ì‚æ‚¤‚É)‚¸‚ê‚Ä‚¢‚é‚©”»’è
    }

   public  void CalcUpperBodyByPosition(FrontBodyPosition pBodyPos, double dCameraAngle)
    {
        // ŠÖßˆÊ’u‚©‚çŠp“x‚ðŽZo‚µAƒJƒƒ‰‚ÌŒX‚«•ª·‚µˆø‚­
        int[] midankle = new int[2]; // —¼‘«ŠÖß‚Ì’†S
        midankle[0] = (int)((pBodyPos.GetRightAnkleX() + pBodyPos.GetLeftAnkleX()) * 0.5);
        midankle[1] = (int)((pBodyPos.GetRightAnkleY() + pBodyPos.GetLeftAnkleY()) * 0.5);

        int[] midloins = new int[2]; // œ”Õ‚Ì’†S
        midloins[0] = (int)((pBodyPos.GetRightHipX() + pBodyPos.GetLeftHipX()) * 0.5);
        midloins[1] = (int)((pBodyPos.GetRightHipY() + pBodyPos.GetLeftHipY()) * 0.5);

        int[] midshoulder = new int[2]; // —¼Œ¨‚Ì’†S
        midshoulder[0] = (int)((pBodyPos.GetRightShoulderX() + pBodyPos.GetLeftShoulderX()) * 0.5);
        midshoulder[1] = (int)((pBodyPos.GetRightShoulderY() + pBodyPos.GetLeftShoulderY()) * 0.5);

        int[] midhead = new int[2]; // “ª•”‚Ì’†S
        double a1, b1, a2, b2;

        if ((pBodyPos.GetLeftEarX() - pBodyPos.GetRightEarX()) != 0)
        {
            a1 = (pBodyPos.GetLeftEarY() - pBodyPos.GetRightEarY()) / (pBodyPos.GetLeftEarX() - pBodyPos.GetRightEarX());
        }
        else
        {
            a1 = 0.0;
        }
        b1 = pBodyPos.GetRightEarY() - (a1 * pBodyPos.GetRightEarX());

        if ((pBodyPos.GetChinX() - pBodyPos.GetGlabellaX()) != 0)
        {
            a2 = (pBodyPos.GetChinY() - pBodyPos.GetGlabellaY()) / (pBodyPos.GetChinX() - pBodyPos.GetGlabellaX()); // retc1->tin
        }
        else
        {
            a2 = 0.0;
        }
        b2 = pBodyPos.GetChinY() - (a2 * pBodyPos.GetChinX());

        if (a1 != a2)
        {
            midhead[0] = (int)((b2 - b1) / (a1 - a2));
        }
        else
        {
            midhead[0] = (int)((pBodyPos.GetLeftEarX() + pBodyPos.GetRightEarX()) * 0.5);
        }
        midhead[1] = (int)(a1 * midhead[0] + b1);


        // Šç‚ÌŒX‚«
        m_dEar = 180.0 - 1.0 * Math.Atan2((double)(pBodyPos.GetLeftEarX() - pBodyPos.GetRightEarX()),
                                    (double)-(pBodyPos.GetLeftEarY() - pBodyPos.GetRightEarY()))
                                    * 180.0 / Math.PI - dCameraAngle - 90.0;

        // Œ¨‚ÌŒX‚«
        m_dShoulder = 180.0 - 1.0 * Math.Atan2((double)(pBodyPos.GetLeftShoulderX() - pBodyPos.GetRightShoulderX()),
                                         (double)-(pBodyPos.GetLeftShoulderY() - pBodyPos.GetRightShoulderY()))
                                         * 180.0 / Math.PI - dCameraAngle - 90.0;

        // Žñ‚ÌŒX‚«
        m_dHead = Math.Atan2((double)(midhead[0] - midshoulder[0]),
                        (double)-(midhead[1] - midshoulder[1]))
                        * 180.0 / Math.PI - dCameraAngle;

        // ˜-Œ¨’†S‚ÌŒX‚«
        m_dBody2Loins = Math.Atan2((double)(midshoulder[0] - midloins[0]),
                               (double)-(midshoulder[1] - midloins[1]))
                               * 180.0 / Math.PI - dCameraAngle;

        // dS-Œ¨’†S‚ÌŒX‚«
        m_dBody2Center = -1.0 * Math.Atan2((double)(midshoulder[0] - midankle[0]),
                                       (double)-(midshoulder[1] - midankle[1]))
                                       * 180.0 / Math.PI - dCameraAngle;

    }

public   bool IsStanding( ) 
{
	return m_bStanding;
}

public double GetHip( ) 
{
	return m_dHip;	//@œ”ÕŠp“x
}

public double GetRightThigh( ) 
{
	return m_dRightThigh;	//@‘å‘ÚŠp“x
}

public double GetLeftThigh( ) 
{
	return m_dLeftThigh;	//@‘å‘ÚŠp“x
}

public double GetRightShank( ) 
{
	return m_dRightShank;	//@‰º‘ÚŠp“x
}

public double GetLeftShank( ) 
{
	return m_dLeftShank;	//@‰º‘ÚŠp“x
}

public double GetCenter() 
{
	return m_dCenter;	//@‘«-œ”Õ’†S
}

public double GetHead( ) 
{
	return m_dHead;	//@‘ÌŠ²’†S-“ª•”’†SŠp“x
}

public double GetEar( ) 
{
	return m_dEar;	//@Ž¨Šp“x
}

public double GetShoulder( ) 
{
	return m_dShoulder;	//@Œ¨Šp“x
}

public double GetBody2Loins( ) 
{
	return m_dBody2Loins;	//@‘ÌŠ²(Œ¨’†S)-œ”Õ’†S@Šp“x
}

public double GetBody2Center( ) 
{
	return m_dBody2Center;	//@‘ÌŠ²iŒ¨’†Sj-‘«’†S@Šp“x
}
    }
}
