﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class SymbolFunc
    {
        public int MUSCLEID_NONE = 0;
        public int MUSCLEID_LATISSIMUSDORSI = 1;
        public int MUSCLEID_BICEPSFEMORIS_LEFT = 2;
        public int MUSCLEID_BICEPSFEMORIS_RIGHT = 3;
        public int MUSCLEID_TENSORFASCIAELATAE_LEFT = 4;
        public int MUSCLEID_TENSORFASCIAELATAE_RIGHT = 5;
        public int MUSCLEID_VASTUSLATERALIS_LEFT = 6;
        public int MUSCLEID_VASTUSLATERALIS_RIGHT = 7;
        public int MUSCLEID_QUADRICEPSFEMORIS_LEFT = 8;
        public int MUSCLEID_QUADRICEPSFEMORIS_RIGHT = 9;
        public int MUSCLEID_HIPJOINTCAPSULE_LEFT = 10;
        public int MUSCLEID_HIPJOINTCAPSULE_RIGHT = 11;
        public int MUSCLEID_GLUTEUSMEDIUS_LEFT = 12;
        public int MUSCLEID_GLUTEUSMEDIUS_RIGHT = 13;
        public int MUSCLEID_RECTUSABDOMINIS = 14;
        public int MUSCLEID_ILIOTIBIALTRACT_LEFT = 15;
        public int MUSCLEID_ILIOTIBIALTRACT_RIGHT = 16;
        public int MUSCLEID_FIBULARIS_LEFT = 17;
        public int MUSCLEID_FIBULARIS_RIGHT = 18;
        public int MUSCLEID_GASTROCNEMIUS_LEFT = 19;
        public int MUSCLEID_GASTROCNEMIUS_RIGHT = 20;
        public int MUSCLEID_CUBOID_LEFT = 21;
        public int MUSCLEID_CUBOID_RIGHT = 22;
        public int MUSCLEID_ADDUCTOR_LEFT = 23;
        public int MUSCLEID_ADDUCTOR_RIGHT = 24;
        public int MUSCLEID_TRAPEZIUS_LEFT = 25;
        public int MUSCLEID_TRAPEZIUS_RIGHT = 26;
        public int MUSCLEID_STERNOCLEIDOMASTOID = 27;
        public int MUSCLEID_MAX = 28;

        // ‹Ø“÷ó‘Ôƒpƒ^[ƒ“‚h‚c‚Ì’è‹`.
        public int MUSCLESTATEPATTERNID_NONE = 0;
        public int MUSCLESTATEPATTERNID_PELVIC_ROTATED_TO_RIGHT = 1;
        public int MUSCLESTATEPATTERNID_PELVIC_ROTATED_TO_LEFT = 2;
        public int MUSCLESTATEPATTERNID_PELVIC_TILTED_TO_RIGHT = 3;
        public int MUSCLESTATEPATTERNID_PELVIC_TILTED_TO_LEFT = 4;
        public int MUSCLESTATEPATTERNID_BANDLY_LEGGED_ON_RIGHT = 5;
        public int MUSCLESTATEPATTERNID_BANDLY_LEGGED_ON_LEFT = 6;
        public int MUSCLESTATEPATTERNID_CROSS_LEGGED_ON_RIGHT = 7;
        public int MUSCLESTATEPATTERNID_CROSS_LEGGED_ON_LEFT = 8;
        public int MUSCLESTATEPATTERNID_SHOULDER_HIGHLY_TILTED_TO_RIGHT = 9;
        public int MUSCLESTATEPATTERNID_SHOULDER_TILTED_TO_RIGHT = 10;
        public int MUSCLESTATEPATTERNID_SHOULDER_TILTED_TO_LEFT = 11;
        public int MUSCLESTATEPATTERNID_SHOULDER_HIGHLY_TILTED_TO_LEFT = 12;
        public int MUSCLESTATEPATTERNID_NECK_HIGHLY_TILTED_TO_RIGHT = 13;
        public int MUSCLESTATEPATTERNID_NECK_TILTED_TO_RIGHT = 14;
        public int MUSCLESTATEPATTERNID_NECK_TILTED_TO_LEFT = 15;
        public int MUSCLESTATEPATTERNID_NECK_HIGHLY_TILTED_TO_LEFT = 16;
        public int MUSCLESTATEPATTERNID_MAX = 17;

        // ‹Ø“÷‚Ì•\Ž¦F‚Ì’è‹`.
        public int MUSCLECOLORID_NONE = 0;
        public int MUSCLECOLORID_YELLOW = 1;
        public int MUSCLECOLORID_RED = 2;
        public int MUSCLECOLORID_BLUE = 3;
        public int MUSCLECOLORID_MAX = 4;

        //‹Ø“÷ó‘Ôƒ^ƒCƒv.
        public int MUSCLESTATETYPEID_NONE = 0;
        public int MUSCLESTATETYPEID_STRAINED = 1;
        public int MUSCLESTATETYPEID_RELAXED = 2;
        public int MUSCLESTATETYPEID_MAX = 3;

        // Šp“xƒXƒNƒŠƒvƒg‚ÌƒIƒyƒR[ƒh‚h‚c.
        public int ANGLESCRIPTOPECODEID_NOP = 0;
        public int ANGLESCRIPTOPECODEID_START = 1;
        public int ANGLESCRIPTOPECODEID_CHECK = 2;
        public int ANGLESCRIPTOPECODEID_END = 3;

        // ‹Ø“÷ƒXƒNƒŠƒvƒg‚ÌƒIƒyƒR[ƒh‚h‚c,
        public int MUSCLESCRIPTOPECODEID_NOP = 0;
        public int MUSCLESCRIPTOPECODEID_START = 1;
        public int MUSCLESCRIPTOPECODEID_CHECK = 2;
        public int MUSCLESCRIPTOPECODEID_END = 3;

        // ”äŠr‰‰ŽZŽq.
        public int COMPAREOPERATORID_NONE = 0;
        public int COMPAREOPERATORID_TRUE = 1;
        public int COMPAREOPERATORID_FALSE = 2;
        public int COMPAREOPERATORID_GREATEROREQUAL = 3;
        public int COMPAREOPERATORID_GREATER = 4;
        public int COMPAREOPERATORID_LESSOREQUAL = 5;
        public int COMPAREOPERATORID_LESS = 6;
        public int COMPAREOPERATORID_MAX = 7;

        //W–ñ‰‰ŽZŽq‚h‚c.
        public int COLLECTOPERATORID_NONE = 0;
        public int COLLECTOPERATORID_GETSUM = 1;
        public int COLLECTOPERATORID_GETMAX = 2;
        public int COLLECTOPERATORID_MAX = 3;

        //@‘ÌˆÊ‚h‚c.
        public int BODYPOSITIONTYPEID_NONE = 0;
        public const int BODYPOSITIONTYPEID_STANDING = 1;
        public const int BODYPOSITIONTYPEID_KNEEDOWN = 2;
        public const int BODYPOSITIONTYPEID_COMMON = 3;
        public int BODYPOSITIONTYPEID_MAX = 4;

        // Šp“x‚h‚c.
        public int BODYANGLEID_NONE = 0;
        public const int BODYANGLEID_HIP = 1;
        public const int BODYANGLEID_CENTER = 2;
        public const int BODYANGLEID_RTHIGH = 3;
        public const int BODYANGLEID_LTHIGH = 4;
        public const int BODYANGLEID_SHOULDER = 5;
        public const int BODYANGLEID_HEAD = 6;
        public const int BODYANGLEID_EAR = 7;
        public const int BODYANGLEID_NECKFORWARDLEANING = 8;
        public const int BODYANGLEID_BODY2LOINS = 9;
        public const int BODYANGLEID_SIDEUPPERCENTER = 10;
        public const int BODYANGLEID_SIDESHOULDEREAR = 11;
        public const int BODYANGLEID_SIDEPELVIS = 12;
        public const int BODYANGLEID_MAX = 13;

        // Œ‹‰Ê‚h‚c.
        public int RESULTID_NONE = 0;
        public int RESULTID_RIGHTKNEE = 1;
        public int RESULTID_LEFTKNEE = 2;
        public int RESULTID_HIP = 3;
        public int RESULTID_CENTERBALANCE = 4;
        public int RESULTID_SHOULDERBALANCE = 5;
        public int RESULTID_EARBALANCE = 6;
        public int RESULTID_BODYCENTER = 7;
        public int RESULTID_HEADCENTER = 8;
        public int RESULTID_SIDEBODYBALANCE = 9;
        public int RESULTID_NECKFORWARDLEANING = 10;
        public int RESULTID_POSTUREPATTERN_NORMAL = 11;
        public int RESULTID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD = 12;
        public int RESULTID_POSTUREPATTERN_STOOP = 13;
        public int RESULTID_POSTUREPATTERN_BEND_BACKWARD = 14;
        public int RESULTID_POSTUREPATTERN_FLATBACK = 15;
        public int RESULTID_MAX = 16;

        // ƒtƒ@ƒCƒ‹“Ç‚Ýž‚ÝƒGƒ‰[ƒR[ƒh.
        public int READFROMSTRING_SYNTAX_OK = 0;
        public int READFROMSTRING_NULLLINE = 1;
        public int READFROMSTRING_SYNTAX_ERROR = 2;
        public int READFROMSTRING_MINVALUE_COMPAREOPERATOR_INVALID = 3;
        public int READFROMSTRING_MAXVALUE_COMPAREOPERATOR_INVALID = 4;
        public int READFROMSTRING_MUSCLESTATEPATTERN_INVALID = 5;
        public int READFROMSTRING_MUSCLE_INVALID = 6;
        public int READFROMSTRING_MUSCLESTATETYPE_INVALID = 7;
        public int READFROMSTRING_OPECODE_INVALID = 8;
        public int READFROMSTRING_BODYANGLE_INVALID = 9;
        public int READFROMSTRING_COLLECTOPERATOR_INVALID = 10;
        public int READFROMSTRING_MUSCLECOLOR_INVALID = 11;
        public int READFROMSTRING_RANGE_INVALID = 12;
        public int READFROMSTRING_RESULT_INVALID = 13;
        public int READFROMSTRING_BODYPOSITIONTYPE_INVALID = 14;


        /*public int GetMuscleIDBySymbol( string pchMuscleSymbol )
        {
            public class CSymbolAndIDPair
                {

                    int iID;
                    string pchSymbol;

                };
                MUSCLEID_LATISSIMUSDORSI 

                CSymbolAndIDPair aSymbolAnbdIDPair[] = 
                {MUSCLEID_LATISSIMUSDORSI,          "LatissimusDorsi"},
                {MUSCLEID_BICEPSFEMORIS_LEFT,       "BicepsFemorisLeft"},
                {MUSCLEID_BICEPSFEMORIS_RIGHT,      "BicepsFemorisRight"},
                {MUSCLEID_TENSORFASCIAELATAE_LEFT,  "TensorFasciaeLataeLeft"},
                {MUSCLEID_TENSORFASCIAELATAE_RIGHT, "TensorFasciaeLataeRight"},
                {MUSCLEID_VASTUSLATERALIS_LEFT,     "VastusLateralisLeft"},
                {MUSCLEID_VASTUSLATERALIS_RIGHT,    "VastusLateralisRight"},
                {MUSCLEID_QUADRICEPSFEMORIS_LEFT,   "QuadricepsFemorisLeft"},
                {MUSCLEID_QUADRICEPSFEMORIS_RIGHT,  "QuadricepsFemorisRight"},
                {MUSCLEID_HIPJOINTCAPSULE_LEFT,     "HipJointCapsuleLeft"},
                {MUSCLEID_HIPJOINTCAPSULE_RIGHT,    "HipJointCapsuleRight"},
                {MUSCLEID_GLUTEUSMEDIUS_LEFT,       "GluteusMediusLeft"},
                {MUSCLEID_GLUTEUSMEDIUS_RIGHT,      "GluteusMediusRight"},
                {MUSCLEID_RECTUSABDOMINIS,          "RectusAbdominis"},
                {MUSCLEID_ILIOTIBIALTRACT_LEFT,     "IliotibialTractLeft"},
                {MUSCLEID_ILIOTIBIALTRACT_RIGHT,    "IliotibialTractRight"},
                {MUSCLEID_FIBULARIS_LEFT,           "FibularisLeft"},
                {MUSCLEID_FIBULARIS_RIGHT,          "FibularisRight"},
                {MUSCLEID_GASTROCNEMIUS_LEFT,       "GastrocnemiusLeft"},
                {MUSCLEID_GASTROCNEMIUS_RIGHT,      "GastrocnemiusRight"},
                {MUSCLEID_CUBOID_LEFT,              "CuboidLeft"},
                {MUSCLEID_CUBOID_RIGHT,             "CuboidRight"},
                {MUSCLEID_ADDUCTOR_LEFT,            "AdductorLeft"},
                {MUSCLEID_ADDUCTOR_RIGHT,           "AdductorRight"},
                {MUSCLEID_TRAPEZIUS_LEFT,           "TrapeziusLeft"},
                {MUSCLEID_TRAPEZIUS_RIGHT,          "TrapeziusRight"},
                {MUSCLEID_STERNOCLEIDOMASTOID,      "Sternocleidomastoid"},
                {MUSCLEID_MAX,                      NULL}
            };

                int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLEID_MAX; i++ ){
                if ( strcmp(aSymbolAnbdIDPair[i].pchSymbol, pchMuscleSymbol ) == 0 ){
                    return aSymbolAnbdIDPair[i].iID;
                }
        }
            return MUSCLEID_NONE;
        }*/
        /*
        string GetStandingMuscleBMPFileNameByID(int iMuscleID )
        {
            class CSymbolAndIDPair
        {
            public:
                int iID;
            const char* pchSymbol;
        };

        static CSymbolAndIDPair aSymbolAnbdIDPair[] = {
                {MUSCLEID_LATISSIMUSDORSI,          "r_01.bmp"},
                {MUSCLEID_BICEPSFEMORIS_LEFT,       "r_02l.bmp"},
                {MUSCLEID_BICEPSFEMORIS_RIGHT,      "r_02r.bmp"},
                {MUSCLEID_TENSORFASCIAELATAE_LEFT,  "r_03l.bmp"},
                {MUSCLEID_TENSORFASCIAELATAE_RIGHT, "r_03r.bmp"},
                {MUSCLEID_VASTUSLATERALIS_LEFT,     "r_04l.bmp"},
                {MUSCLEID_VASTUSLATERALIS_RIGHT,    "r_04r.bmp"},
                {MUSCLEID_QUADRICEPSFEMORIS_LEFT,   "r_05l.bmp"},
                {MUSCLEID_QUADRICEPSFEMORIS_RIGHT,  "r_05r.bmp"},
                {MUSCLEID_HIPJOINTCAPSULE_LEFT,     "r_06l.bmp"},
                {MUSCLEID_HIPJOINTCAPSULE_RIGHT,    "r_06r.bmp"},
                {MUSCLEID_GLUTEUSMEDIUS_LEFT,       "r_07l.bmp"},
                {MUSCLEID_GLUTEUSMEDIUS_RIGHT,      "r_07r.bmp"},
                {MUSCLEID_RECTUSABDOMINIS,          "r_08.bmp"},
                {MUSCLEID_ILIOTIBIALTRACT_LEFT,     "r_09l.bmp"},
                {MUSCLEID_ILIOTIBIALTRACT_RIGHT,    "r_09r.bmp"},
                {MUSCLEID_FIBULARIS_LEFT,           "r_10l.bmp"},
                {MUSCLEID_FIBULARIS_RIGHT,          "r_10r.bmp"},
                {MUSCLEID_GASTROCNEMIUS_LEFT,       "r_11l.bmp"},
                {MUSCLEID_GASTROCNEMIUS_RIGHT,      "r_11r.bmp"},
                {MUSCLEID_CUBOID_LEFT,              "r_12l.bmp"},
                {MUSCLEID_CUBOID_RIGHT,             "r_12r.bmp"},
                {MUSCLEID_ADDUCTOR_LEFT,            "r_13l.bmp"},
                {MUSCLEID_ADDUCTOR_RIGHT,           "r_13r.bmp"},
                {MUSCLEID_TRAPEZIUS_LEFT,           "r_14l.bmp"},
                {MUSCLEID_TRAPEZIUS_RIGHT,          "r_14r.bmp"},
                {MUSCLEID_STERNOCLEIDOMASTOID,      "r_15.bmp"},
                {MUSCLEID_MAX,                      NULL}
            };

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLEID_MAX; i++ ){
                if ( aSymbolAnbdIDPair[i].iID == iMuscleID ){
                    return aSymbolAnbdIDPair[i].pchSymbol;
                }
            }
            return NULL;
        }
        */
        /*
        const char* GetKneedownMuscleBMPFileNameByID(int iMuscleID )
        {
            class CSymbolAndIDPair
        {
            public:
                int iID;
            const char* pchSymbol;
        };

        static CSymbolAndIDPair aSymbolAnbdIDPair[] = {
                {MUSCLEID_LATISSIMUSDORSI,          "k_01.bmp"},
                {MUSCLEID_BICEPSFEMORIS_LEFT,       "k_02l.bmp"},
                {MUSCLEID_BICEPSFEMORIS_RIGHT,      "k_02r.bmp"},
                {MUSCLEID_TENSORFASCIAELATAE_LEFT,  "k_03l.bmp"},
                {MUSCLEID_TENSORFASCIAELATAE_RIGHT, "k_03r.bmp"},
                {MUSCLEID_VASTUSLATERALIS_LEFT,     "k_04l.bmp"},
                {MUSCLEID_VASTUSLATERALIS_RIGHT,    "k_04r.bmp"},
                {MUSCLEID_QUADRICEPSFEMORIS_LEFT,   "k_05l.bmp"},
                {MUSCLEID_QUADRICEPSFEMORIS_RIGHT,  "k_05r.bmp"},
                {MUSCLEID_HIPJOINTCAPSULE_LEFT,     "k_06l.bmp"},
                {MUSCLEID_HIPJOINTCAPSULE_RIGHT,    "k_06r.bmp"},
                {MUSCLEID_GLUTEUSMEDIUS_LEFT,       "k_07l.bmp"},
                {MUSCLEID_GLUTEUSMEDIUS_RIGHT,      "k_07r.bmp"},
                {MUSCLEID_RECTUSABDOMINIS,          "k_08.bmp"},
                {MUSCLEID_ILIOTIBIALTRACT_LEFT,     "k_09l.bmp"},
                {MUSCLEID_ILIOTIBIALTRACT_RIGHT,    "k_09r.bmp"},
                {MUSCLEID_FIBULARIS_LEFT,           "k_10l.bmp"},
                {MUSCLEID_FIBULARIS_RIGHT,          "k_10r.bmp"},
                {MUSCLEID_GASTROCNEMIUS_LEFT,       "k_11l.bmp"},
                {MUSCLEID_GASTROCNEMIUS_RIGHT,      "k_11r.bmp"},
                {MUSCLEID_CUBOID_LEFT,              "k_12l.bmp"},
                {MUSCLEID_CUBOID_RIGHT,             "k_12r.bmp"},
                {MUSCLEID_ADDUCTOR_LEFT,            "k_13l.bmp"},
                {MUSCLEID_ADDUCTOR_RIGHT,           "k_13r.bmp"},
                {MUSCLEID_TRAPEZIUS_LEFT,           "k_14l.bmp"},
                {MUSCLEID_TRAPEZIUS_RIGHT,          "k_14r.bmp"},
                {MUSCLEID_STERNOCLEIDOMASTOID,      "k_15.bmp"},
                {MUSCLEID_MAX,                      NULL}
            };

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLEID_MAX; i++ ){
                if ( aSymbolAnbdIDPair[i].iID == iMuscleID ){
                    return aSymbolAnbdIDPair[i].pchSymbol;
                }
            }
            return NULL;
        }
        */
        /*
        int GetMuscleStatePatternIDBySymbol( const char* pchMuscleStatePatternSymbol)
        {

            class CSymbolAndIDPair
        {
            public:
                int iID;
            const char* pchSymbol;
        };

        static CSymbolAndIDPair aSymbolAnbdIDPair[] = {
                {MUSCLESTATEPATTERNID_PELVIC_ROTATED_TO_RIGHT,          "PelvicRotatedToRight"},
                {MUSCLESTATEPATTERNID_PELVIC_ROTATED_TO_LEFT,           "PelvicRotatedToLeft"},
                {MUSCLESTATEPATTERNID_PELVIC_TILTED_TO_RIGHT,           "PelvicTiltedToRight"},
                {MUSCLESTATEPATTERNID_PELVIC_TILTED_TO_LEFT,            "PelvicTiltedToLeft"},
                {MUSCLESTATEPATTERNID_BANDLY_LEGGED_ON_RIGHT,           "BandlyLeggedOnRight"},
                {MUSCLESTATEPATTERNID_BANDLY_LEGGED_ON_LEFT,            "BandlyLeggedOnLeft"},
                {MUSCLESTATEPATTERNID_CROSS_LEGGED_ON_RIGHT,            "CrossLeggedOnRight"},
                {MUSCLESTATEPATTERNID_CROSS_LEGGED_ON_LEFT,             "CrossLeggedOnLeft"},
                {MUSCLESTATEPATTERNID_SHOULDER_HIGHLY_TILTED_TO_RIGHT,  "ShoulderHighlyTiltedToRight"},
                {MUSCLESTATEPATTERNID_SHOULDER_TILTED_TO_RIGHT,         "ShoulderTiltedToRight"},
                {MUSCLESTATEPATTERNID_SHOULDER_TILTED_TO_LEFT,          "ShoulderTiltedToLeft"},
                {MUSCLESTATEPATTERNID_SHOULDER_HIGHLY_TILTED_TO_LEFT,   "ShoulderHighlyTiltedToLeft"},
                {MUSCLESTATEPATTERNID_NECK_HIGHLY_TILTED_TO_RIGHT,      "NeckHighlyTiltedToRight"},
                {MUSCLESTATEPATTERNID_NECK_TILTED_TO_RIGHT,             "NeckTiltedToRight"},
                {MUSCLESTATEPATTERNID_NECK_TILTED_TO_LEFT,              "NeckTiltedToLeft"},
                {MUSCLESTATEPATTERNID_NECK_HIGHLY_TILTED_TO_LEFT,       "NeckHighlyTiltedToLeft"},
                {MUSCLESTATEPATTERNID_MAX,                              NULL},
            };

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLESTATEPATTERNID_MAX; i++ ){
                if ( strcmp(aSymbolAnbdIDPair[i].pchSymbol, pchMuscleStatePatternSymbol ) == 0 ){
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return MUSCLESTATEPATTERNID_NONE;
        }
        */
        /*
        int GetMuscleColorIDBySymbol( const char* pchMuscleColorSymbol)
        {

            class CSymbolAndIDPair
        {
            public:
                int iID;
            const char* pchSymbol;
        };

        static CSymbolAndIDPair aSymbolAnbdIDPair[] = {
                {MUSCLECOLORID_NONE,    "None"},
                {MUSCLECOLORID_YELLOW,  "Yellow"},
                {MUSCLECOLORID_RED,     "Red"},
                {MUSCLECOLORID_BLUE,    "Blue"},
                {MUSCLECOLORID_MAX,     NULL}
            };

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLECOLORID_MAX; i++ ){
                if ( strcmp(aSymbolAnbdIDPair[i].pchSymbol, pchMuscleColorSymbol ) == 0 ){
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return MUSCLECOLORID_NONE;
        }*/
        /*
        int GetCollectOperatorIDBySymbol( const char* pchCollectOperatorSymbol)
        {

            class CSymbolAndIDPair
        {
            public:
                int iID;
            const char* pchSymbol;
        };

        static CSymbolAndIDPair aSymbolAnbdIDPair[] = {
                {COLLECTOPERATORID_GETSUM,  "Sum"},
                {COLLECTOPERATORID_GETMAX,  "Max"},
                {COLLECTOPERATORID_MAX,     NULL}
            };
        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != COLLECTOPERATORID_MAX; i++ ){
                if ( strcmp(aSymbolAnbdIDPair[i].pchSymbol, pchCollectOperatorSymbol ) == 0 ){
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return COLLECTOPERATORID_NONE;
        }
        */
        /*
        int GetMuscleStateTypeIDBySymbol( const char* pchMuscleStateTypeSymbol)
        {

            class CSymbolAndIDPair
        {
            public:
                int iID;
            const char* pchSymbol;
        };

        static CSymbolAndIDPair aSymbolAnbdIDPair[] = {
                {MUSCLESTATETYPEID_STRAINED,    "Strained"},
                {MUSCLESTATETYPEID_RELAXED,     "Relaxed"},
                {MUSCLESTATETYPEID_MAX,         NULL}
            };

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLESTATETYPEID_MAX; i++ ){
                if ( strcmp(aSymbolAnbdIDPair[i].pchSymbol, pchMuscleStateTypeSymbol ) == 0 ){
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return MUSCLESTATETYPEID_NONE;
        }*/

        public int GetBodyPositionTypeIDBySymbol(string pchBodyPositionTypeSymbol)
        {

            /*   class CSymbolAndIDPair
           {
               public:
                   int iID;
               const char* pchSymbol;
           };*/
            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_STANDING, pchSymbol = "Standing" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_KNEEDOWN, pchSymbol = "Kneedown" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_COMMON, pchSymbol = "Common" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_MAX, pchSymbol = "null" };




            int i = 0;
            for (i = 0; aSymbolAnbdIDPair[i].iID != BODYPOSITIONTYPEID_MAX; i++)
            {
                if (aSymbolAnbdIDPair[i].pchSymbol == pchBodyPositionTypeSymbol)
                {
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return BODYPOSITIONTYPEID_NONE;
        }

        public int GetBodyAngleIDBySymbol(string pchBodyAngleSymbol)
        {

            /*public class CSymbolAndIDPair
                {

                    public int iID;
                    public string pchSymbol;
                };*/

            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = BODYANGLEID_HIP, pchSymbol = "hip" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = BODYANGLEID_CENTER, pchSymbol = "center" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = BODYANGLEID_RTHIGH, pchSymbol = "rthigh" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = BODYANGLEID_LTHIGH, pchSymbol = "lthigh" };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDPair { iID = BODYANGLEID_SHOULDER, pchSymbol = "shoulder" };
            aSymbolAnbdIDPair[5] = new CSymbolAndIDPair { iID = BODYANGLEID_HEAD, pchSymbol = "head" };
            aSymbolAnbdIDPair[6] = new CSymbolAndIDPair { iID = BODYANGLEID_EAR, pchSymbol = "ear" };
            aSymbolAnbdIDPair[7] = new CSymbolAndIDPair { iID = BODYANGLEID_NECKFORWARDLEANING, pchSymbol = "NeckForwardLeaning" };
            aSymbolAnbdIDPair[8] = new CSymbolAndIDPair { iID = BODYANGLEID_BODY2LOINS, pchSymbol = "body2loins" };
            aSymbolAnbdIDPair[9] = new CSymbolAndIDPair { iID = BODYANGLEID_SIDEUPPERCENTER, pchSymbol = "sideuppercenter" };
            aSymbolAnbdIDPair[11] = new CSymbolAndIDPair { iID = BODYANGLEID_SIDESHOULDEREAR, pchSymbol = "sideshoulderear" };
            aSymbolAnbdIDPair[12] = new CSymbolAndIDPair { iID = BODYANGLEID_SIDEPELVIS, pchSymbol = "sidepelvis" };
            aSymbolAnbdIDPair[13] = new CSymbolAndIDPair { iID = BODYANGLEID_MAX, pchSymbol = "NULL" };






            int i = 0;
            for (i = 0; aSymbolAnbdIDPair[i].iID != BODYANGLEID_MAX; i++)
            {
                if (aSymbolAnbdIDPair[i].pchSymbol == pchBodyAngleSymbol)
                {
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return BODYANGLEID_NONE;
        }



        public int GetResultIDBySymbol(string pchResultSymbol)
        {

            /* class CSymbolAndIDPair
             {
             public:
                 int iID;
             const char* pchSymbol;
             };*/

            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = RESULTID_RIGHTKNEE, pchSymbol = "RightKnee" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = RESULTID_LEFTKNEE, pchSymbol = "LeftKnee" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = RESULTID_CENTERBALANCE, pchSymbol = "CenterBalance" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = RESULTID_SHOULDERBALANCE, pchSymbol = "ShoulderBalance" };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDPair { iID = RESULTID_EARBALANCE, pchSymbol = "EarBalance" };
            aSymbolAnbdIDPair[5] = new CSymbolAndIDPair { iID = RESULTID_BODYCENTER, pchSymbol = "BodyCenter" };
            aSymbolAnbdIDPair[6] = new CSymbolAndIDPair { iID = RESULTID_HEADCENTER, pchSymbol = "HeadCenter" };
            aSymbolAnbdIDPair[7] = new CSymbolAndIDPair { iID = RESULTID_SIDEBODYBALANCE, pchSymbol = "SideBodyBalance" };
            aSymbolAnbdIDPair[8] = new CSymbolAndIDPair { iID = RESULTID_NECKFORWARDLEANING, pchSymbol = "NeckForwardLeaning" };
            aSymbolAnbdIDPair[9] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_NORMAL, pchSymbol = "POSTUREPATTERN_NORMAL" };
            aSymbolAnbdIDPair[11] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD, pchSymbol = "POSTUREPATTERN_STOOP_AND_BEND_BACKWARD" };
            aSymbolAnbdIDPair[12] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_STOOP, pchSymbol = "POSTUREPATTERN_STOOP" };
            aSymbolAnbdIDPair[13] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_BEND_BACKWARD, pchSymbol = "POSTUREPATTERN_BEND_BACKWARD" };

            aSymbolAnbdIDPair[14] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_FLATBACK, pchSymbol = "POSTUREPATTERN_FLATBACK" };

            aSymbolAnbdIDPair[15] = new CSymbolAndIDPair { iID = RESULTID_MAX, pchSymbol = "NULL" };



            int i = 0;
            for (i = 0; aSymbolAnbdIDPair[i].iID != RESULTID_MAX; i++)
            {
                if (aSymbolAnbdIDPair[i].pchSymbol == pchResultSymbol)
                {
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return RESULTID_NONE;
        }
        /*
        int GetCompareOperatorIDBySymbol( const char* pchCompareOperatorSymbol)
        {

            class CSymbolAndIDPair
        {
            public:
                int iID;
            const char* pchSymbol;
        };

        static CSymbolAndIDPair aSymbolAnbdIDPair[] = {
                {COMPAREOPERATORID_GREATEROREQUAL,  "GreaterOrEqual"},
                {COMPAREOPERATORID_GREATER,         "Greater"},
                {COMPAREOPERATORID_LESSOREQUAL,     "LessOrEqual"},
                {COMPAREOPERATORID_LESS,            "Less"},
                {COMPAREOPERATORID_MAX,             NULL}
            };

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != COMPAREOPERATORID_MAX; i++ ){
                if ( strcmp(aSymbolAnbdIDPair[i].pchSymbol, pchCompareOperatorSymbol ) == 0 ){
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return COMPAREOPERATORID_NONE;
        }
        */
        public double GetBodyAngleValue(FrontBodyAngle Angle, SideBodyAngle AngleSide, int iBodyAngleID)
        {
            double dValue = 0.0;

            switch (iBodyAngleID)
            {
                case BODYANGLEID_HIP:
                    return Angle.GetHip();
                    break;
                case BODYANGLEID_CENTER:
                    return Angle.GetCenter();
                    break;
                case BODYANGLEID_RTHIGH:
                    return Angle.GetRightThigh();
                    break;
                case BODYANGLEID_LTHIGH:
                    return Angle.GetLeftThigh();
                    break;
                case BODYANGLEID_SHOULDER:
                    return Angle.GetShoulder();
                    break;
                case BODYANGLEID_HEAD:
                    return Angle.GetHead();
                    break;
                case BODYANGLEID_EAR:
                    return Angle.GetEar();
                    break;
                case BODYANGLEID_NECKFORWARDLEANING:
                    return AngleSide.GetNeckForwardLeaning();
                    break;
                case BODYANGLEID_BODY2LOINS:
                    return Angle.GetBody2Loins();
                    break;
                case BODYANGLEID_SIDEUPPERCENTER:
                    return AngleSide.GetBodyBalance();
                    break;
                case BODYANGLEID_SIDESHOULDEREAR:
                    return AngleSide.GetShoulderEarAngle();
                    break;
                case BODYANGLEID_SIDEPELVIS:
                    return AngleSide.GetPelvicAngle();
                    break;
                default:
                    break;
            }
            return dValue;
        }
        /*
                void OutputReadErrorString(CString &strError, int iLineNo, int iErrorCode)
                {
                    CString strNewError;
                    switch (iErrorCode)
                    {
                        case READFROMSTRING_SYNTAX_OK:
                        case READFROMSTRING_NULLLINE:
                            break;
                        case READFROMSTRING_SYNTAX_ERROR:
                            strNewError.Format("Line %d: SYNTAX_ERROR\n", iLineNo);
                            break;
                        case READFROMSTRING_MINVALUE_COMPAREOPERATOR_INVALID:
                            strNewError.Format("Line %d: MINVALUE_COMPAREOPERATOR_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_MAXVALUE_COMPAREOPERATOR_INVALID:
                            strNewError.Format("Line %d: MAXVALUE_COMPAREOPERATOR_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_MUSCLESTATEPATTERN_INVALID:
                            strNewError.Format("Line %d: MUSCLESTATEPATTERN_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_MUSCLE_INVALID:
                            strNewError.Format("Line %d: MUSCLE_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_MUSCLESTATETYPE_INVALID:
                            strNewError.Format("Line %d: MUSCLESTATETYPE_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_OPECODE_INVALID:
                            strNewError.Format("Line %d: OPECODE_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_BODYANGLE_INVALID:
                            strNewError.Format("Line %d: BODYANGLE_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_COLLECTOPERATOR_INVALID:
                            strNewError.Format("Line %d: COLLECTOPERATOR_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_MUSCLECOLOR_INVALID:
                            strNewError.Format("Line %d: MUSCLECOLOR_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_RANGE_INVALID:
                            strNewError.Format("Line %d: RANGE_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_RESULT_INVALID:
                            strNewError.Format("Line %d: RESULT_INVALID\n", iLineNo);
                            break;
                        case READFROMSTRING_BODYPOSITIONTYPE_INVALID:
                            strNewError.Format("Line %d: BODYPOSITIONTYPE_INVALID\n", iLineNo);
                            break;
                        default:
                            break;
                    }
                    if (!(strNewError.IsEmpty()))
                    {
                        strError += strNewError;
                    }
                }






                }*/


        public class CSymbolAndIDPair
        {

            public int iID;
            public string pchSymbol;
        }
    }
}