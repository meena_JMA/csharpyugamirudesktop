﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class ResultActionScriptMgr
    {
        int m_iCount = 0;
        ResultActionScriptElement[] m_pElement;
        SymbolFunc m_SymbolFunc;





        // Šp“xƒAƒNƒVƒ‡ƒ“ƒXƒNƒŠƒvƒgƒ}ƒXƒ^[ƒf[ƒ^.
        ResultActionScriptMgr()
        {
            m_SymbolFunc = new SymbolFunc();
        }



        public ResultActionScriptMgr(ResultActionScriptMgr rSrc)
        {
            m_iCount = rSrc.m_iCount;

            m_pElement = null;
            if (rSrc.m_pElement != null)
            {
                m_pElement = new ResultActionScriptElement[m_iCount];
                int i = 0;
                for (i = 0; i < m_iCount; i++)
                {
                    m_pElement[i] = rSrc.m_pElement[i];
                }
            }
        }
        /*
        public ~ResultActionScriptMgr( )
        {
            if ( m_pElement != NULL ){
                delete[] m_pElement;
            m_pElement = NULL;
            }
        }

        CResultActionScriptMgr &CResultActionScriptMgr::operator=( const CResultActionScriptMgr &rSrc )
        {
            if (m_pElement != NULL)
            {
                delete[] m_pElement;
                m_pElement = NULL;
            }
            m_iCount = rSrc.m_iCount;
            if (rSrc.m_pElement != NULL)
            {
                m_pElement = new CResultActionScriptElement[m_iCount];
                int i = 0;
                for (i = 0; i < m_iCount; i++)
                {
                    m_pElement[i] = rSrc.m_pElement[i];
                }
            }
            return *this;
        }
        */
        /*
        public bool ReadFromFile(string lpszFolderName, string lpszFileName,  string pchErrorFilePath /* = NULL */ /*)
        {
            char buf[1024];
            list<ResultActionScriptElement> listElement;

            if (m_pElement != NULL)
            {
                delete[] m_pElement;
                m_pElement = NULL;
            }
            m_iCount = 0;

            CDualSourceTextReader DualSourceTextReader;
            if (!DualSourceTextReader.Open(lpszFolderName, lpszFileName))
            {
                return FALSE;
            }

            CString strError;
            int iLineNo = 1;
            while (!(DualSourceTextReader.IsEOF()))
            {
                buf[0] = '\0';
                DualSourceTextReader.Read(&(buf[0]), 1024);
                CResultActionScriptElement Element;
                int iErrorCode = Element.ReadFromString(&(buf[0]));
                if (iErrorCode == READFROMSTRING_SYNTAX_OK)
                {
                    listElement.push_back(Element);
                }
                else
                {
                    OutputReadErrorString(strError, iLineNo, iErrorCode);
                }
                iLineNo++;
            }

            if (pchErrorFilePath != NULL)
            {
                FILE* fp = ::fopen(pchErrorFilePath, "wt");

                ::fwrite((const char*)strError, 1, strError.GetLength(), fp );

                ::fclose(fp);
                fp = NULL;
            }

            // €–Ú”•ª‚¾‚¯ƒƒ‚ƒŠŠm•Û.
            INT iSize = listElement.size();
            if (iSize <= 0)
            {
                return TRUE;
            }
            m_pElement = new CResultActionScriptElement[iSize];
            if (m_pElement == NULL)
            {
                return FALSE;
            }
            m_iCount = iSize;

            INT i = 0;
            list<CResultActionScriptElement>::iterator index;
            for (i = 0, index = listElement.begin(); index != listElement.end(); index++, i++)
            {
                m_pElement[i] = (*index);
            }

            return TRUE;
        }

        BOOL ResolveRange( const CRealValueRangeDefinitionMgr &RangeDefinitionMgr, 
        const char* pchErrorFilePath /* = NULL */ /*)
        {
            BOOL bRet = TRUE;
            CString strError;
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {
                if (m_pElement[i].m_iScriptOpecodeID == ANGLESCRIPTOPECODEID_CHECK)
                {
                    CString strRangeSymbol = m_pElement[i].m_strRangeSymbol;
                    int iMinValueOperatorID = 0;
                    int iMaxValueOperatorID = 0;
                    double dMinValue = 0.0;
                    double dMaxValue = 0.0;
                    if (RangeDefinitionMgr.GetRange(iMinValueOperatorID, iMaxValueOperatorID, dMinValue, dMaxValue, strRangeSymbol))
                    {
                        m_pElement[i].ResolveRange(iMinValueOperatorID, iMaxValueOperatorID, dMinValue, dMaxValue);
                    }
                    else
                    {
                        CString strTmp;
                        strTmp.Format("Symbol %s is undefined\n", strRangeSymbol);
                        strError += strTmp;
                        bRet = FALSE;
                    }
                }
            }
            if (pchErrorFilePath != NULL)s
            {
                FILE* fp = ::fopen(pchErrorFilePath, "wt");

                ::fwrite((const char*)strError, 1, strError.GetLength(), fp );

                ::fclose(fp);
                fp = NULL;
            }
            return bRet;
        }
        */

        public bool Execute(FrontBodyResultData FrontBodyResultData, FrontBodyAngle Angle)
        {
            SideBodyAngle AngleSide = new SideBodyAngle();
            bool bContextFlag = true;
            //m_SymbolFunc = new SymbolFunc();
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {

                if (m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_START)
                {
                    switch (m_pElement[i].m_iBodyPositionTypeID)
                    {

                        case Constant.BODYPOSITIONTYPEID_STANDING:
                            bContextFlag = (Angle.IsStanding());
                            break;
                        case Constant.BODYPOSITIONTYPEID_KNEEDOWN:
                            bContextFlag = !(Angle.IsStanding());
                            break;
                        case Constant.BODYPOSITIONTYPEID_COMMON:
                            bContextFlag = true;
                            break;
                        default:
                            bContextFlag = false;
                            break;
                    }
                }
                else if (m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_CHECK)
                {
                    int iBodyAngleID = m_pElement[i].m_iBodyAngleID;
                    // strAngleSymbol‚©‚ç•Ï”‚Ì’l‚ðdValue‚É‹‚ß‚é.			
                    double dValue = m_SymbolFunc.GetBodyAngleValue(Angle, AngleSide, iBodyAngleID);
                    if (!(m_pElement[i].IsSatisfied(dValue)))
                    {
                        bContextFlag = false;
                    }
                }
                else if (m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_END)
                {
                    if (bContextFlag)
                    {
                        int iResultID = m_pElement[i].m_iResultID;
                        int iResultValue = m_pElement[i].m_iResultValue;
                        FrontBodyResultData.SetDataByResultID(iResultID, iResultValue);
                    }
                }
            }
            return true;
        }
        /*
        bool Execute(SideBodyResultData SideBodyResultData, const CSideBodyAngle &AngleSide ) const
        {
            CFrontBodyAngle Angle;
        BOOL bContextFlag = TRUE;
        int i = 0;
            for( i = 0; i<m_iCount; i++ ){
                if ( m_pElement[i].m_iScriptOpecodeID == ANGLESCRIPTOPECODEID_START ){
                    bContextFlag = TRUE;
                }
                else if ( m_pElement[i].m_iScriptOpecodeID == ANGLESCRIPTOPECODEID_CHECK ){
                    int iBodyAngleID = m_pElement[i].m_iBodyAngleID;
        // strAngleSymbol‚©‚ç•Ï”‚Ì’l‚ðdValue‚É‹‚ß‚é.			
        double dValue = GetBodyAngleValue(Angle, AngleSide, iBodyAngleID);
                    if ( !(m_pElement[i].IsSatisfied(dValue )) ){
                        bContextFlag = FALSE;
                    }
                }
                else if ( m_pElement[i].m_iScriptOpecodeID == ANGLESCRIPTOPECODEID_END ){
                    if ( bContextFlag ){
                        int iResultID = m_pElement[i].m_iResultID;
        int iResultValue = m_pElement[i].m_iResultValue;
        SideBodyResultData.SetDataByResultID( iResultID, iResultValue );
                    }
                }
            }
            return TRUE;
        }*/

    }
}
